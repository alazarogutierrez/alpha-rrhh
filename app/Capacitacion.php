<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Capacitacion extends Model
{
    //
    protected $table='capacitacion';
    protected $primaryKey='id_capacitacion';
    public $timestamps=false;
}
