<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Privilegio extends Model
{
    protected $table='privilegio';
    public function rol_priv(){
        return $this->hasMany('App\Rol_Privilegio');
    }
}
