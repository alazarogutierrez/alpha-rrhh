<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vinculacion extends Model
{
    protected $table='vinculacion';
    protected $primaryKey='id_vin';
    public $timestamps=false;
}
