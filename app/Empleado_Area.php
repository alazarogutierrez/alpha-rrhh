<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empleado_Area extends Model
{
    protected $table='empleado_area';
    protected $primaryKey='id_empleado_area';
    public $timestamps=false;
}
