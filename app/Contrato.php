<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contrato extends Model
{
    protected $table='contrato';
    protected $primaryKey='id_contrato';
    public $timestamps=false;
}
