<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Eliminacionmem extends Model
{
    protected $table='eliminacionmem';
    protected $primaryKey='id_em';
    public $timestamps=false;
}
