<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmpleadoCapacitacion extends Model
{
    protected $table='empleado_capacitacion';
    protected $primaryKey='id_empleado_capacitacion';
    public $timestamps=false;
}
