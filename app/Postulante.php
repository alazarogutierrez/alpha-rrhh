<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Postulante extends Model
{
    protected $table='postulante';
    protected $primaryKey='id_postulante';
    public $timestamps=false;

}
