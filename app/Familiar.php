<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Familiar extends Model
{
    protected $table='familiar';
    protected $primaryKey='id_familiar';
    public $timestamps=false;
}
