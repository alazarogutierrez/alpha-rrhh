<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hoja extends Model
{
    protected $table='hojadevida';
    protected $primaryKey='id_hv';
    public $timestamps=false;
}
