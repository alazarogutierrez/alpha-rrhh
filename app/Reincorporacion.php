<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reincorporacion extends Model
{
    protected $table='reincorporacion';
    protected $primaryKey='id_re';
    public $timestamps=false;
}
