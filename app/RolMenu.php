<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RolMenu extends Model
{
    protected  $table = 'rol_menu';
    protected $primaryKey = 'id_rol_menu';
    public $timestamps = false;
}
