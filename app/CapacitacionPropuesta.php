<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CapacitacionPropuesta extends Model
{
    protected $table='capacitacion_propuesta';
    protected $primaryKey='id_capacitacion_propuesta';
    public $timestamps=false;
}
