<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Registromem extends Model
{
    protected $table='registromem';
    protected $primaryKey='id_rm';
    public $timestamps=false;
}
