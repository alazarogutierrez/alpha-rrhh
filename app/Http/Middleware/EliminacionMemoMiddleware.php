<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class EliminacionMemoMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $query = DB::select("SELECT e.nombre
                                    FROM privilegio a, rol b, usuario_rol c, rol_privilegio d, usuario e
                                    WHERE e.id_usuario = c.id_usuario 
                                          AND b.id_rol = c.id_rol 
                                          AND b.id_rol = d.id_rol 
                                          AND a.id_privilegio = d.id_privilegio 
                                          AND a.nombre = 'eliminacion_memo' 
                                          AND e.id_usuario = ? ",
            [Auth::user()->id_usuario]);
        if(count($query)==0){
            return redirect()->route('denegado');
        }
        return $next($request);
    }
}
