<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Empleado;
use App\Hoja;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use DB;
use Barryvdh\DomPDF\Facade as PDF;


class HojaController extends Controller
{
    public function __construct()
    {

    }

    public function index(Request $request)
    {
        if ($request)
        {
            $query=trim($request->get('searchText'));
            $empleados=DB::table('empleado')
                ->where('documento','LIKE','%'.$query.'%')
                ->where ('estado','=','activo')
                ->orderBy('id_empleado','asc')
                ->paginate(7);

            return view('hoja.index',["empleados"=>$empleados,"searchText"=>$query]);
        }
    }


    public function crear($id)
    {
        $empleado = Empleado::findOrFail($id);
        $count = DB::table('hojadevida')
            ->where('id_empleado', '=',$empleado->id_empleado)
            ->count();
        if($count > 0 ) {
            return Redirect::to('hoja');
        }
        else {
            return view("hoja.create", ["empleado" => $empleado]);
        }
    }

    public function guardar(Request $request, $id)
    {
        $empleado=Empleado::findOrFail($id);
        $hoja = new Hoja;
        $hoja->nivel=$request->get('nivel');
        $hoja->primaria=$request->get('primaria');
        $hoja->añoprimaria=$request->get('añoprimaria');
        $hoja->secundaria=$request->get('secundaria');
        $hoja->añosecundaria=$request->get('añosecundaria');
        $hoja->pregrado1=$request->get('pregrado1');
        $hoja->tpregrado1=$request->get('tpregrado1');
        $hoja->apregrado1=$request->get('apregrado1');
        $hoja->pregrado2=$request->get('pregrado2');
        $hoja->tpregrado2=$request->get('tpregrado2');
        $hoja->apregrado2=$request->get('apregrado2');
        $hoja->postgrado1=$request->get('postgrado1');
        $hoja->tpostgrado1=$request->get('tpostgrado1');
        $hoja->apostgrado1=$request->get('apostgrado1');
        $hoja->postgrado2=$request->get('postgrado2');
        $hoja->tpostgrado2=$request->get('tpostgrado2');
        $hoja->apostgrado2=$request->get('apostgrado2');
        $hoja->postgrado3=$request->get('postgrado3');
        $hoja->tpostgrado3=$request->get('tpostgrado3');
        $hoja->apostgrado3=$request->get('apostgrado3');
        $hoja->elaboral1=$request->get('elaboral1');
        $hoja->celaboral1=$request->get('celaboral1');
        $hoja->aelaboral1=$request->get('aelaboral1');
        $hoja->relaboral1=$request->get('relaboral1');
        $hoja->elaboral2=$request->get('elaboral2');
        $hoja->celaboral2=$request->get('celaboral2');
        $hoja->aelaboral2=$request->get('aelaboral2');
        $hoja->relaboral2=$request->get('relaboral2');
        $hoja->elaboral3=$request->get('elaboral3');
        $hoja->celaboral3=$request->get('celaboral3');
        $hoja->aelaboral3=$request->get('aelaboral3');
        $hoja->relaboral3=$request->get('relaboral3');
        $hoja->curso1=$request->get('curso1');
        $hoja->tcurso1=$request->get('tcurso1');
        $hoja->acurso1=$request->get('acurso1');
        $hoja->curso2=$request->get('curso2');
        $hoja->tcurso2=$request->get('tcurso2');
        $hoja->acurso2=$request->get('acurso2');
        $hoja->curso3=$request->get('curso3');
        $hoja->tcurso3=$request->get('tcurso3');
        $hoja->acurso3=$request->get('acurso3');
        $hoja->idioma1=$request->get('idioma1');
        $hoja->idioma2=$request->get('idioma2');
        $hoja->idioma3=$request->get('idioma3');
        $hoja->id_empleado=$empleado->id_empleado;
        $hoja->usuario_ini=Auth::user()->id_usuario;
        $hoja->fch_ini=Carbon::now();
        $hoja->host_ini=$request->ip();
        $hoja->save();
        $count = DB::table('hojadevida')
            ->where('id_empleado', '=',$empleado->id_empleado)
            ->count();
        $empleado->hv = $count;
        $empleado->save();
        return Redirect::to('hoja');
    }

    public function show($id)
    {
        $empleado=Empleado::findOrFail($id);
        $hoja=Hoja::where('id_empleado',$empleado->id_empleado)->firstOrFail();
        return view("hoja.show",["hoja"=>$hoja]);
    }

    public function edit($id)
    {
        $empleado=Empleado::findOrFail($id);
        $hoja=Hoja::where('id_empleado',$empleado->id_empleado)->firstOrFail();
        return view("hoja.edit",["hoja"=>$hoja,"empleado"=>$empleado]);
    }
    public function update(Request $request, $id)
    {
        $hoja=Hoja::findOrFail($id);
        $hoja->nivel=$request->get('nivel');
        $hoja->primaria=$request->get('primaria');
        $hoja->añoprimaria=$request->get('añoprimaria');
        $hoja->secundaria=$request->get('secundaria');
        $hoja->añosecundaria=$request->get('añosecundaria');
        $hoja->pregrado1=$request->get('pregrado1');
        $hoja->tpregrado1=$request->get('tpregrado1');
        $hoja->apregrado1=$request->get('apregrado1');
        $hoja->pregrado2=$request->get('pregrado2');
        $hoja->tpregrado2=$request->get('tpregrado2');
        $hoja->apregrado2=$request->get('apregrado2');
        $hoja->postgrado1=$request->get('postgrado1');
        $hoja->tpostgrado1=$request->get('tpostgrado1');
        $hoja->apostgrado1=$request->get('apostgrado1');
        $hoja->postgrado2=$request->get('postgrado2');
        $hoja->tpostgrado2=$request->get('tpostgrado2');
        $hoja->apostgrado2=$request->get('apostgrado2');
        $hoja->postgrado3=$request->get('postgrado3');
        $hoja->tpostgrado3=$request->get('tpostgrado3');
        $hoja->apostgrado3=$request->get('apostgrado3');
        $hoja->elaboral1=$request->get('elaboral1');
        $hoja->celaboral1=$request->get('celaboral1');
        $hoja->aelaboral1=$request->get('aelaboral1');
        $hoja->relaboral1=$request->get('relaboral1');
        $hoja->elaboral2=$request->get('elaboral2');
        $hoja->celaboral2=$request->get('celaboral2');
        $hoja->aelaboral2=$request->get('aelaboral2');
        $hoja->relaboral2=$request->get('relaboral2');
        $hoja->elaboral3=$request->get('elaboral3');
        $hoja->celaboral3=$request->get('celaboral3');
        $hoja->aelaboral3=$request->get('aelaboral3');
        $hoja->relaboral3=$request->get('relaboral3');
        $hoja->curso1=$request->get('curso1');
        $hoja->tcurso1=$request->get('tcurso1');
        $hoja->acurso1=$request->get('acurso1');
        $hoja->curso2=$request->get('curso2');
        $hoja->tcurso2=$request->get('tcurso2');
        $hoja->acurso2=$request->get('acurso2');
        $hoja->curso3=$request->get('curso3');
        $hoja->tcurso3=$request->get('tcurso3');
        $hoja->acurso3=$request->get('acurso3');
        $hoja->idioma1=$request->get('idioma1');
        $hoja->idioma2=$request->get('idioma2');
        $hoja->idioma3=$request->get('idioma3');
        $hoja->usuario_mod=Auth::user()->id_usuario;
        $hoja->fch_mod=Carbon::now();
        $hoja->host_mod=$request->ip();
        $hoja->update();
        return Redirect::to('hoja');
    }

    public function pdf($id)
    {
        $empleado=Empleado::findOrFail($id);
        $hojas=Hoja::where('id_empleado',$empleado->id_empleado)->firstOrFail();

        $pdf = PDF::loadView('pdf.hv',["hojas"=>$hojas]);

        return $pdf->download('hv.pdf');
    }
    public function destroy($id)
    {

    }
}
