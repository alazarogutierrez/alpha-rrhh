<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Capacitacion;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;

class CursoSolicitudController extends Controller
{
    public function index(Request $request){
        if ($request)
        {
            $query=trim($request->get('searchText'));
            $cursos= DB::table('capacitacion')
                ->where('fch_del','=',null)
                ->where('tema','LIKE','%'.$query.'%')
                ->where('aprobacion','=',0)
                ->orderby('fch_ini','DESc')
                ->paginate(8);
            return view('curso_aprobacion.index',["cursos"=>$cursos,"searchText"=>$query]);
        }
    }
    public function rechazar_solicitud(Request $request,$id){
        $curso=Capacitacion::findOrFail($id);
        $curso->descripcion_aprobacion=$request->observacion;
        $curso->aprobacion=2;
        $curso->usuario_mod=Auth::user()->id_usuario;
        $curso->fch_mod=Carbon::now();
        $curso->host_mod=$request->ip();
        $curso->update();
        return Redirect::to('curso_solicitud');
    }

}
