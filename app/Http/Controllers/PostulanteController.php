<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests\PostulanteFormRequest;
use App\Postulante;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use DB;

class PostulanteController extends Controller
{
    public function index(Request $request)
    {
        if ($request)
        {
            $query=trim($request->get('searchText'));
            $postulantes=DB::table('postulante')
                ->where('documento','LIKE','%'.$query.'%')
                ->where ('fch_del','=',null)
                ->orderBy('id_postulante','asc')
                ->paginate(7);

            return view('postulante.index',["postulantes"=>$postulantes,"searchText"=>$query]);
        }
    }
    public function create()
    {
        return view("postulante.create");
    }
    public function store(PostulanteFormRequest $request){
        $p=new Postulante;
        $p->primer_nombre=$request->get('primer_nombre');
        $p->segundo_nombre=$request->get('segundo_nombre');
        $p->primer_apellido=$request->get('primer_apellido');
        $p->segundo_apellido=$request->get('segundo_apellido');
        $p->documento=$request->get('documento');
        $p->complemento1=$request->get('complemento1');
        $p->complemento2=$request->get('complemento2');
        $p->cat_tipo_documento=$request->get('cat_tipo_documento');
        $p->fecha_nacimiento=$request->get('fecha_nacimiento');
        $edad = Carbon::parse($p->fecha_nacimiento)->age;
        dump($edad);
        $p->edad=$edad;
        $p->cat_genero=$request->get('cat_genero');
        $p->celular=$request->get('celular');
        $p->telefono=$request->get('telefono');
        $p->direccion=$request->get('direccion');
        $p->cat_estado_civil=$request->get('cat_estado_civil');
        $p->cat_nacionalidad=$request->get('cat_nacionalidad');
        $p->usuario_ini=Auth::user()->id_usuario;
        $p->fch_ini=Carbon::now();
        $p->host_ini=$request->ip();
        $p->save();

        return Redirect::to('postulante');
    }
    public function show($id)
    {
    }
    public function edit($id)
    {
        $postulante=Postulante::findOrFail($id);
        return view("postulante.edit",["postulante"=>$postulante]);
    }
    public function update(Request $request,$id)
    {
        $postulante=Postulante::findOrFail($id);
        $postulante->primer_nombre=$request->get('primer_nombre');
        $postulante->segundo_nombre=$request->get('segundo_nombre');
        $postulante->primer_apellido=$request->get('primer_apellido');
        $postulante->segundo_apellido=$request->get('segundo_apellido');
        $postulante->documento=$request->get('documento');
        $postulante->complemento1=$request->get('complemento1');
        $postulante->complemento2=$request->get('complemento2');
        $postulante->cat_tipo_documento=$request->get('cat_tipo_documento');
        $postulante->fecha_nacimiento=$request->get('fecha_nacimiento');
        $edad = Carbon::parse($postulante->fecha_nacimiento)->age;
        dump($edad);
        $postulante->edad=$edad;
        $postulante->cat_genero=$request->get('cat_genero');
        $postulante->celular=$request->get('celular');
        $postulante->telefono=$request->get('telefono');
        $postulante->direccion=$request->get('direccion');
        $postulante->cat_estado_civil=$request->get('cat_estado_civil');
        $postulante->cat_nacionalidad=$request->get('cat_nacionalidad');
        $postulante->usuario_ini=Auth::user()->id_usuario;
        $postulante->fch_ini=Carbon::now();
        $postulante->host_ini=$request->ip();
        $postulante->update();
        return Redirect::to('postulante');
    }
    public function destroy($id)
    {
        $postulante=Postulante::findOrFail($id);
        $postulante->fch_del=Carbon::now();
        $postulante->update();
        return Redirect::to('postulante');
    }

}