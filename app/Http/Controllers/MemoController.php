<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Empleado;
use App\Memo;
use App\Registromem;
use App\Eliminacionmem;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use DB;

class MemoController extends Controller
{
    public function __construct()
    {

    }

    public function index(Request $request)
    {
        if ($request)
        {
            $query=trim($request->get('searchText'));
            $empleados=DB::table('empleado')
                ->where('documento','LIKE','%'.$query.'%')
                ->where ('estado','=','activo')
                ->orderBy('id_empleado','asc')
                ->paginate(7);

            return view('memo.index',["empleados"=>$empleados,"searchText"=>$query]);
        }
    }


    public function crear($id)
    {
        $empleado = Empleado::findOrFail($id);

        return view("memo.create", ["empleado" => $empleado]);

    }

    public function guardar(Request $request, $id)
    {
        $empleado=Empleado::findOrFail($id);
        $memo = new Memo;
        $memo->motivo=$request->get('motivo');
        $memo->fecha=$request->get('fecha');
        $memo->descripcion=$request->get('descripcion');
        $memo->sancion=$request->get('sancion');
        $memo->descuento=$request->get('descuento');
        $memo->id_empleado=$empleado->id_empleado;
        $memo->id_copia=$memo->id_empleado;
        $memo->usuario_ini=Auth::user()->id_usuario;
        $memo->fch_ini=Carbon::now();
        $memo->host_ini=$request->ip();
        $memo->save();
        $count = DB::table('memorandum')
            ->where('id_empleado', '=',$empleado->id_empleado)
            ->count();
        $empleado->memo = $count;
        $empleado->save();
        $rm =  new Registromem;
        $rm->id_mem=$memo->id_mem;
        $rm->id_empleado=$empleado->id_empleado;
        $rm->documento=$empleado->documento;
        $rm->usuario_ini=Auth::user()->id_usuario;
        $rm->fch_ini=Carbon::now();
        $rm->host_ini=$request->ip();
        $rm->save();
        return Redirect::to('memo');
    }

    public function listam(Request $request)
    {
        if ($request)
        {
            $query=trim($request->get('searchText'));
            $memos=DB::table('memorandum')
                ->where('id_copia','LIKE','%'.$query.'%')
                ->where ('fch_del','=',null)
                ->orderBy('id_mem','asc')
                ->paginate(7);

            return view('memo.lista',["memos"=>$memos,"searchText"=>$query]);
        }
    }
    public function show($id)
    {
        $memo=Memo::findOrFail($id);
        return view("memo.show",["memo"=>$memo]);
    }

    public function edit($id)
    {
        $memo=Memo::findOrFail($id);
        return view("memo.edit",["memo"=>$memo]);
    }
    public function update(Request $request, $id)
    {
        $memo=Memo::findOrFail($id);
        $memo->motivo=$request->get('motivo');
        $memo->fecha=$request->get('fecha');
        $memo->descripcion=$request->get('descripcion');
        $memo->sancion=$request->get('sancion');
        $memo->descuento=$request->get('descuento');
        $memo->usuario_mod=Auth::user()->id_usuario;
        $memo->fch_mod=Carbon::now();
        $memo->host_mod=$request->ip();
        $memo->update();
        return Redirect::to('listam');
    }


    public function destroy($id)
    {
        $memo=Memo::findOrFail($id);
        $memo->usuario_del=Auth::user()->id_usuario;
        $memo->fch_del=Carbon::now();
        $memo->host_del='127.0.0.1';
        $memo->update();
        $empleado=Empleado::where('id_empleado',$memo->id_empleado)->firstOrFail();
        $empleado->memo =($empleado->memo)-1;
        $empleado->update();
        $memo->id_empleado=null;
        $memo->update();
        $em =  new Eliminacionmem;
        $em->id_mem=$memo->id_mem;
        $em->id_empleado=$empleado->id_empleado;
        $em->documento=$empleado->documento;
        $em->usuario_ini=Auth::user()->id_usuario;
        $em->fch_ini=Carbon::now();
        $em->host_ini='127.0.0.1';
        $em->save();
        return Redirect::to('listam');
    }
    public function mail2($id){
        //
    }
}
