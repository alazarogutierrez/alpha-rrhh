<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class VinculacionController extends Controller
{
    public function index(Request $request)
    {
        if ($request)
        {
            $query=trim($request->get('searchText'));
            $vinculaciones=DB::table('vinculacion')
                ->where('documento','LIKE','%'.$query.'%')
                ->orderBy('id_vin','asc')
                ->paginate(7);

            return view('historial.vin',["vinculaciones"=>$vinculaciones,"searchText"=>$query]);
        }
    }
}
