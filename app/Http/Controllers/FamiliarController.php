<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Empleado;
use App\Familiar;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use DB;


class FamiliarController extends Controller
{
    public function index(Request $request)
    {
        if ($request)
        {
            $query=trim($request->get('searchText'));
            $familiares=DB::table('familiar')
                ->where('id_copia','LIKE','%'.$query.'%')
                ->where ('fch_del','=',null)
                ->orderBy('id_familiar','asc')
                ->paginate(7);

            return view('familiar.index',["familiares"=>$familiares,"searchText"=>$query]);
        }
    }
    public function crear($id)
    {
        $empleado = Empleado::findOrFail($id);
        return view("familiar.create", ["empleado" => $empleado]);

    }

    public function guardar(Request $request, $id)
    {
        $empleado=Empleado::findOrFail($id);
        $familiar = new Familiar;
        $familiar->primer_nombre=$request->get('primer_nombre');
        $familiar->segundo_nombre=$request->get('segundo_nombre');
        $familiar->primer_apellido=$request->get('primer_apellido');
        $familiar->segundo_apellido=$request->get('segundo_apellido');
        $familiar->tipo=$request->get('tipo');
        $familiar->documento=$request->get('documento');
        $familiar->complemento1=$request->get('complemento1');
        $familiar->cat_tipo_documento=$request->get('cat_tipo_documento');
        $familiar->cat_genero=$request->get('cat_genero');
        $familiar->fecha_nacimiento=$request->get('fecha_nacimiento');
        $edad = Carbon::parse($familiar->fecha_nacimiento)->age;
        dump($edad);
        $familiar->edad=$edad;
        $familiar->celular=$request->get('celular');
        $familiar->telefono=$request->get('telefono');
        $familiar->id_empleado=$empleado->id_empleado;
        $familiar->id_copia=$familiar->id_empleado;
        $familiar->usuario_ini=Auth::user()->id_usuario;
        $familiar->fch_ini=Carbon::now();
        $familiar->host_ini=$request->ip();
        $familiar->save();
        $count = DB::table('familiar')
            ->where('id_empleado', '=',$empleado->id_empleado)
            ->where('tipo','=',"Hij@")
            ->count();
        $casado = DB::table('familiar')
            ->where('id_empleado', '=',$empleado->id_empleado)
            ->where('tipo','=',"Espos@")
            ->count();
        $empleado->nhijos = $count;
        $empleado->save();
        if($casado>0){
            $empleado->cat_estado_civil = 'Casado';
            $empleado->update();
        }
        return Redirect::to('empleado');
    }
    public function edit($id)
    {
        $familiar=Familiar::findOrFail($id);
        return view("familiar.edit",["familiar"=>$familiar]);
    }
    public function update(Request $request, $id)
    {
        $familiar=Familiar::findOrFail($id);
        $familiar->primer_nombre=$request->get('primer_nombre');
        $familiar->segundo_nombre=$request->get('segundo_nombre');
        $familiar->primer_apellido=$request->get('primer_apellido');
        $familiar->segundo_apellido=$request->get('segundo_apellido');
        $familiar->tipo=$request->get('tipo');
        $familiar->documento=$request->get('documento');
        $familiar->complemento1=$request->get('complemento1');
        $familiar->cat_tipo_documento=$request->get('cat_tipo_documento');
        $familiar->cat_genero=$request->get('cat_genero');
        $familiar->fecha_nacimiento=$request->get('fecha_nacimiento');
        $edad = Carbon::parse($familiar->fecha_nacimiento)->age;
        dump($edad);
        $familiar->edad=$edad;
        $familiar->celular=$request->get('celular');
        $familiar->telefono=$request->get('telefono');
        $familiar->id_copia=$familiar->id_empleado;
        $familiar->usuario_ini=Auth::user()->id_usuario;
        $familiar->fch_ini=Carbon::now();
        $familiar->host_ini=$request->ip();
        $familiar->update();
        $empleado=Empleado::where('id_empleado',$familiar->id_empleado)->firstOrFail();
        $count = DB::table('familiar')
            ->where('id_empleado', '=',$empleado->id_empleado)
            ->where('tipo','=',"Hij@")
            ->count();
        $casado = DB::table('familiar')
            ->where('id_empleado', '=',$empleado->id_empleado)
            ->where('tipo','=',"Espos@")
            ->count();
        $empleado->nhijos = $count;
        $empleado->update();
        if($casado>0){
            $empleado->cat_estado_civil = 'Casado';
            $empleado->update();
        }
        return Redirect::to('familiar');
    }
    public function destroy($id)
    {
        $familiar=Familiar::findOrFail($id);
        $familiar->usuario_del=Auth::user()->id_usuario;
        $familiar->fch_del=Carbon::now();
        $familiar->host_del='127.0.0.1';
        $familiar->update();
        $empleado=Empleado::where('id_empleado',$familiar->id_empleado)->firstOrFail();
        if($familiar->tipo=="Hij@"){
            $empleado->nhijos =($empleado->nhijos)-1;
            $empleado->update();
        }
        $familiar->id_empleado=null;
        $familiar->update();
        return Redirect::to('familiar');
    }
}
