<?php

namespace App\Http\Controllers;

use App\Empleado_Seguro;
use App\Seguro;
use App\Rol;
use App\Usuario_Rol;
use App\Afp;
use App\Empleado_Afp;
use App\Area;
use App\Empleado_Area;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Empleado;
use App\User;
use App\Desvinculacion;
use App\Vinculacion;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\EmpleadoFormRequest;
use Illuminate\Support\Facades\Input;
use DB;
use Illuminate\Support\Facades\Hash;

class EmpleadoController extends Controller
{
    public function __construct()
    {

    }
    public function index(Request $request)
    {
        if ($request)
        {
            $query=trim($request->get('searchText'));
            $empleados=DB::table('empleado')
                ->where('documento','LIKE','%'.$query.'%')
                ->where ('estado','=','activo')
                ->orderBy('id_empleado','asc')
                ->paginate(7);

            return view('empleado.index',["empleados"=>$empleados,"searchText"=>$query]);
        }
    }
    public function index2(Request $request)
    {
        if ($request)
        {
            $query=trim($request->get('searchText'));
            $empleados=DB::table('empleado')
                ->where('documento','LIKE','%'.$query.'%')
                ->where ('estado','=','inactivo')
                ->orderBy('id_empleado','asc')
                ->paginate(7);

            return view('empleado.index2',["empleados"=>$empleados,"searchText"=>$query]);
        }
    }
    public function create()
    {
        $seguro = Seguro::get();
        $rol = Rol::get();
        $afp = Afp::get();
        $area = Area::get();
        return view("empleado.create")->with('seguro',$seguro)->with('rol',$rol)->with('afp',$afp)->with('area',$area);
    }
    public function store(EmpleadoFormRequest $request)
    {
        $empleado=new Empleado;
        $empleado->primer_nombre=$request->get('primer_nombre');
        $empleado->segundo_nombre=$request->get('segundo_nombre');
        $empleado->tercer_nombre=$request->get('tercer_nombre');
        $empleado->primer_apellido=$request->get('primer_apellido');
        $empleado->segundo_apellido=$request->get('segundo_apellido');
        $empleado->tercer_apellido=$request->get('tercer_apellido');
        $empleado->documento=$request->get('documento');
        $empleado->complemento1=$request->get('complemento1');
        $empleado->complemento2=$request->get('complemento2');
        $empleado->cat_tipo_documento=$request->get('cat_tipo_documento');
        $empleado->fecha_nacimiento=$request->get('fecha_nacimiento');
        $edad = Carbon::parse($empleado->fecha_nacimiento)->age;
        dump($edad);
        $empleado->edad=$edad;
        $empleado->cat_genero=$request->get('cat_genero');
        $empleado->celular=$request->get('celular');
        $empleado->telefono=$request->get('telefono');
        $empleado->direccion=$request->get('direccion');
        $empleado->cat_estado_civil=$request->get('cat_estado_civil');
        $empleado->cat_nacionalidad=$request->get('cat_nacionalidad');
        $empleado->cat_discapacidad=$request->get('cat_discapacidad');
        $empleado->estado='activo';
        if(Input::hasfile('hoja')){
            $file=Input::file('hoja');
            $name = time().$file->getClientOriginalName();
            $file->move(public_path().'/hojas/',$name);
            $empleado->hoja=$name;
        }
        $empleado->memo=0;
        $empleado->mbaja=0;
        $empleado->hv=0;
        $empleado->nhijos=0;
        $empleado->cont=0;
        $empleado->usuario_ini=Auth::user()->id_usuario;
        $empleado->fch_ini=Carbon::now();
        $empleado->host_ini=$request->ip();
        $empleado->save();

        $user =  new User;
        $pn=strtolower($empleado->primer_nombre);
        $pa=strtolower($empleado->primer_apellido);
        $pn2=substr($pn,0,2);
        $nu=$pn2."_".$pa;
        $co=$nu."@gmail.com";
        $doc=substr($empleado->documento,0,2);
        $nu2=$nu."_".$doc;
        $co2=$nu2."@gmail.com";
        $count = DB::table('usuario')
            ->where('nombre', '=', $nu)
            ->count();
        $doc2=substr($empleado->documento,2,2);
        $nu3=$nu."_".$doc2;
        $co3=$nu3."@gmail.com";
        $count2 = DB::table('usuario')
            ->where('nombre', '=', $nu2)
            ->count();
        $doc3=substr($empleado->documento,4,2);
        $nu4=$nu."_".$doc3;
        $co4=$nu4."@gmail.com";
        $count3 = DB::table('usuario')
            ->where('nombre', '=', $nu3)
            ->count();
        if($count > 0 ) {
           if ($count2 > 0 ) {
               if ($count3 > 0 ) {
                   $user->nombre = $nu4;
                   $user->email = $co4;
               }
               else {
                   $user->nombre = $nu3;
                   $user->email = $co3;
               }
           }
           else {
               $user->nombre = $nu2;
               $user->email = $co2;
           }
        }
        else {
            $user->nombre = $nu;
            $user->email = $co;
        }
        $user->password=bcrypt($request->get('documento'));
        $user->usuario_ini=Auth::user()->id_usuario;
        $user->fch_ini=Carbon::now();
        $user->host_ini=$request->ip();
        $user->save();
        $empleado->id_usuario=$user->id_usuario;
        $empleado->save();

        $emp_s= new Empleado_Seguro;
        $emp_s->id_empleado=$empleado->id_empleado;
        $emp_s->id_seguro=$request->seguro;
        $emp_s->usuario_ini=Auth::user()->id_usuario;
        $emp_s->fch_ini=Carbon::now();
        $emp_s->host_ini=$request->ip();
        $emp_s->save();

        $emp_a= new Empleado_Afp;
        $emp_a->id_empleado=$empleado->id_empleado;
        $emp_a->id_afp=$request->afp;
        $emp_a->usuario_ini=Auth::user()->id_usuario;
        $emp_a->fch_ini=Carbon::now();
        $emp_a->host_ini=$request->ip();
        $emp_a->save();

        $emp_ar= new Empleado_Area;
        $emp_ar->id_empleado=$empleado->id_empleado;
        $emp_ar->id_area=$request->area;
        $emp_ar->usuario_ini=Auth::user()->id_usuario;
        $emp_ar->fch_ini=Carbon::now();
        $emp_ar->host_ini=$request->ip();
        $emp_ar->save();

        $emp_r= new Usuario_Rol;
        $emp_r->id_usuario=$user->id_usuario;
        $emp_r->id_rol=4;
        $emp_r->usuario_ini=Auth::user()->id_usuario;
        $emp_r->fch_ini=Carbon::now();
        $emp_r->host_ini=$request->ip();
        $emp_r->save();

        $vin =  new Vinculacion;
        $vin->id_empleado=$empleado->id_empleado;
        $vin->documento=$empleado->documento;
        $vin->usuario_ini=Auth::user()->id_usuario;
        $vin->fch_ini=Carbon::now();
        $vin->host_ini=$request->ip();
        $vin->save();
        return Redirect::to('empleado');
    }
    public function show($id)
    {
        return view("empleado.show",["empleado"=>Empleado::findOrFail($id)]);
    }
    public function edit($id)
    {
        $empleado=Empleado::findOrFail($id);
        $emp_seg=Empleado_Seguro::where('id_empleado',$empleado->id_empleado)->firstOrFail();
        $emp_r=Usuario_Rol::where('id_usuario',$empleado->id_usuario)->firstOrFail();
        $emp_afp=Empleado_Afp::where('id_empleado',$empleado->id_empleado)->firstOrFail();
        $emp_ar=Empleado_Area::where('id_empleado',$empleado->id_empleado)->firstOrFail();
        return view("empleado.edit",["empleado"=>$empleado,"seguro"=>Seguro::get(),"rol"=>Rol::get(),"afp"=>Afp::get(),"area"=>Area::get(),"emp_seg"=>$emp_seg,"emp_r"=>$emp_r,"emp_afp"=>$emp_afp,"emp_ar"=>$emp_ar]);
    }
    public function update(EmpleadoFormRequest $request,$id)
    {
        $empleado=Empleado::findOrFail($id);
        $empleado->primer_nombre=$request->get('primer_nombre');
        $empleado->segundo_nombre=$request->get('segundo_nombre');
        $empleado->tercer_nombre=$request->get('tercer_nombre');
        $empleado->primer_apellido=$request->get('primer_apellido');
        $empleado->segundo_apellido=$request->get('segundo_apellido');
        $empleado->tercer_apellido=$request->get('tercer_apellido');
        $empleado->documento=$request->get('documento');
        $empleado->complemento1=$request->get('complemento1');
        $empleado->complemento2=$request->get('complemento2');
        $empleado->cat_tipo_documento=$request->get('cat_tipo_documento');
        $empleado->cat_genero=$request->get('cat_genero');
        $empleado->fecha_nacimiento=$request->get('fecha_nacimiento');
        $edad = Carbon::parse($empleado->fecha_nacimiento)->age;
        dump($edad);
        $empleado->edad=$edad;
        $empleado->celular=$request->get('celular');
        $empleado->telefono=$request->get('telefono');
        $empleado->direccion=$request->get('direccion');
        $empleado->cat_estado_civil=$request->get('cat_estado_civil');
        $empleado->cat_nacionalidad=$request->get('cat_nacionalidad');
        $empleado->cat_discapacidad=$request->get('cat_discapacidad');
        if(Input::hasfile('hoja')){
            $file=Input::file('hoja');
            $name = time().$file->getClientOriginalName();
            $file->move(public_path().'/hojas/',$name);
            $empleado->hoja=$name;
        }
        $empleado->usuario_mod=Auth::user()->id_usuario;
        $empleado->fch_mod=Carbon::now();
        $empleado->host_mod=$request->ip();
        $empleado->update();

        $emp_s=Empleado_Seguro::where('id_empleado',$empleado->id_empleado)->firstOrFail();
        $emp_s->id_seguro=$request->seguro;
        $emp_s->usuario_mod=Auth::user()->id_usuario;
        $emp_s->fch_mod=Carbon::now();
        $emp_s->host_mod=$request->ip();
        $emp_s->update();

        $emp_a=Empleado_Afp::where('id_empleado',$empleado->id_empleado)->firstOrFail();
        $emp_a->id_afp=$request->afp;
        $emp_a->usuario_mod=Auth::user()->id_usuario;
        $emp_a->fch_mod=Carbon::now();
        $emp_a->host_mod=$request->ip();
        $emp_a->update();

        $emp_ar=Empleado_Area::where('id_empleado',$empleado->id_empleado)->firstOrFail();
        $emp_ar->id_area=$request->area;
        $emp_ar->usuario_mod=Auth::user()->id_usuario;
        $emp_ar->fch_mod=Carbon::now();
        $emp_ar->host_mod=$request->ip();
        $emp_ar->update();

        $emp_r=Usuario_Rol::where('id_usuario',$empleado->id_usuario)->firstOrFail();
        $emp_r->id_rol=4;
        $emp_r->usuario_mod=Auth::user()->id_usuario;
        $emp_r->fch_mod=Carbon::now();
        $emp_r->host_mod=$request->ip();
        $emp_r->update();
        return Redirect::to('empleado');
    }
    public function destroy($id)
    {
        $empleado=Empleado::findOrFail($id);
        $empleado->usuario_del=Auth::user()->id_usuario;
        $empleado->fch_del=Carbon::now();
        $empleado->host_del='127.0.0.1';
        $empleado->estado='inactivo';
        $empleado->update();
        $user=User::where('id_usuario',$empleado->id_usuario)->firstOrFail();
        $user->nombre=null;
        $user->email=null;
        $user->password=null;
        $user->update();
        $des =  new Desvinculacion;
        $des->id_empleado=$empleado->id_empleado;
        $des->documento=$empleado->documento;
        $des->usuario_ini=Auth::user()->id_usuario;
        $des->fch_ini=Carbon::now();
        $des->host_ini='127.0.0.1';
        $des->save();
        return Redirect::to('einactivo');
    }
    public function mail($id){
            //
    }

}