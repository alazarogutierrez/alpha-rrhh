<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class RegistromemController extends Controller
{
    public function index(Request $request)
    {
        if ($request)
        {
            $query=trim($request->get('searchText'));
            $rms=DB::table('registromem')
                ->where('documento','LIKE','%'.$query.'%')
                ->orderBy('id_rm','asc')
                ->paginate(7);

            return view('historial.rm',["rms"=>$rms,"searchText"=>$query]);
        }
    }
}
