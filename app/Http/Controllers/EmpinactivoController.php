<?php

namespace App\Http\Controllers;


use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Empleado;
use App\User;
use App\Baja;
use App\Reincorporacion;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\EmpleadoFormRequest;
use DB;


class EmpinactivoController extends Controller
{
    public function __construct()
    {

    }

    public function index(Request $request)
    {
        if ($request) {
            $query = trim($request->get('searchText'));
            $empleados = DB::table('empleado')
                ->where('documento', 'LIKE', '%' . $query . '%')
                ->where('estado', '=', 'inactivo')
                ->orderBy('id_empleado', 'asc')
                ->paginate(7);

            return view('empleado.index2', ["empleados" => $empleados, "searchText" => $query]);
        }
    }

    public function crear($id)
    {
        $empleado = Empleado::findOrFail($id);
        $count = DB::table('baja')
            ->where('id_empleado', '=',$empleado->id_empleado)
            ->count();
        if($count > 0 ) {
            return Redirect::to('einactivo');
        }
        else {
            return view("baja.create", ["empleado" => $empleado]);
        }
    }

    public function guardar(Request $request, $id)
    {
        $empleado=Empleado::findOrFail($id);
        $baja = new Baja;
        $baja->fecha=$request->get('fecha');
        $baja->motivo=$request->get('motivo');
        $baja->monto=$request->get('monto');
        $baja->id_empleado=$empleado->id_empleado;
        $baja->usuario_ini=Auth::user()->id_usuario;
        $baja->fch_ini=Carbon::now();
        $baja->host_ini=$request->ip();
        $baja->save();
        $empleado->mbaja=$baja->motivo;
        $empleado->save();
        return Redirect::to('einactivo');
    }

    public function show($id)
    {
        return view("empleado.show", ["empleado" => Empleado::findOrFail($id)]);
    }

    public function edit($id)
    {
        $empleado=Empleado::findOrFail($id);
        $baja=Baja::where('id_empleado',$empleado->id_empleado)->firstOrFail();
        return view("baja.edit",["baja"=>$baja,"empleado"=>$empleado]);
    }

    public function update(Request $request, $id)
    {
        $baja=Baja::findOrFail($id);
        $baja->fecha=$request->get('fecha');
        $baja->motivo=$request->get('motivo');
        $baja->monto=$request->get('monto');
        $baja->usuario_mod=Auth::user()->id_usuario;
        $baja->fch_mod=Carbon::now();
        $baja->host_mod=$request->ip();
        $baja->update();
        $empleado=Empleado::where('id_empleado',$baja->id_empleado)->firstOrFail();
        $empleado->mbaja=$baja->motivo;
        $empleado->update();
        return Redirect::to('einactivo');
    }

    public function destroy($id)
    {
        $empleado = Empleado::findOrFail($id);
        $empleado->usuario_del=null;
        $empleado->fch_del = null;
        $empleado->host_del=null;
        $empleado->estado = 'activo';
        $empleado->mbaja = 6;
        $empleado->update();
        $user=User::where('id_usuario',$empleado->id_usuario)->firstOrFail();
        $pn=strtolower($empleado->primer_nombre);
        $pa=strtolower($empleado->primer_apellido);
        $pn2=substr($pn,0,2);
        $nu=$pn2."_".$pa;
        $co=$nu."@gmail.com";
        $doc=substr($empleado->documento,0,2);
        $nu2=$nu."_".$doc;
        $co2=$nu2."@gmail.com";
        $count = DB::table('usuario')
            ->where('nombre', '=', $nu)
            ->count();
        $doc2=substr($empleado->documento,2,2);
        $nu3=$nu."_".$doc2;
        $co3=$nu3."@gmail.com";
        $count2 = DB::table('usuario')
            ->where('nombre', '=', $nu2)
            ->count();
        $doc3=substr($empleado->documento,4,2);
        $nu4=$nu."_".$doc3;
        $co4=$nu4."@gmail.com";
        $count3 = DB::table('usuario')
            ->where('nombre', '=', $nu3)
            ->count();
        if($count > 0 ) {
            if ($count2 > 0 ) {
                if ($count3 > 0 ) {
                    $user->nombre = $nu4;
                    $user->email = $co4;
                }
                else {
                    $user->nombre = $nu3;
                    $user->email = $co3;
                }
            }
            else {
                $user->nombre = $nu2;
                $user->email = $co2;
            }
        }
        else {
            $user->nombre = $nu;
            $user->email = $co;
        }
        $user->password=bcrypt($empleado->documento);
        $user->update();
        $re =  new Reincorporacion;
        $re->id_empleado=$empleado->id_empleado;
        $re->documento=$empleado->documento;
        $re->usuario_ini=Auth::user()->id_usuario;
        $re->fch_ini=Carbon::now();
        $re->host_ini='127.0.0.1';
        $re->save();
        return Redirect::to('empleado');
    }

}