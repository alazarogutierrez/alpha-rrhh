<?php

namespace App\Http\Controllers;

use App\Empleado;
use App\EmpleadoCapacitacion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Capacitacion;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;

class CursoAprobacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request)
        {
            $query=trim($request->get('searchText'));
            $cursos =$cursos= DB::table('capacitacion')
                ->join('capacitacion_propuesta','capacitacion_propuesta.id_capacitacion', '=', 'capacitacion.id_capacitacion')
                ->where('capacitacion_propuesta.fch_del','=',null)
                ->where('capacitacion.fch_del','=',null)
                ->where('capacitacion.aprobacion','=',1)
                ->where('capacitacion.segunda_aprobacion','=',0)
                ->where('capacitacion_propuesta.eleccion','=',1)
                ->orderby('capacitacion.fch_ini','DESC')
                ->paginate(8);
            return view('curso_aprobacion_fin.index',["cursos"=>$cursos,"searchText"=>$query]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $curso=Capacitacion::findOrFail($id);
        return view("curso_aprobacion.aprobar",['curso'=>$curso]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $curso = Capacitacion::findOrFail($id);
        $curso->segunda_aprobacion=1;
        $curso->segunda_descripcion_aprobacion=$request->observacion;
        $curso->usuario_mod=Auth::user()->id_usuario;
        $curso->fch_mod=Carbon::now();
        $curso->host_mod=$request->ip();
        $curso->update();
        $empleado = Empleado::all();
        foreach ($empleado as $emp){
            $empleado_capacitacion = new EmpleadoCapacitacion;
            $empleado_capacitacion->id_empleado=$emp->id_empleado;
            $empleado_capacitacion->id_capacitacion=$id;
            $empleado_capacitacion->usuario_ini=Auth::user()->id_usuario;
            $empleado_capacitacion->fch_ini=Carbon::now();
            $empleado_capacitacion->host_ini=$request->ip();
            $empleado_capacitacion->save();
        }
        return Redirect::to('curso_aprobacion');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,Request $request)
    {
        $curso = Capacitacion::findOrFail($id);
        $curso->segunda_aprobacion=2;
        $curso->segunda_descripcion_aprobacion=$request->observacion;
        $curso->usuario_mod=Auth::user()->id_usuario;
        $curso->fch_mod=Carbon::now();
        $curso->host_mod=$request->ip();
        $curso->update();
        return Redirect::to('curso_aprobacion');
    }
    public function aceptar($id,Request $request){

        /*$curso = Capacitacion::findOrFail($id);
        $curso->segunda_aprobacion=1;
        $curso->segunda_descripcion_aprobacion=$request->observacion;
        $curso->usuario_mod=Auth::user()->id_usuario;
        $curso->fch_mod=Carbon::now();
        $curso->host_mod=$request->ip();
        $curso->update();

        $empleado = Empleado::all();
        foreach ($empleado as $emp){
            $empleado_capacitacion = new EmpleadoCapacitacion;
            $empleado_capacitacion->id_empleado=$emp->id_empleado;
            $empleado_capacitacion->id_capacitacion=$id;
            $empleado_capacitacion->usuario_ini=Auth::user()->id_usuario;
            $empleado_capacitacion->fch_ini=Carbon::now();
            $empleado_capacitacion->host_ini=$request->ip();
            $empleado_capacitacion->save();
        }*/
        //return Redirect::to('curso_aprobacion');
    }
}
