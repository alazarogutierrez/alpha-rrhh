<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Capacitacion;
use App\CapacitacionPropuesta;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Carbon\Carbon;

class CursoPropuestasController extends Controller
{
    public function index(){
        //
    }
    public function ver_curso($id){
        $curso = Capacitacion::findOrFail($id);
        $curso2 =$cursos= DB::table('capacitacion')
            ->join('capacitacion_propuesta','capacitacion_propuesta.id_capacitacion', '=', 'capacitacion.id_capacitacion')
            ->where('capacitacion_propuesta.fch_del','=',null)
            ->where('capacitacion.fch_del','=',null)
            ->where('capacitacion.id_capacitacion','=',$id)
            ->where('capacitacion.aprobacion','=',0)
            ->orderby('capacitacion.fch_ini','DESC')
            ->paginate(8);
        return view('curso_propuestas.index',['curso'=>$curso,'curso2'=>$curso2]);
    }

}
