<?php

namespace App\Http\Controllers;

use App\Capacitacion;
use App\EmpleadoCapacitacion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;

class CalificacionEmpleadoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $query = DB::select('
                                SELECT a.primer_nombre,a.primer_apellido,a.documento, c.sesiones,d.nota_final,d.asistencia,d.id_empleado_capacitacion
                                FROM empleado a, capacitacion b , capacitacion_propuesta c, empleado_capacitacion d
                                WHERE
                                    a.id_empleado = d.id_empleado
                                AND b.id_capacitacion = d.id_capacitacion
                                AND b.id_capacitacion = c.id_capacitacion
                                AND d.estado_inscripcion = 1
                                AND c.eleccion = 1
                                AND b.id_capacitacion = ?
        ',[$id]);
        $query2= Capacitacion::findOrFail($id);
        return view("curso_calificacion_asistencia.index",['alumnos'=>$query,"curso"=>$query2]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $emp_nota = EmpleadoCapacitacion::findOrfail($id);
        $emp_nota->nota_final=$request->nota_final;
        $emp_nota->asistencia=$request->asistencia;
        $emp_nota->usuario_mod=Auth::user()->id_usuario;
        $emp_nota->fch_mod=Carbon::now();
        $emp_nota->host_mod=$request->ip();
        $emp_nota->update();
        return Redirect::to('calificacion_empleado/'.$emp_nota->id_capacitacion);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
