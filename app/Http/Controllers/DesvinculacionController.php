<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class DesvinculacionController extends Controller
{
    public function index(Request $request)
    {
        if ($request)
        {
            $query=trim($request->get('searchText'));
            $desvinculaciones=DB::table('desvinculacion')
                ->where('documento','LIKE','%'.$query.'%')
                ->orderBy('id_des','asc')
                ->paginate(7);

            return view('historial.des',["desvinculaciones"=>$desvinculaciones,"searchText"=>$query]);
        }
    }
}
