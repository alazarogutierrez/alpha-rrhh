<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Empleado;
use App\Contrato;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use DB;

class ContratoController extends Controller
{
    public function __construct()
    {

    }

    public function index(Request $request)
    {
        if ($request)
        {
            $query=trim($request->get('searchText'));
            $empleados=DB::table('empleado')
                ->where('documento','LIKE','%'.$query.'%')
                ->where ('estado','=','activo')
                ->orderBy('id_empleado','asc')
                ->paginate(7);

            return view('contrato.index',["empleados"=>$empleados,"searchText"=>$query]);
        }
    }


    public function crear($id)
    {
        $empleado = Empleado::findOrFail($id);
        $count = DB::table('contrato')
            ->where('id_empleado', '=',$empleado->id_empleado)
            ->count();
        if($count > 0 ) {
            return Redirect::to('contrato');
        }
        else {
            return view("contrato.create", ["empleado" => $empleado]);
        }
    }

    public function guardar(Request $request, $id)
    {
        $empleado=Empleado::findOrFail($id);
        $contrato = new Contrato;
        $contrato->cat_contrato=$request->get('cat_contrato');
        $contrato->fechai=$request->get('fechai');
        $contrato->fechaf=$request->get('fechaf');
        $contrato->cargo=$request->get('cargo');
        $contrato->salario=$request->get('salario');
        $contrato->multa=$request->get('multa');
        $contrato->id_empleado=$empleado->id_empleado;
        $contrato->usuario_ini=Auth::user()->id_usuario;
        $contrato->fch_ini=Carbon::now();
        $contrato->host_ini=$request->ip();
        $contrato->save();
        $count = DB::table('contrato')
            ->where('id_empleado', '=',$empleado->id_empleado)
            ->count();
        $empleado->cont = $count;
        $empleado->save();
        return Redirect::to('contrato');
    }

    public function show($id)
    {
        $empleado=Empleado::findOrFail($id);
        $contrato=Contrato::where('id_empleado',$empleado->id_empleado)->firstOrFail();
        return view("contrato.show",["contrato"=>$contrato]);
    }

    public function edit($id)
    {
        $empleado=Empleado::findOrFail($id);
        $contrato=Contrato::where('id_empleado',$empleado->id_empleado)->firstOrFail();
        return view("contrato.edit",["contrato"=>$contrato,"empleado"=>$empleado]);
    }

    public function update(Request $request, $id)
    {
        $contrato=Contrato::findOrFail($id);
        $contrato->cat_contrato=$request->get('cat_contrato');
        $contrato->fechai=$request->get('fechai');
        $contrato->fechaf=$request->get('fechaf');
        $contrato->cargo=$request->get('cargo');
        $contrato->salario=$request->get('salario');
        $contrato->multa=$request->get('multa');
        $contrato->usuario_mod=Auth::user()->id_usuario;
        $contrato->fch_mod=Carbon::now();
        $contrato->host_mod=$request->ip();
        $contrato->update();
        return Redirect::to('contrato');
    }

    public function destroy($id)
    {

    }
}
