<?php

namespace App\Http\Controllers;

use App\User;
use App\Empleado;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\EmpleadoFormRequest;
use Carbon\Carbon;

class InfoController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $empleado=Empleado::where('id_usuario',$user->id_usuario)->firstOrFail();

        return view("info.index")->with('empleado',$empleado);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EmpleadoFormRequest $request, $id)
    {
        $empleado=Empleado::findOrFail($id);
        $empleado->primer_nombre=$request->get('primer_nombre');
        $empleado->segundo_nombre=$request->get('segundo_nombre');
        $empleado->tercer_nombre=$request->get('tercer_nombre');
        $empleado->primer_apellido=$request->get('primer_apellido');
        $empleado->segundo_apellido=$request->get('segundo_apellido');
        $empleado->tercer_apellido=$request->get('tercer_apellido');
        $empleado->documento=$request->get('documento');
        $empleado->complemento1=$request->get('complemento1');
        $empleado->complemento2=$request->get('complemento2');
        $empleado->cat_tipo_documento=$request->get('cat_tipo_documento');
        $empleado->cat_genero=$request->get('cat_genero');
        $empleado->fecha_nacimiento=$request->get('fecha_nacimiento');
        $edad = Carbon::parse($empleado->fecha_nacimiento)->age;
        dump($edad);
        $empleado->edad=$edad;
        $empleado->celular=$request->get('celular');
        $empleado->telefono=$request->get('telefono');
        $empleado->direccion=$request->get('direccion');
        $empleado->cat_estado_civil=$request->get('cat_estado_civil');
        $empleado->cat_nacionalidad=$request->get('cat_nacionalidad');
        $empleado->cat_discapacidad=$request->get('cat_discapacidad');
        $empleado->usuario_ini=Auth::user()->id_usuario;
        $empleado->fch_ini=Carbon::now();
        $empleado->host_ini=$request->ip();
        $empleado->update();
        return Redirect::to('home');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}