<?php

namespace App\Http\Controllers;

use App\Capacitacion;
use App\CapacitacionPropuesta;
use App\Horario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Carbon\Carbon;

class CursoPropuestaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request)
        {
            $query=trim($request->get('searchText'));
            $cursos= DB::table('capacitacion')
                ->where('fch_del','=',null)
                ->where('tema','LIKE','%'.$query.'%')
                ->where('aprobacion','=',1)
                ->where('segunda_aprobacion','=',0)
                ->orderby('fch_ini','DESc')
                ->paginate(8);
            return view('curso_propuesta.index',["cursos"=>$cursos,"searchText"=>$query]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $propuesta = new CapacitacionPropuesta;
        $propuesta->instituto=$request->institucion;
        $propuesta->costo=$request->costo;
        $propuesta->fecha_inicio=$request->fecha_inicio;
        $propuesta->fecha_fin=$request->fecha_fin;
        $propuesta->direccion=$request->direccion;
        $file=Input::file('descripcion');
        $name = time().$file->getClientOriginalName();
        $file->move(public_path().'/propuestas/',$request->id.Carbon::now().$name);
        $propuesta->descripcion=$request->id.Carbon::now().$name;
        $file=Input::file('cv_docentes');
        $name = time().$file->getClientOriginalName();
        $file->move(public_path().'/cv_docentes/',$request->id.Carbon::now().$name);
        $propuesta->descripcion_cv=$request->id.Carbon::now().$name;
        $propuesta->eleccion=0;
        $propuesta->sesiones=$request->sesiones;
        $propuesta->id_capacitacion=$request->id;
        $propuesta->usuario_ini=Auth::user()->id_usuario;
        $propuesta->fch_ini=Carbon::now();
        $propuesta->host_ini=$request->ip();
        $propuesta->save();
        $curso= Capacitacion::findOrFail($request->id);
        $curso->usuario_mod=Auth::user()->id_usuario;
        $curso->fch_mod=Carbon::now();
        $curso->host_mod='127.0.0.1';
        $curso->update();
        $arr=explode(",",$request->semana);
        for($i=0 ; $i<count($arr) ; $i++)
        {
            $arr2 = explode(' ',$arr[$i]);
            $arr3 = explode('-',$arr2[1]);
            $horario = new Horario;
            $horario->dia=$arr2[0];
            $horario->hora_ini=$arr3[0].":00";
            $horario->hora_fin=$arr3[1].":00";
            $horario->id_capacitacion_propuesta=$propuesta->id_capacitacion_propuesta;
            $horario->usuario_ini=Auth::user()->id_usuario;
            $horario->fch_ini=Carbon::now();
            $horario->host_ini=$request->ip();
            $horario->save();
        }
        //return Redirect::route('curso_propuestas.ver_curso',$request->id);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $propuesta=CapacitacionPropuesta::findOrFail($id);
        $curso=Capacitacion::findOrFail($propuesta->id_capacitacion);
        $horarios=DB::select("select * from horario a where a.id_capacitacion_propuesta = ? and a.fch_del IS NULL",[$propuesta->id_capacitacion_propuesta]);
        return view("curso_propuesta.editar",['propuesta'=>$propuesta,'horarios'=>$horarios,'curso'=>$curso]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $curso=Capacitacion::findOrFail($id);
        return view("curso_propuesta.agregar",['curso'=>$curso]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $propuesta = CapacitacionPropuesta::findOrFail($request->id_capacitacion_propuesta);
        $propuesta->instituto=$request->institucion;
        $propuesta->costo=$request->costo;
        $propuesta->fecha_inicio=$request->fecha_inicio;
        $propuesta->fecha_fin=$request->fecha_fin;
        $propuesta->direccion=$request->direccion;
        /*$file=Input::file('descripcion');
        $name = time().$file->getClientOriginalName();
        $file->move(public_path().'/propuestas/',$request->id.Carbon::now().$name);
        $propuesta->descripcion=$request->id.Carbon::now().$name;
        $file=Input::file('cv_docentes');
        $name = time().$file->getClientOriginalName();
        $file->move(public_path().'/cv_docentes/',$request->id.Carbon::now().$name);
        $propuesta->descripcion_cv=$request->id.Carbon::now().$name;*/
        $propuesta->eleccion=0;
        $propuesta->sesiones=$request->sesiones;
        $propuesta->id_capacitacion=$request->id;
        $propuesta->usuario_mod=Auth::user()->id_usuario;
        $propuesta->fch_mod=Carbon::now();
        $propuesta->host_mod=$request->ip();
        $propuesta->update();
        $arr=explode(",",$request->semana);
        Horario::where('id_capacitacion_propuesta','=',$propuesta->id_capacitacion_propuesta)
                ->update(['usuario_del'=>Auth::user()->id_usuario,'fch_del'=>Carbon::now(),'host_del'=>$request->ip()]);
        for($i=0 ; $i<count($arr) ; $i++)
        {
            $arr2 = explode(' ',$arr[$i]);
            $arr3 = explode('-',$arr2[1]);
            $horario = new Horario;
            $horario->dia=$arr2[0];
            $horario->hora_ini=$arr3[0].":00";
            $horario->hora_fin=$arr3[1].":00";
            $horario->id_capacitacion_propuesta=$propuesta->id_capacitacion_propuesta;
            $horario->usuario_ini=Auth::user()->id_usuario;
            $horario->fch_ini=Carbon::now();
            $horario->host_ini=$request->ip();
            $horario->save();
        }
    }
    public function actualizar_propuesta(Request $request,$id_prop,$id_cap){
        $propuesta = CapacitacionPropuesta::findOrFail($id_prop);
        $propuesta->instituto=$request->institucion;
        $propuesta->costo=$request->costo;
        $propuesta->fecha_inicio=$request->fecha_inicio;
        $propuesta->fecha_fin=$request->fecha_fin;
        $propuesta->direccion=$request->direccion;
        $file=Input::file('descripcion');
        if(isset($file)){
            echo "hola";
            $file=Input::file('descripcion');
            $name = time().$file->getClientOriginalName();
            $file->move(public_path().'/propuestas/',$request->id.Carbon::now().$name);
            $propuesta->descripcion=$request->id.Carbon::now().$name;
        }
        $file=Input::file('cv_docentes');
        if(isset($file)){
            $file=Input::file('cv_docentes');
            $name = time().$file->getClientOriginalName();
            $file->move(public_path().'/cv_docentes/',$request->id.Carbon::now().$name);
            $propuesta->descripcion_cv=$request->id.Carbon::now().$name;
        }
        $propuesta->eleccion=0;
        $propuesta->sesiones=$request->sesiones;
        $propuesta->id_capacitacion=$id_cap;
        $propuesta->usuario_mod=Auth::user()->id_usuario;
        $propuesta->fch_mod=Carbon::now();
        $propuesta->host_mod=$request->ip();
        $propuesta->update();
        $arr=explode(",",$request->semana);
        Horario::where('id_capacitacion_propuesta','=',$propuesta->id_capacitacion_propuesta)
            ->update(['usuario_del'=>Auth::user()->id_usuario,'fch_del'=>Carbon::now(),'host_del'=>$request->ip()]);
        for($i=0 ; $i<count($arr) ; $i++)
        {
            $arr2 = explode(' ',$arr[$i]);
            $arr3 = explode('-',$arr2[1]);
            $horario = new Horario;
            $horario->dia=$arr2[0];
            $horario->hora_ini=strlen($arr3[0])==5?($arr3[0].":00"):$arr3[0];
            $horario->hora_fin=strlen($arr3[1])==5?($arr3[1].":00"):$arr3[1];
            $horario->id_capacitacion_propuesta=$propuesta->id_capacitacion_propuesta;
            $horario->usuario_ini=Auth::user()->id_usuario;
            $horario->fch_ini=Carbon::now();
            $horario->host_ini=$request->ip();
            $horario->save();
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       //
    }
    public function elegir($id_curso,$id_propuesta,Request $request){
            $curso=Capacitacion::findOrFail($id_curso);
            $curso->descripcion_aprobacion=$request->observacion;
            $curso->aprobacion=1;
            $curso->usuario_mod=Auth::user()->id_usuario;
            $curso->fch_mod=Carbon::now();
            $curso->host_mod='127.0.0.1';
            $curso->update();
            $propuesta=CapacitacionPropuesta::findOrFail($id_propuesta);
            $propuesta->eleccion=1;
            $propuesta->usuario_mod=Auth::user()->id_usuario;
            $propuesta->fch_mod=Carbon::now();
            $propuesta->host_mod='127.0.0.1';
            $propuesta->update();
            return Redirect::to('curso_solicitud');
    }
    public function eliminar_propuesta($id_curso,$id_propuesta){
        $curso = CapacitacionPropuesta::findOrFail($id_propuesta);
        $curso->usuario_del=Auth::user()->id_usuario;
        $curso->fch_del=Carbon::now();
        $curso->host_del='127.0.0.1';
        $curso->update();
        return Redirect::route('curso_propuestas.ver_curso',$id_curso);
    }
}
