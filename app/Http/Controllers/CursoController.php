<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Capacitacion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use phpDocumentor\Reflection\Types\Null_;
use Psr\Log\NullLogger;

class CursoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request)
        {
            $query=trim($request->get('searchText'));
            $cursos= DB::table('capacitacion')
                    //->join('capacitacion_propuesta','capacitacion_propuesta.id_capacitacion', '=', 'capacitacion.id_capacitacion')
                    ->where('capacitacion.fch_del','=',null)
                    ->where('capacitacion.tema','LIKE','%'.$query.'%')
                    ->whereBetween('capacitacion.aprobacion',[0,1])
                    ->where('capacitacion.segunda_aprobacion','=',0)
                    ->orderby('capacitacion.fch_ini','DESc')
                    ->paginate(8);
            return view('cursos.index',["cursos"=>$cursos,"searchText"=>$query]);
        }
    }
    public function rechazado(Request $request)
    {
        if ($request)
        {
            $query=trim($request->get('searchText'));
            $cursos= DB::table('capacitacion')
                //->join('capacitacion_propuesta','capacitacion_propuesta.id_capacitacion', '=', 'capacitacion.id_capacitacion')
                ->where('capacitacion.fch_del','=',null)
                ->where('capacitacion.tema','LIKE','%'.$query.'%')
                ->where('capacitacion.aprobacion','=',2)
                ->orWhere('capacitacion.segunda_aprobacion','=',2)
                ->orderby('capacitacion.fch_ini','DESc')
                ->paginate(8);
            return view('cursos.index_rechazado',["cursos"=>$cursos,"searchText"=>$query]);
        }
    }
    public function aprobado(Request $request)
    {
        if ($request)
        {
            $query=trim($request->get('searchText'));
            $cursos= DB::table('capacitacion')
                ->join('capacitacion_propuesta','capacitacion_propuesta.id_capacitacion', '=', 'capacitacion.id_capacitacion')
                ->where('capacitacion.fch_del','=',null)
                ->where('capacitacion.tema','LIKE','%'.$query.'%')
                ->where('capacitacion.aprobacion','=',1)
                ->where('capacitacion.segunda_aprobacion','=',1)
                ->where('capacitacion_propuesta.eleccion','=',1)
                ->orderby('capacitacion.fch_ini','DESc')
                ->paginate(8);
            return view('cursos.index_aprobado',["cursos"=>$cursos,"searchText"=>$query]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cursos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validar = $request->validate([
            'tema' => 'required|max:50',
            'capacidad' => 'required|integer',
            'descripcion' => 'required',
            'pre_solicitado' => 'required',
        ]);
        if($validar==false){
            return Redirect::to('curso.create');
        }
        $curso = new Capacitacion;
        $curso->tema=$request->tema;
        $curso->estudiantes=$request->capacidad;
        $curso->descripcion=$request->descripcion;
        $curso->aprobacion=0;
        $curso->segunda_aprobacion=0;
        $curso->presupuesto_solicitado=$request->pre_solicitado;
        $curso->usuario_ini=Auth::user()->id_usuario;
        $curso->fch_ini=Carbon::now();
        $curso->host_ini=$request->ip();
        $curso->save();
        return Redirect::to('curso');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $curso=Capacitacion::findOrFail($id);
        return view("cursos.edit",['curso'=>$curso]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validar = $request->validate([
            'tema' => 'required|max:50',
            'capacidad' => 'required|integer',
            'descripcion' => 'required',
            'pre_solicitado' => 'required|between:1,100000',
        ]);
        if($validar==false){
            return Redirect::to('curso');
        }
        $curso = Capacitacion::findOrFail($id);
        $curso->tema=$request->tema;
        $curso->estudiantes=$request->capacidad;
        $curso->presupuesto_solicitado= $request->pre_solicitado;
        $curso->descripcion=$request->descripcion;
        $curso->usuario_mod=Auth::user()->id_usuario;
        $curso->fch_mod=Carbon::now();
        $curso->host_mod=$request->ip();
        if($curso->aprobacion==0){
            $curso->update();
        }
        return Redirect::to('curso');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $curso=Capacitacion::findOrFail($id);
        $curso->usuario_del=Auth::user()->id_usuario;
        $curso->fch_del=Carbon::now();
        $curso->host_del=$request->ip();
        if($curso->aprobacion==0){
            $curso->update();
        }
        return Redirect::to('curso');
    }
}
