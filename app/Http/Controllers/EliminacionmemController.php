<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class EliminacionmemController extends Controller
{
    public function index(Request $request)
    {
        if ($request)
        {
            $query=trim($request->get('searchText'));
            $ems=DB::table('eliminacionmem')
                ->where('documento','LIKE','%'.$query.'%')
                ->orderBy('id_em','asc')
                ->paginate(7);

            return view('historial.em',["ems"=>$ems,"searchText"=>$query]);
        }
    }

}
