<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\DB;

class ReporteCursoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Horarios =DB::select("");
        return view('resultados_cursos.index');
        //$pdf = PDF::loadView('resultados_cursos.index');
        //return $pdf->download('listado.pdf');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $curso = DB::select("
                SELECT a.tema,a.estudiantes,a.presupuesto_solicitado,b.instituto,b.costo,b.fecha_inicio,b.fecha_fin,b.direccion,sesiones
                FROM capacitacion a, capacitacion_propuesta b 
                WHERE 
                     a.id_capacitacion = b.id_capacitacion 
                     and 
                     b.eleccion = 1
                     and 
                     a.id_capacitacion = ? 
        ",[$id]);
        $horarios =DB::select("
                    SELECT a.dia,a.hora_ini,a.hora_fin
                    FROM horario a, capacitacion b , capacitacion_propuesta c
                    WHERE
                       b.id_capacitacion = c.id_capacitacion
                       and c.id_capacitacion_propuesta =a.id_capacitacion_propuesta
                       and c.eleccion=1
                       and b.id_capacitacion = ?
                       and a.fch_del is null
        ",[$id]);
        $notas = DB::select("
                        select t.rango , count(*)
                        from (
                              select a.id_empleado,
                                 case when a.nota_final >= 0 and a.nota_final< 25 then '0-25'
                                 when a.nota_final >= 25 and a.nota_final< 50 then '25-50'
                                 when a.nota_final >= 50 and a.nota_final< 75 then '50-75'
                                 else '75-100' end as rango
                             from empleado_capacitacion a
                             where a.id_capacitacion = ?
                                   and a.estado_inscripcion = 1) t
                        group by t.rango
        
        ",[$id]);
        $nota_minima = DB::select("
                SELECT  b.primer_nombre , b.primer_apellido,b.documento,a.nota_final
                FROM empleado_capacitacion a, empleado b
                WHERE
                    b.id_empleado = a.id_empleado
                    and
                    a.id_capacitacion = ?
                    AND
                    a.estado_inscripcion = 1
                    AND a.nota_final = (SELECT min(c.nota_final)
                                        FROM empleado_capacitacion c
                                        WHERE c.id_capacitacion = a.id_capacitacion
                                              and c.estado_inscripcion = 1) 
        ",[$id]);
        $nota_maxima = DB::select("
                SELECT  b.primer_nombre , b.primer_apellido,b.documento,a.nota_final
                FROM empleado_capacitacion a, empleado b
                WHERE
                    b.id_empleado = a.id_empleado
                    and
                    a.id_capacitacion = ?
                    AND
                    a.estado_inscripcion = 1
                    AND a.nota_final = (SELECT max(c.nota_final)
                                        FROM empleado_capacitacion c
                                        WHERE c.id_capacitacion = a.id_capacitacion
                                              and c.estado_inscripcion = 1) 
        ",[$id]);
        $empleado = DB::select("
                 SELECT b.primer_nombre,b.primer_apellido,b.documento,a.nota_final,d.sesiones,a.asistencia,c.tema,c.fch_ini,
                      (CASE WHEN a.nota_final >= 51 THEN  'APROBADO'
                                     ELSE 'REPROBADO'END) as desempeno

                FROM empleado_capacitacion a, empleado b, capacitacion c,capacitacion_propuesta d
                WHERE a.id_empleado = b.id_empleado
                and a.id_capacitacion = c.id_capacitacion
                and d.id_capacitacion=a.id_capacitacion
                and a.estado_inscripcion = 1
                and a.fch_del is null
                and d.eleccion=1
                and c.id_capacitacion = ?
        ",[$id]);
        return view('resultados_cursos.index',['horarios'=>$horarios,
                                                        'notas'=>$notas,
                                                        'nota_minima'=>$nota_minima,
                                                        'nota_maxima'=>$nota_maxima,
                                                        'curso'=>$curso,
                                                        'id'=>$id,
                                                        'empleado'=>$empleado]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $curso = DB::select("
                SELECT a.tema,a.estudiantes,a.presupuesto_solicitado,b.instituto,b.costo,b.fecha_inicio,b.fecha_fin,b.direccion,sesiones
                FROM capacitacion a, capacitacion_propuesta b 
                WHERE 
                     a.id_capacitacion = b.id_capacitacion 
                     and 
                     b.eleccion = 1
                     and 
                     a.id_capacitacion = ? 
        ",[$id]);
        $horarios =DB::select("
                    SELECT a.dia,a.hora_ini,a.hora_fin
                    FROM horario a, capacitacion b , capacitacion_propuesta c
                    WHERE
                       b.id_capacitacion = c.id_capacitacion
                       and c.id_capacitacion_propuesta =a.id_capacitacion_propuesta
                       and c.eleccion=1
                       and b.id_capacitacion = ?
                       and a.fch_del is null
        ",[$id]);
        $notas = DB::select("
                        select t.rango , count(*)
                        from (
                              select a.id_empleado,
                                 case when a.nota_final >= 0 and a.nota_final< 25 then '0-25'
                                 when a.nota_final >= 25 and a.nota_final< 50 then '25-50'
                                 when a.nota_final >= 50 and a.nota_final< 75 then '50-75'
                                 else '75-100' end as rango
                             from empleado_capacitacion a
                             where a.id_capacitacion = ?
                                   and a.estado_inscripcion = 1) t
                        group by t.rango
        
        ",[$id]);
        $nota_minima = DB::select("
                SELECT  b.primer_nombre , b.primer_apellido,b.documento,a.nota_final
                FROM empleado_capacitacion a, empleado b
                WHERE
                    b.id_empleado = a.id_empleado
                    and
                    a.id_capacitacion = ?
                    AND
                    a.estado_inscripcion = 1
                    AND a.nota_final = (SELECT min(c.nota_final)
                                        FROM empleado_capacitacion c
                                        WHERE c.id_capacitacion = a.id_capacitacion
                                              and c.estado_inscripcion = 1) 
        ",[$id]);
        $nota_maxima = DB::select("
                SELECT  b.primer_nombre , b.primer_apellido,b.documento,a.nota_final
                FROM empleado_capacitacion a, empleado b
                WHERE
                    b.id_empleado = a.id_empleado
                    and
                    a.id_capacitacion = ?
                    AND
                    a.estado_inscripcion = 1
                    AND a.nota_final = (SELECT max(c.nota_final)
                                        FROM empleado_capacitacion c
                                        WHERE c.id_capacitacion = a.id_capacitacion
                                              and c.estado_inscripcion = 1) 
        ",[$id]);
        $empleado = DB::select("
               SELECT b.primer_nombre,b.primer_apellido,b.documento,a.nota_final,d.sesiones,a.asistencia,c.tema,c.fch_ini,
                      (CASE WHEN a.nota_final >= 51 THEN  'APROBADO'
                                     ELSE 'REPROBADO'END) as desempeno

                FROM empleado_capacitacion a, empleado b, capacitacion c,capacitacion_propuesta d
                WHERE a.id_empleado = b.id_empleado
                and a.id_capacitacion = c.id_capacitacion
                and d.id_capacitacion=a.id_capacitacion
                and a.estado_inscripcion = 1
                and a.fch_del is null
                and d.eleccion=1
                and c.id_capacitacion = ?
        ",[$id]);
        $pdf = PDF::loadView('resultados_cursos.reporte',['horarios'=>$horarios,
            'notas'=>$notas,
            'nota_minima'=>$nota_minima,
            'nota_maxima'=>$nota_maxima,
            'curso'=>$curso,
            'empleado'=>$empleado]);
        return $pdf->download('reporte.pdf');
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
