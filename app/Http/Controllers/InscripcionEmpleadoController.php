<?php

namespace App\Http\Controllers;

use App\Capacitacion;
use App\Empleado;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\EmpleadoCapacitacion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class InscripcionEmpleadoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        if ($request)
        {
            $query=trim($request->get('searchText'));

            /*$empleado = DB::table('empleado_capacitacion')
            ->join('empleado','empleado.id_empleado', '=', 'empleado_capacitacion.id_empleado')
            ->join('capacitacion','capacitacion.id_capacitacion', '=', 'empleado_capacitacion.id_capacitacion')
            ->where('empleado_capacitacion.id_capacitacion','=',2)
            ->where('empleado_capacitacion.estado_inscripcion','=',0)
            ->where('capacitacion.fch_del','=',null)
            ->where('empleado.primer_nombre','LIKE','%'.$query.'%')
            ->orWhere('empleado.primer_apellido','LIKE','%'.$query.'%')
            ->orWhere('empleado.documento','LIKE','%'.$query.'%')
            ->orderby('empleado.primer_nombre','asc')
            ->paginate(8);*/
            $empleado= DB::select("
            
                        SELECT *
                        FROM empleado_capacitacion a, empleado b, capacitacion c
                        WHERE a.id_empleado = b.id_empleado
                        AND   a.id_capacitacion = c.id_capacitacion
                        AND   (c.fch_del is null )
                        AND   (
                                   b.primer_nombre LIKE '%".$query."%'
                                OR b.primer_apellido LIKE '%".$query."%'
                                OR b.documento LIKE '%".$query."%'
                              )
                        
                        AND   a.id_capacitacion = ? 
                        ORDER BY b.primer_apellido ASC
            ",[$id]);
            $capacidad=DB::select("
                                    SELECT sum(b.estado_inscripcion),a.estudiantes
                                    FROM capacitacion a, empleado_capacitacion b
                                    WHERE a.id_capacitacion = b.id_capacitacion
                                          and a.id_capacitacion = ?
                                    GROUP BY a.estudiantes;
              ",[$id]);
            foreach ($capacidad as $p)
            {
                $total=$p->estudiantes;
                $inscritos = $p->sum;
            }


            return view('curso_inscripcion.index',['empleado'=>$empleado,'cod_curso'=>$id,"searchText"=>$query, 'plazas'=>($total-$inscritos) ]);
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $capacidad=DB::select("
                                    SELECT sum(b.estado_inscripcion),a.estudiantes
                                    FROM capacitacion a, empleado_capacitacion b
                                    WHERE a.id_capacitacion = b.id_capacitacion
                                          and a.id_capacitacion = (
                                                  SELECT m.id_capacitacion
                                                  FROM empleado_capacitacion m
                                                  WHERE m.id_empleado_capacitacion = ?
                                          )
                                    GROUP BY a.estudiantes;
              ",[$id]);
        foreach ($capacidad as $p)
        {
            $total=$p->estudiantes;
            $inscritos = $p->sum;
        }
        $empleado_capacitacion = EmpleadoCapacitacion::findOrFail($id);
        $empleado_capacitacion->estado_inscripcion=$empleado_capacitacion->estado_inscripcion==0?1:0;
        $empleado_capacitacion->usuario_ini=Auth::user()->id_usuario;
        $empleado_capacitacion->fch_ini=Carbon::now();
        $empleado_capacitacion->host_ini=$request->ip();
        if($total-$inscritos>0){
            $empleado_capacitacion->update();
        }
        if($empleado_capacitacion->estado_inscripcion==0){
            $empleado_capacitacion->update();
        }

        return Redirect::to('inscripcion_empleado/'.$empleado_capacitacion->id_capacitacion);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        echo "hola";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $capacitacion = Capacitacion::findOrFail($id);
        $capacitacion->estado_curso=1;
        $capacitacion->usuario_ini=Auth::user()->id_usuario;
        $capacitacion->fch_ini=Carbon::now();
        $capacitacion->host_ini=$request->ip();
        $capacitacion->update();
        return Redirect::to('curso_aprobado');
    }
    public function inscribir($id_cap,$id_emp,$id_accion){
        return "hola";

    }
}
