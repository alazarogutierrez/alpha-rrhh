<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ReincorporacionController extends Controller
{
    public function index(Request $request)
    {
        if ($request)
        {
            $query=trim($request->get('searchText'));
            $reincorporaciones=DB::table('reincorporacion')
                ->where('documento','LIKE','%'.$query.'%')
                ->orderBy('id_re','asc')
                ->paginate(7);

            return view('historial.re',["reincorporaciones"=>$reincorporaciones,"searchText"=>$query]);
        }
    }
}
