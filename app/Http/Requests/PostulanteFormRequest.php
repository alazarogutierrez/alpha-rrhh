<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostulanteFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'primer_nombre'=> 'required', 'max:50',
            'segundo_nombre'=>'max:50',
            'tercer_nombre'=>'max:50',
            'primer_apellido'=> 'required', 'max:50',
            'segundo_apellido'=>'max:50',
            'documento'=> 'required|numeric', 'max:50',
            'complemento1'=>'max:2',
            'complemento2'=>'max:2',
            'cat_tipo_documento'=> 'required', 'max:10',
            'celular'=> 'numeric',
            'telefono'=> 'numeric',
            'direccion'=>'max:300',
            'cat_estado_civil'=>'max:10',
            'cat_nacionalidad'=>'max:10',
            'host_ini'=>'max:50',
            'host_mod'=>'max:50',
            'host_del'=>'max:50',
        ];
    }
}
