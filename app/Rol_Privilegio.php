<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rol_Privilegio extends Model
{
    //
    protected $table='rol_privilegio';

    public function rol(){
        return $this->belongsTo('App\Rol');
    }
    public function privilegio(){
        return $this->belongsTo('App\Privilegio');
    }
}
