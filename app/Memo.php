<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Memo extends Model
{
    protected $table='memorandum';
    protected $primaryKey='id_mem';
    public $timestamps=false;
}
