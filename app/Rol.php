<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rol extends Model
{

    protected $table='rol';
    public function user_rol(){
        return $this->hasMany('App\Usuario_Rol');
    }
    public function rol_priv(){
        return $this->hasMany('App\Rol_Privilegio');
    }
}
