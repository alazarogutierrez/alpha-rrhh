<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Desvinculacion extends Model
{
    protected $table='desvinculacion';
    protected $primaryKey='id_des';
    public $timestamps=false;
}
