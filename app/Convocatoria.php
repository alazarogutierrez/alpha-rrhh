<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Convocatoria extends Model
{
    protected $table='convocatorias';
    protected $primaryKey='id_convocatoria';
}
