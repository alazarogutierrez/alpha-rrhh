<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empleado_Afp extends Model
{
    //
    protected $table='empleado_afp';
    protected $primaryKey='id_empleado_afp';
    public $timestamps=false;
}
