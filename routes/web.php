<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('home');
})*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index');
Route::get('/denegado', function(){
    return  view('denegado');
})->name('denegado');

//Route::resource('empleado','EmpleadoController');

/*Route::group(['middleware' => ['administrador']], function () {
    Route::resource('empleado','EmpleadoController');
    Route::resource('curso','CursoController');

});*/
Route::resource('empleado','EmpleadoController')->middleware('empleado');
Route::resource('curso','CursoController')->middleware('curso');
Route::get('curso_rechazado','CursoController@rechazado')->name('curso.rechazado')->middleware('curso');
Route::get('curso_aprobado','CursoController@aprobado')->name('curso.aprobado')->middleware('curso');
Route::resource('curso_aprobacion','CursoAprobacionController')->middleware('aprobacion');
//Route::resource('reporte_curso','ReporteCursoController');
Route::resource('reporte_curso','ReporteCursoController');
Route::resource('curso_propuesta','CursoPropuestaController');
Route::resource('perfil_empleado','EmpleadoPerfilController')->middleware('propuesta');
Route::resource('inscripcion_empleado','InscripcionEmpleadoController');
Route::get('inscripcion_empleado/{id_cap}/{id_emp}/{id_accion}','InscripcionEmpleadoController@inscribir')->name('inscripcion_empleado.inscribir');
Route::resource('calificacion_empleado','CalificacionEmpleadoController');
Route::get('curso_solicitud','CursoSolicitudController@index')->name('cursos_solicitud.index')->middleware('propuesta');
Route::delete('curso_solicitud/{id}','CursoSolicitudController@rechazar_solicitud')->name('cursos_solicitud.rechazar_solicitud')->middleware('propuesta');
Route::get('curso_propuestas/{id}','CursoPropuestasController@ver_curso')->name('curso_propuestas.ver_curso')->middleware('propuesta');
Route::get('curso_propuesta/{id_curso}/{id_propuesta}','CursoPropuestaController@elegir')->name('curso_propuesta.elegir')->middleware('propuesta');
Route::delete('curso_propuesta/{id_curso}/{id_propuesta}','CursoPropuestaController@eliminar_propuesta')->name('curso_propuesta.eliminar_propuesta')->middleware('propuesta');
Route::post('curso_propuesta/{id_prop}/{id_cap}','CursoPropuestaController@actualizar_propuesta')->name('curso_propuesta.actualizar_propuesta');
Route::get('curso_aprobacion/{id}','CursoAprobacionController@aceptar')->name('curso_aprobacion.aceptar')->middleware('aprobacion');

//Estos son los middleware
//->middleware('empleado')
//->middleware('historiales')
//->middleware('hoja de vida')
//->middleware('familiares')
//->middleware('memorandum')
//->middleware('contrato')
//->middleware('vinculacion')
//->middleware('desvinculacion')
//->midlleware('reincorporar')
//->midlleware('registro_memo')
//->midlleware('eliminacion_memo')

Route::get('baja/{id_empleado}/edit}', 'EmpinactivoController@edit')->name('baja.edit');//->middleware('empleado')
Route::patch('baj/{id_baja}', 'EmpinactivoController@update')->name('baj.update');//->middleware('empleado')
Route::get('baja/{id_empleado}/crear}', 'EmpinactivoController@crear')->name('baja.crear');//->middleware('empleado')
Route::patch('baja/{id_empleado}', 'EmpinactivoController@guardar')->name('baja.guardar');//->middleware('empleado')
Route::resource('einactivo','EmpinactivoController')->middleware('empleado');
Route::resource('perfil','PerfilController');//->middleware('empleado')
Route::resource('info','InfoController');//->middleware('empleado')

Route::get('hv/{id_empleado}', 'HojaController@show')->name('hv.show');//->middleware('hoja de vida')
Route::get('hoja/{id_empleado}/edit}', 'HojaController@edit')->name('hoja.edit');//->middleware('hoja de vida')
Route::patch('hoj/{id_hoja}', 'HojaController@update')->name('hoj.update');//->middleware('hoja de vida')
Route::get('hoja/{id_empleado}/crear}', 'HojaController@crear')->name('hoja.crear');//->middleware('hoja de vida')
Route::patch('hoja/{id_empleado}', 'HojaController@guardar')->name('hoja.guardar');//->middleware('hoja de vida')
Route::resource('hoja','HojaController')->middleware('hoja de vida');

Route::get('mm/{id_mem}', 'MemoController@show')->name('mm.show');//->middleware('memorandum')
Route::get('memo/{id_mem}/edit}', 'MemoController@edit')->name('memo.edit');//->middleware('memorandum')
Route::patch('mem/{id_mem}', 'MemoController@update')->name('mem.update');//->middleware('memorandum')
Route::get('/listam', 'MemoController@listam')->name('memo.listam')->middleware('memorandum');
Route::get('memo/{id_empleado}/crear}', 'MemoController@crear')->name('memo.crear');//->middleware('memorandum')
Route::patch('memo/{id_empleado}', 'MemoController@guardar')->name('memo.guardar');//->middleware('memorandum')
Route::resource('memo','MemoController')->middleware('memorandum');

Route::get('familiar/{id_familiar}/edit}', 'FamiliarController@edit')->name('familiar.edit');//->middleware('familiares')
Route::patch('fam/{id_familiar}', 'FamiliarController@update')->name('fam.update');//->middleware('familiares')
Route::get('familiar/{id_empleado}/crear}', 'FamiliarController@crear')->name('familiar.crear');//->middleware('familiares')
Route::patch('familiar/{id_empleado}', 'FamiliarController@guardar')->name('familiar.guardar');//->middleware('familiares')
Route::resource('familiar','FamiliarController')->middleware('familiares');

Route::resource('vinculacion','VinculacionController')->middleware('vinculacion');
Route::resource('desvinculacion','DesvinculacionController')->middleware('desvinculacion');
Route::resource('reincorporacion','ReincorporacionController')->middleware('reincorporar');
Route::resource('registromem','RegistromemController')->middleware('registro_memo');
Route::resource('eliminacionmem','EliminacionmemController')->middleware('eliminacion_memo');

Route::get('ct/{id_empleado}', 'ContratoController@show')->name('ct.show');
Route::get('contrato/{id_empleado}/edit}', 'ContratoController@edit')->name('contrato.edit');
Route::patch('cont/{id_contrato}', 'ContratoController@update')->name('cont.update');
Route::get('contrato/{id_empleado}/crear}', 'ContratoController@crear')->name('contrato.crear');
Route::patch('contrato/{id_empleado}', 'ContratoController@guardar')->name('contrato.guardar');
Route::resource('contrato','ContratoController')->middleware('contrato');

Route::get('descargarhv/{id_empleado}', 'HojaController@pdf')->name('hoja.pdf');

Route::get('mail', ['as' => 'enviar', function () {
    $user=\App\User::findOrFail(\Illuminate\Support\Facades\Input::get('id'));
    $data=['usuario'=>$user->nombre,'password'=>\Illuminate\Support\Facades\Input::get('ci')];

    \Mail::send('email.mensaje', $data, function ($message) {
        $message->from('admin@empresabolivia.net', 'Styde.Net');
        $message->to('user@empresabolivia.com')->subject('Notificación');
    });
    return \Illuminate\Support\Facades\Redirect::route('empleado.index');
}]);

Route::get('mail2', ['as' => 'enviar2', function () {
    $empleado=\App\Empleado::findOrFail(\Illuminate\Support\Facades\Input::get('id'));
    $data=['nombre'=>$empleado->primer_nombre,'apellido'=>$empleado->primer_apellido,'memo'=>\Illuminate\Support\Facades\Input::get('memo')];

    \Mail::send('email.mensaje2', $data, function ($message) {
        $message->from('admin@empresabolivia.net', 'Styde.Net');
        $message->to('user@empresabolivia.com')->subject('Notificación');
    });
    return \Illuminate\Support\Facades\Redirect::route('memo.index');
}]);

Route::get('pdf',function (){
    $pdf = PDF::loadView('prueba');
    return $pdf->download('prueba.pdf');
});
//Oliver
Route::resource('postulante','PostulanteController');


