SISTEMA DE RECURSOS HUMANOS

###Caracteristicas

* Proyecto: Sistema de RRHH

* Lenguaje de Programación: PHP 

* Framework: Laravel

* Motor de Base de Datos: Postgre Sql

* Control de Versiones: GitLab

* SE NECESITA:

  * Php 7.1.8
  * Laravel 5.6
  * PostgreSQL 10
  
###Instalacion  del proyecto

Para ejecutar el proyecto se deben seguir los siguientes pasos:

1. git clone https://gitlab.com/alazarogutierrez/alpha-rrhh.git

1. $ composer install

2. $ cp .env.example .env

3. Crear bd en postgresql con nombre db_rrhh

3. Editar el archivo .env con la informacion del DBMS y la BDD

4. $ php artisan key:generate

5. $ php artisan migrate --seed
   
6. $ php artisan serve 



