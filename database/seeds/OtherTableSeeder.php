<?php

use Illuminate\Database\Seeder;

class OtherTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $seg = [['cat_seguro' => 'Medico','caja_de_salud' => 'cns'],
            ['cat_seguro' => 'Social','caja_de_salud' => 'csc']];
        $afp = [['cat_afp' => 'Prevision'],
            ['cat_afp' => 'Futuro de Bolivia'],
            ['cat_afp' => 'Otra']];
        $area = [['nombre' => 'Recursos Humanos'],
            ['nombre' => 'Finanzas'],
            ['nombre' => 'Sistemas'],
            ['nombre' => 'Contabilidad']];
        DB::table('seguro')->insert($seg);
        DB::table('afp')->insert($afp);
        DB::table('area')->insert($area);
    }
}
