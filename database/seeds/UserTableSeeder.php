<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [['nombre' => 'admin1','email' => 'admin@gmail.com','password' => bcrypt('admin1')],//1
                 ['nombre' => 'gerente1','email' => 'gerenterrhh@gmail.com','password' => bcrypt('gerente1')],//2
                 ['nombre' => 'operador1','email' => 'secretaria@gmail.com','password' => bcrypt('operador1')],//3
                 ['nombre' => 'empleado1','email' => 'emp@gmail.com','password' =>bcrypt( 'empleado1')],//4
                 ['nombre' => 'jefearea','email' => 'jefearea@gmail.com','password' =>bcrypt( 'jefea')],//5
                 ['nombre' => 'jefefinan','email' => 'jefefinan@gmail.com','password' =>bcrypt( 'jefefinan')]//6
            ];
        $rol = [['nombre'=> 'administrador'],//1
                ['nombre'=> 'gerente_rrhh'],//2
                ['nombre'=> 'operador_rrhh'],//3
                ['nombre'=> 'empleado'],//4
                ['nombre'=>'jefe area'],//5
                ['nombre'=>'jefe finanzas']];//6
        $u_r = [
                ['id_usuario'=>'1','id_rol'=>'1'],
                ['id_usuario'=>'2','id_rol'=>'2'],
                ['id_usuario'=>'3','id_rol'=>'3'],
                ['id_usuario'=>'4','id_rol'=>'4'],
                ['id_usuario'=>'5','id_rol'=>'5'],
                ['id_usuario'=>'6','id_rol'=>'6']
        ];
        $menu= [['nombre'=>'Empleados','url'=>'#','padre'=>null],//1
                ['nombre'=>'Nomina de Empleado','url'=>'EmpleadoController@index','padre'=>'1'],//2
                ['nombre'=>'Empleados Inactivos','url'=>'EmpinactivoController@index','padre'=>'1'],//3
                ['nombre'=>'Hoja de Vida','url'=>'HojaController@index','padre'=>'1'],//4
                ['nombre'=>'Lista de Familiares','url'=>'FamiliarController@index','padre'=>'1'],//5
                ['nombre'=>'Registrar Memorandum','url'=>'MemoController@index','padre'=>'1'],//6
                ['nombre'=>'Lista de Memorandums','url'=>'MemoController@listam','padre'=>'1'],//7
                ['nombre'=>'Capacitacion','url'=>'#','padre'=>null],//8
                ['nombre'=>'Lista de Cursos','url'=>'CursoController@index','padre'=>'8'],//9
                ['nombre'=>'Aprobacion de cursos','url'=>'CursoAprobacionController@index','padre'=>'8'],//10
                ['nombre'=>'Propuesta de cursos','url'=>'CursoPropuestaController@index','padre'=>'8'],//11
                //11
                ];
        $rol_menu=[
                ['id_rol'=>'1','id_menu'=>'1'],
                ['id_rol'=>'1','id_menu'=>'2'],
                ['id_rol'=>'1','id_menu'=>'3'],
                ['id_rol'=>'1','id_menu'=>'4'],
                ['id_rol'=>'1','id_menu'=>'5'],
                ['id_rol'=>'1','id_menu'=>'6'],
                ['id_rol'=>'1','id_menu'=>'7'],
                ['id_rol'=>'1','id_menu'=>'8'],
                ['id_rol'=>'1','id_menu'=>'9'],
                ['id_rol'=>'1','id_menu'=>'10'],
                ['id_rol'=>'1','id_menu'=>'11'],
                ['id_rol'=>'5','id_menu'=>'8'],
                ['id_rol'=>'5','id_menu'=>'9'],
                ['id_rol'=>'5','id_menu'=>'10'],
                ['id_rol'=>'5','id_menu'=>'11'],
                ['id_rol'=>'6','id_menu'=>'8'],
                ['id_rol'=>'6','id_menu'=>'11'],
        ];
        $privilegio = [
                        ['nombre'=>'empleado'],//1
                        ['nombre'=>'curso'],//2
                        ['nombre'=>'propuesta'],//3
                        ['nombre'=>'aprobacion'],//4
                        ['nombre'=>'historiales'],//5
                        ['nombre'=>'hoja de vida'],//6
                        ['nombre'=>'familiares'],//7
                        ['nombre'=>'memorandum'],//8
                        ['nombre'=>'contrato'],//9
                        ['nombre'=>'vinculacion'],//10
                        ['nombre'=>'desvinculacion'],//11
                        ['nombre'=>'reincorporar'],//12
                        ['nombre'=>'registro_memo'],//13
                        ['nombre'=>'eliminacion_memo'],//14
                      ];
        $rol_privilegio=[
                            ['id_rol'=>'1','id_privilegio'=>'1'],
                            ['id_rol'=>'1','id_privilegio'=>'2'],
                            ['id_rol'=>'1','id_privilegio'=>'3'],
                            ['id_rol'=>'1','id_privilegio'=>'4'],
                            ['id_rol'=>'1','id_privilegio'=>'5'],
                            ['id_rol'=>'1','id_privilegio'=>'6'],
                            ['id_rol'=>'1','id_privilegio'=>'7'],
                            ['id_rol'=>'1','id_privilegio'=>'8'],
                            ['id_rol'=>'1','id_privilegio'=>'9'],
                            ['id_rol'=>'1','id_privilegio'=>'10'],
                            ['id_rol'=>'1','id_privilegio'=>'11'],
                            ['id_rol'=>'1','id_privilegio'=>'12'],
                            ['id_rol'=>'1','id_privilegio'=>'13'],
                            ['id_rol'=>'1','id_privilegio'=>'14'],
                            ['id_rol'=>'2','id_privilegio'=>'3'],
                            ['id_rol'=>'2','id_privilegio'=>'1'],
                            ['id_rol'=>'2','id_privilegio'=>'5'],
                            ['id_rol'=>'2','id_privilegio'=>'10'],
                            ['id_rol'=>'2','id_privilegio'=>'11'],
                            ['id_rol'=>'2','id_privilegio'=>'12'],
                            ['id_rol'=>'2','id_privilegio'=>'13'],
                            ['id_rol'=>'2','id_privilegio'=>'14'],
                            ['id_rol'=>'3','id_privilegio'=>'1'],
                            ['id_rol'=>'3','id_privilegio'=>'6'],
                            ['id_rol'=>'3','id_privilegio'=>'9'],
                            ['id_rol'=>'3','id_privilegio'=>'7'],
                            ['id_rol'=>'3','id_privilegio'=>'8'],//falta asignar empleado
                            ['id_rol'=>'5','id_privilegio'=>'2'],
                            ['id_rol'=>'6','id_privilegio'=>'4'],
                        ];
        $empleado=[
            /*1*/   ['primer_nombre'=>'alejandro','primer_apellido'=>'lazaro','documento'=>'9867486','cat_tipo_documento'=>'CI','usuario_ini'=>'1','fch_ini'=>'2018-05-02','host_ini'=>'127.0.0.1'],
            /*2*/   ['primer_nombre'=>'maria de los angeles','primer_apellido'=>'bustillos','documento'=>'3245627','cat_tipo_documento'=>'CI','usuario_ini'=>'1','fch_ini'=>'2018-05-02','host_ini'=>'127.0.0.1'],
            /*3*/   ['primer_nombre'=>'pilar','primer_apellido'=>'ortiz','documento'=>'8245627','cat_tipo_documento'=>'CI','usuario_ini'=>'1','fch_ini'=>'2018-05-02','host_ini'=>'127.0.0.1'],
            /*4*/   ['primer_nombre'=>'alejandra','primer_apellido'=>'aparicio','documento'=>'9245627','cat_tipo_documento'=>'CI','usuario_ini'=>'1','fch_ini'=>'2018-05-02','host_ini'=>'127.0.0.1'],
            /*5*/   ['primer_nombre'=>'luz','primer_apellido'=>'ceballos','documento'=>'7245627','cat_tipo_documento'=>'CI','usuario_ini'=>'1','fch_ini'=>'2018-05-02','host_ini'=>'127.0.0.1'],
            /*6*/   ['primer_nombre'=>'fatima','primer_apellido'=>'segovia','documento'=>'6245627','cat_tipo_documento'=>'CI','usuario_ini'=>'1','fch_ini'=>'2018-05-02','host_ini'=>'127.0.0.1'],
            /*7*/   ['primer_nombre'=>'mary luz','primer_apellido'=>'pozo','documento'=>'1245627','cat_tipo_documento'=>'CI','usuario_ini'=>'1','fch_ini'=>'2018-05-02','host_ini'=>'127.0.0.1'],
            /*8*/   ['primer_nombre'=>'karen','primer_apellido'=>'schwarz','documento'=>'2245627','cat_tipo_documento'=>'CI','usuario_ini'=>'1','fch_ini'=>'2018-05-02','host_ini'=>'127.0.0.1'],
            /*9*/   ['primer_nombre'=>'tilza','primer_apellido'=>'lozano','documento'=>'3245627','cat_tipo_documento'=>'CI','usuario_ini'=>'1','fch_ini'=>'2018-05-02','host_ini'=>'127.0.0.1'],
            /*10*/   ['primer_nombre'=>'tula','primer_apellido'=>'rodriguez','documento'=>'2255627','cat_tipo_documento'=>'CI','usuario_ini'=>'1','fch_ini'=>'2018-05-02','host_ini'=>'127.0.0.1'],
            /*11*/   ['primer_nombre'=>'oscar','primer_apellido'=>'romero','documento'=>'1245927','cat_tipo_documento'=>'CI','usuario_ini'=>'1','fch_ini'=>'2018-05-02','host_ini'=>'127.0.0.1'],
        ];
        $area=[
            ['nombre'=>'area mercado ','usuario_ini'=>'1','fch_ini'=>'2018-05-02','host_ini'=>'127.0.0.1'],
            ['nombre'=>'area ventas','usuario_ini'=>'1','fch_ini'=>'2018-05-02','host_ini'=>'127.0.0.1'],
        ];
        $empleado_area=[
            ['id_empleado'=>'1','id_area'=>'1','usuario_ini'=>'1','fch_ini'=>'2018-05-02','host_ini'=>'127.0.0.1'],
            ['id_empleado'=>'2','id_area'=>'1','usuario_ini'=>'1','fch_ini'=>'2018-05-02','host_ini'=>'127.0.0.1'],
            ['id_empleado'=>'3','id_area'=>'1','usuario_ini'=>'1','fch_ini'=>'2018-05-02','host_ini'=>'127.0.0.1'],
            ['id_empleado'=>'4','id_area'=>'1','usuario_ini'=>'1','fch_ini'=>'2018-05-02','host_ini'=>'127.0.0.1'],
            ['id_empleado'=>'5','id_area'=>'1','usuario_ini'=>'1','fch_ini'=>'2018-05-02','host_ini'=>'127.0.0.1'],
            ['id_empleado'=>'6','id_area'=>'1','usuario_ini'=>'1','fch_ini'=>'2018-05-02','host_ini'=>'127.0.0.1'],
            ['id_empleado'=>'7','id_area'=>'2','usuario_ini'=>'1','fch_ini'=>'2018-05-02','host_ini'=>'127.0.0.1'],
            ['id_empleado'=>'8','id_area'=>'2','usuario_ini'=>'1','fch_ini'=>'2018-05-02','host_ini'=>'127.0.0.1'],
            ['id_empleado'=>'9','id_area'=>'2','usuario_ini'=>'1','fch_ini'=>'2018-05-02','host_ini'=>'127.0.0.1'],
            ['id_empleado'=>'10','id_area'=>'2','usuario_ini'=>'1','fch_ini'=>'2018-05-02','host_ini'=>'127.0.0.1'],
            ['id_empleado'=>'11','id_area'=>'2','usuario_ini'=>'1','fch_ini'=>'2018-05-02','host_ini'=>'127.0.0.1'],
        ];
        DB::table('usuario')->insert($user);
        DB::table('rol')->insert($rol);
        DB::table('usuario_rol')->insert($u_r);
        DB::table('privilegio')->insert($privilegio);
        DB::table('rol_privilegio')->insert($rol_privilegio);
        DB::table('menu')->insert($menu);
        DB::table('rol_menu')->insert($rol_menu);
        DB::table('empleado')->insert($empleado);
        DB::table('area')->insert($area);
        DB::table('empleado_area')->insert($empleado_area);

    }
}
