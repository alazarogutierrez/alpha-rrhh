<?php

use Illuminate\Database\Seeder;

class ConvocatoriaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Convocatoria::class, 2)->create();
    }
}
