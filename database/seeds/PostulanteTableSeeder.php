<?php

use Illuminate\Database\Seeder;

class PostulanteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Postulante::class, 5)->create();
    }
}
