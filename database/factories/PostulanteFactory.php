<?php

use Faker\Generator as Faker;

$factory->define(App\Postulante::class, function (Faker $faker) {
    return [
        'primer_nombre' => $faker->firstName,
        'segundo_nombre' => $faker->firstName,
        'primer_apellido' => $faker->lastName,
        'segundo_apellido' => $faker->lastName,
        'documento' => rand(2000000,9999999),
        'complemento1' => 'LP',
        'cat_tipo_documento' => 'CI',
        'fecha_nacimiento' => $faker->dateTimeThisCentury,
        'edad' => rand(18, 60),
        'cat_genero' => 'M',
        'celular' => rand(70100000,79999999),
        'telefono' => rand(2000000,2999999),
        'direccion' => $faker->address,
        'cat_estado_civil' => 'Soltero',
        'cat_nacionalidad' => 'Bolivia',
        'usuario_ini' => rand(1, 3),
        'fch_ini' => $faker->dateTimeThisYear('+1 month'),
        'host_ini' => $faker->ipv4,
    ];
});
