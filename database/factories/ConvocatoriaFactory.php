<?php

use Faker\Generator as Faker;

$factory->define(App\Convocatoria::class, function (Faker $faker) {
    return [
        'descripcion' => $faker->text(rand(256, 512)),
        'fecha_inicio' => $faker->dateTimeThisCentury,
        'fecha_fin' => $faker->dateTimeThisYear('+1 month'),
        'estado' => rand(1, 2),
    ];
});
