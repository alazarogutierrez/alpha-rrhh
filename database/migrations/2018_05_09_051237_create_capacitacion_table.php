<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCapacitacionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('capacitacion', function (Blueprint $table) {
            $table->increments('id_capacitacion');
            $table->string('tema',100);
            $table->integer('estudiantes');
            $table->Decimal('presupuesto_solicitado');
            $table->text('descripcion');
            $table->text('descripcion_aprobacion')->nullable();
            $table->text('segunda_descripcion_aprobacion')->nullable();
            $table->integer('estado_curso')->default(0);
            $table->integer('aprobacion');
            $table->integer('segunda_aprobacion');
            $table->integer('usuario_ini');
            $table->integer('usuario_mod')->nullable();
            $table->integer('usuario_del')->nullable();
            $table->dateTime('fch_ini');
            $table->dateTime('fch_mod')->nullable();
            $table->dateTime('fch_del')->nullable();
            $table->string('host_ini');
            $table->string('host_mod')->nullable();
            $table->string('host_del')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('capacitacion');
    }
}
