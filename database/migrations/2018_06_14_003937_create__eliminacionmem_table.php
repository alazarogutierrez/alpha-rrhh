<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEliminacionmemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eliminacionmem', function (Blueprint $table) {
            $table->increments('id_em');
            $table->integer('id_mem')->nullable();
            $table->integer('id_empleado')->nullable();
            $table->string('documento',50)->nullable();
            $table->integer('usuario_ini')->nullable();
            $table->dateTime('fch_ini')->nullable();
            $table->string('host_ini')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eliminacionmem');
    }
}
