<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHojadevidaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hojadevida', function (Blueprint $table) {
            $table->increments('id_hv');
            $table->string('nivel',50)->nullable();
            $table->string('primaria',100)->nullable();
            $table->integer('añoprimaria')->nullable();
            $table->string('secundaria',100)->nullable();
            $table->integer('añosecundaria')->nullable();
            $table->string('pregrado1',100)->nullable();
            $table->string('tpregrado1',70)->nullable();
            $table->integer('apregrado1')->nullable();
            $table->string('pregrado2',100)->nullable();
            $table->string('tpregrado2',70)->nullable();
            $table->integer('apregrado2')->nullable();
            $table->string('postgrado1',100)->nullable();
            $table->string('tpostgrado1',70)->nullable();
            $table->integer('apostgrado1')->nullable();
            $table->string('postgrado2',100)->nullable();
            $table->string('tpostgrado2',70)->nullable();
            $table->integer('apostgrado2')->nullable();
            $table->string('postgrado3',100)->nullable();
            $table->string('tpostgrado3',70)->nullable();
            $table->integer('apostgrado3')->nullable();
            $table->string('elaboral1',100)->nullable();
            $table->string('celaboral1',70)->nullable();
            $table->integer('aelaboral1')->nullable();
            $table->string('relaboral1',20)->nullable();
            $table->string('elaboral2',100)->nullable();
            $table->string('celaboral2',70)->nullable();
            $table->integer('aelaboral2')->nullable();
            $table->string('relaboral2',20)->nullable();
            $table->string('elaboral3',100)->nullable();
            $table->string('celaboral3',70)->nullable();
            $table->integer('aelaboral3')->nullable();
            $table->string('relaboral3',20)->nullable();
            $table->string('curso1',100)->nullable();
            $table->string('tcurso1',70)->nullable();
            $table->integer('acurso1')->nullable();
            $table->string('curso2',100)->nullable();
            $table->string('tcurso2',70)->nullable();
            $table->integer('acurso2')->nullable();
            $table->string('curso3',100)->nullable();
            $table->string('tcurso3',70)->nullable();
            $table->integer('acurso3')->nullable();
            $table->string('idioma',70)->nullable();
            $table->string('idioma1',70)->nullable();
            $table->string('idioma2',70)->nullable();
            $table->string('idioma3',70)->nullable();
            $table->unsignedInteger('id_empleado')->unique()->nullable();
            $table->integer('usuario_ini');
            $table->integer('usuario_mod')->nullable();
            $table->integer('usuario_del')->nullable();
            $table->dateTime('fch_ini');
            $table->dateTime('fch_mod')->nullable();
            $table->dateTime('fch_del')->nullable();
            $table->string('host_ini',50);
            $table->string('host_mod',50)->nullable();
            $table->string('host_del',50)->nullable();
            $table->foreign('id_empleado')->references('id_empleado')->on('empleado');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hojadevida');
    }
}
