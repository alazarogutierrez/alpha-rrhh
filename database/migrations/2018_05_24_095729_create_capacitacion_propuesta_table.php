<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCapacitacionPropuestaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('capacitacion_propuesta', function (Blueprint $table) {
            $table->increments('id_capacitacion_propuesta');
            $table->string('instituto');
            $table->decimal('costo');
            $table->date('fecha_inicio');
            $table->date('fecha_fin');
            $table->string('direccion');
            $table->string('descripcion');
            $table->string('descripcion_cv');
            $table->integer('eleccion');
            $table->integer('sesiones');
            $table->unsignedInteger('id_capacitacion');
            $table->integer('usuario_ini');
            $table->integer('usuario_mod')->nullable();
            $table->integer('usuario_del')->nullable();
            $table->dateTime('fch_ini');
            $table->dateTime('fch_mod')->nullable();
            $table->dateTime('fch_del')->nullable();
            $table->string('host_ini');
            $table->string('host_mod')->nullable();
            $table->string('host_del')->nullable();
            $table->foreign('id_capacitacion')->references('id_capacitacion')->on('capacitacion');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('capacitacion_propuesta');
    }
}
