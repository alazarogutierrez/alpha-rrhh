<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolMenuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rol_menu', function (Blueprint $table) {
            $table->increments('id_rol_menu');
            $table->unsignedInteger('id_rol');
            $table->unsignedInteger('id_menu');
            $table->integer('usuario_ini')->nullable();
            $table->integer('usuario_mod')->nullable();
            $table->integer('usuario_del')->nullable();
            $table->dateTime('fch_ini')->nullable();
            $table->dateTime('fch_mod')->nullable();
            $table->dateTime('fch_del')->nullable();
            $table->string('host_ini',50)->nullable();
            $table->string('host_mod',50)->nullable();
            $table->string('host_del',50)->nullable();
            $table->foreign('id_rol')->references('id_rol')->on('rol');
            $table->foreign('id_menu')->references('id_menu')->on('menu');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rol_menu');
    }
}
