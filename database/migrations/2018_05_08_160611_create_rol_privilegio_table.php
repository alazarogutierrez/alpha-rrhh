<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolPrivilegioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rol_privilegio', function (Blueprint $table) {
            $table->increments('id_rol_privilegio');
            $table->unsignedInteger('id_rol')->nullable();
            $table->unsignedInteger('id_privilegio')->nullable();
            $table->integer('usuario_ini')->nullable();
            $table->integer('usuario_mod')->nullable();
            $table->integer('usuario_del')->nullable();
            $table->dateTime('fch_ini')->nullable();
            $table->dateTime('fch_mod')->nullable();
            $table->dateTime('fch_del')->nullable();
            $table->string('host_ini')->nullable();
            $table->string('host_mod')->nullable();
            $table->string('host_del')->nullable();
            $table->foreign('id_rol')->references('id_rol')->on('rol');
            $table->foreign('id_privilegio')->references('id_privilegio')->on('privilegio');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rol_privilegio');
    }
}
