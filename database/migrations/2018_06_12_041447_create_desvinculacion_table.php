<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDesvinculacionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('desvinculacion', function (Blueprint $table) {
            $table->increments('id_des');
            $table->integer('id_empleado')->nullable();
            $table->string('documento',50)->nullable();
            $table->integer('usuario_ini')->nullable();
            $table->dateTime('fch_ini')->nullable();
            $table->string('host_ini')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('desvinculacion');
    }
}
