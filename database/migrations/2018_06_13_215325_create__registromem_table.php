<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegistromemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registromem', function (Blueprint $table) {
            $table->increments('id_rm');
            $table->integer('id_mem')->nullable();
            $table->integer('id_empleado')->nullable();
            $table->string('documento',50)->nullable();
            $table->integer('usuario_ini')->nullable();
            $table->dateTime('fch_ini')->nullable();
            $table->string('host_ini')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registromem');
    }
}
