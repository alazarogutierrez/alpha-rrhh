@extends ('layouts.admin')
@section('titulo_content')
    <h2>Lista de Familiares</h2>
@endsection
@section ('contenido')
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            @include('familiar.search')
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 table-responsive">
            <table class="table table-striped table-bordered table-condensed table-hover">
                <thead>
                <th>Id</th>
                <th colspan="2" style="text-align:center">Nombres </th>
                <th colspan="2" style="text-align:center">Apellidos</th>
                <th>Id Empleado</th>
                <th>Parentesco</th>
                <th colspan="3" style="text-align:center">Documento</th>
                <th>Edad</th>
                <th>Genero</th>
                <th>Celular</th>
                <th>Telefono</th>

                </thead>
                @foreach ($familiares as $per)

                    <tr>
                        <td>{{$per->id_familiar}}</td>
                        <td>{{$per->primer_nombre}}</td>
                        <td>{{$per->segundo_nombre}}</td>
                        <td>{{$per->primer_apellido}}</td>
                        <td>{{$per->segundo_apellido}}</td>
                        <td>{{$per->id_empleado}}</td>
                        <td>{{$per->tipo}}</td>
                        <td>{{$per->cat_tipo_documento}}</td>
                        <td>{{$per->documento}}</td>
                        <td>{{$per->complemento1}}</td>
                        <td>{{$per->edad}}</td>
                        <td>{{$per->cat_genero}}</td>
                        <td>{{$per->celular}}</td>
                        <td>{{$per->telefono}}</td>
                        <td><a href="{{URL::action('FamiliarController@edit',$per->id_familiar)}}"><button class="btn btn-success" title="Editar Datos de Familiar"><i class="fa fa-edit"></i></button></a></td>
                        <td><a href="" data-target="#modal-delete-{{$per->id_familiar}}" data-toggle="modal"><button class="btn btn-danger" title="Eliminar Familiar"><i class="fa fa-trash"></i></button></a></td>
                    </tr>
                    @include('familiar.modal')
                @endforeach
            </table>
            {{$familiares->render()}}
        </div>
    </div>
@stop