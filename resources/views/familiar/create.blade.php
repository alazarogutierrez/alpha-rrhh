@extends ('layouts.admin')
@section('titulo_content')
    <div class="row">
        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
            <h2>Registrar Familiar</h2>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
            <h2><a href="{{URL::action('EmpleadoController@index')}}"><button class="btn btn-success" type="button">Volver</button></a></h2>
        </div>
    </div>
@endsection
@section ('contenido')
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            @if (count($errors)>0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            {!!Form::model($empleado,['method'=>'PATCH','route'=>['familiar.guardar',$empleado->id_empleado]])!!}
            {{Form::token()}}
            <form class="was-validated">
                <div class="form-group">
                    <label for="nombre">Nombre*</label>
                    <input type="text" name="primer_nombre" class="form-control" placeholder="Nombre" required>
                </div>
                <div class="form-group">
                    <label for="nombre">Segundo Nombre</label>
                    <input type="text" name="segundo_nombre" class="form-control" placeholder="Segundo Nombre">
                </div>
                <div class="form-group">
                    <label for="nombre">Apellido*</label>
                    <input type="text" name="primer_apellido" class="form-control" placeholder="Apellido" required>
                </div>
                <div class="form-group">
                    <label for="nombre">Segundo Apellido</label>
                    <input type="text" name="segundo_apellido" class="form-control" placeholder="Segundo Apellido">
                </div>
                <div class="form-group">
                    <label>Grado de Parentesco</label>
                    <select name="tipo" class="form-control">
                        <option value="Hij@" selected>Hijo-Hija</option>
                        <option value="Espos@">Esposo-Esposa</option>
                        <option value="Padre">Padre</option>
                        <option value="Madre">Madre</option>
                        <option value="Otro">Otro</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="nombre">Documento*</label>
                    <input type="text" name="documento" class="form-control" placeholder="Numero de Documento"><br>
                    <input type="text" name="complemento1" class="form-control" placeholder="Complemento 1"><br>
                </div>
                <div class="form-group">
                    <label>Tipo Documento*</label>
                    <select name="cat_tipo_documento" class="form-control">
                        <option value="CI" selected>CI</option>
                        <option value="RUN">RUN</option>
                        <option value="PPTE">PPTE</option>
                        <option value="CE">CE</option>
                        <option value="Otro">Otro</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="nombre">Fecha Nacimiento*</label>
                    <input type="date" name="fecha_nacimiento" class="form-control" placeholder="Fecha de Nacimiento">
                </div>
                <div class="form-group">
                    <label>Genero*</label>
                    <select name="cat_genero" class="form-control">
                        <option value="Masculino" selected>Masculino</option>
                        <option value="Femenino">Femenino</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="nombre">Celular</label>
                    <input type="text" name="celular" class="form-control" placeholder="Celular">
                </div>
                <div class="form-group">
                    <label for="nombre">Telefono</label>
                    <input type="text" name="telefono" class="form-control" placeholder="Telefono">
                </div>
                <div class="form-group">
                    <button class="btn btn-primary" type="submit">Guardar</button>
                    <button class="btn btn-danger" type="reset">Cancelar</button>
                </div>
            </form>
            {!!Form::close()!!}

        </div>
    </div>
@stop