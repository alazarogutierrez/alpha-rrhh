@extends ('layouts.admin')
@section('titulo_content')
    <div class="row">
        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
            <h2>Modificar Registro Familiar</h2>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
            <h2><a href="{{URL::action('FamiliarController@index')}}"><button class="btn btn-success" type="button">Volver</button></a></h2>
        </div>
    </div>
@endsection
@section ('contenido')
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            @if (count($errors)>0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            {!!Form::model($familiar,['method'=>'PATCH','route'=>['fam.update',$familiar->id_familiar]])!!}
            {{Form::token()}}
                <div class="form-group">
                    <label for="nombre">Nombre*</label>
                    <input type="text" name="primer_nombre" class="form-control" value="{{$familiar->primer_nombre}}" placeholder="Nombre" required>
                </div>
                <div class="form-group">
                    <label for="nombre">Segundo Nombre</label>
                    <input type="text" name="segundo_nombre" class="form-control" value="{{$familiar->segundo_nombre}}" placeholder="Segundo Nombre">
                </div>
                <div class="form-group">
                    <label for="nombre">Apellido*</label>
                    <input type="text" name="primer_apellido" class="form-control" value="{{$familiar->primer_apellido}}" placeholder="Apellido" required>
                </div>
                <div class="form-group">
                    <label for="nombre">Segundo Apellido</label>
                    <input type="text" name="segundo_apellido" class="form-control" value="{{$familiar->segundo_apellido}}" placeholder="Segundo Apellido">
                </div>
                <div class="form-group">
                    <label>Grado de Parentesco</label>
                    <select name="tipo" class="form-control">
                        @if($familiar->tipo=='Hij@')
                        <option value="Hij@" selected>Hijo-Hija</option>
                        <option value="Espos@">Esposo-Esposa</option>
                        <option value="Padre">Padre</option>
                        <option value="Madre">Madre</option>
                        <option value="Otro">Otro</option>
                        @elseif($familiar->tipo=='Espos@')
                            <option value="Hij@">Hijo-Hija</option>
                            <option value="Espos@" selected>Esposo-Esposa</option>
                            <option value="Padre">Padre</option>
                            <option value="Madre">Madre</option>
                            <option value="Otro">Otro</option>
                        @elseif($familiar->tipo=='Padre')
                            <option value="Hij@">Hijo-Hija</option>
                            <option value="Espos@">Esposo-Esposa</option>
                            <option value="Padre" selected>Padre</option>
                            <option value="Madre">Madre</option>
                            <option value="Otro">Otro</option>
                        @elseif($familiar->tipo=='Madre')
                            <option value="Hij@">Hijo-Hija</option>
                            <option value="Espos@">Esposo-Esposa</option>
                            <option value="Padre">Padre</option>
                            <option value="Madre" selected>Madre</option>
                            <option value="Otro">Otro</option>
                        @elseif($familiar->tipo=='Otro')
                            <option value="Hij@">Hijo-Hija</option>
                            <option value="Espos@">Esposo-Esposa</option>
                            <option value="Padre">Padre</option>
                            <option value="Madre">Madre</option>
                            <option value="Otro" selected>Otro</option>
                        @endif
                    </select>
                </div>
                <div class="form-group">
                    <label for="nombre">Documento*</label>
                    <input type="text" name="documento" class="form-control" placeholder="Numero de Documento"><br>
                    <input type="text" name="complemento1" class="form-control" placeholder="Complemento 1"><br>
                </div>
                <div class="form-group">
                    <label>Tipo Documento*</label>
                    <select name="cat_tipo_documento" class="form-control">
                        @if($familiar->cat_tipo_documento=='CI')
                            <option value="CI" selected>CI</option>
                            <option value="RUN">RUN</option>
                            <option value="PPTE">PPTE</option>
                            <option value="CE">CE</option>
                            <option value="Otro">Otro</option>
                        @elseif($familiar->cat_tipo_documento=='RUN')
                            <option value="CI">CI</option>
                            <option value="RUN" selected>RUN</option>
                            <option value="PPTE">PPTE</option>
                            <option value="CE">CE</option>
                            <option value="Otro">Otro</option>
                        @elseif($familiar->cat_tipo_documento=='PPTE')
                            <option value="CI">CI</option>
                            <option value="RUN">RUN</option>
                            <option value="PPTE" selected>PPTE</option>
                            <option value="CE">CE</option>
                            <option value="Otro">Otro</option>
                        @elseif($familiar->cat_tipo_documento=='CE')
                            <option value="CI">CI</option>
                            <option value="RUN">RUN</option>
                            <option value="PPTE">PPTE</option>
                            <option value="CE" selected>CE</option>
                            <option value="Otro">Otro</option>
                        @elseif($familiar->cat_tipo_documento=='Otro')
                            <option value="CI">CI</option>
                            <option value="RUN">RUN</option>
                            <option value="PPTE">PPTE</option>
                            <option value="CE">CE</option>
                            <option value="Otro" selected>Otro</option>
                        @endif
                    </select>
                </div>
                <div class="form-group">
                    <label for="nombre">Fecha Nacimiento*</label>
                    <input type="date" name="fecha_nacimiento" class="form-control" value="{{$familiar->fecha_nacimiento}}" placeholder="Fecha de Nacimiento">
                </div>
                <div class="form-group">
                    <label>Genero*</label>
                    <select name="cat_genero" class="form-control">
                        @if($familiar->cat_genero=='Masculino')
                            <option value="Masculino" selected>M</option>
                            <option value="Femenino">F</option>
                        @elseif($familiar->cat_genero=='Femenino')
                            <option value="Masculino">M</option>
                            <option value="Femenino" selected>F</option>
                        @endif
                    </select>
                </div>
                <div class="form-group">
                    <label for="nombre">Celular</label>
                    <input type="text" name="celular" class="form-control" value="{{$familiar->celular}}" placeholder="Celular">
                </div>
                <div class="form-group">
                    <label for="nombre">Telefono</label>
                    <input type="text" name="telefono" class="form-control" value="{{$familiar->telefono}}" placeholder="Telefono">
                </div>


            <div class="form-group">
                <button class="btn btn-primary" type="submit">Guardar</button>
                <button class="btn btn-danger" type="reset">Cancelar</button>
            </div>

            {!!Form::close()!!}

        </div>
    </div>
@stop