@extends ('layouts.admin')
@section('titulo_content')
    <div class="row">
        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
            <h2>Modificar Datos de Contrato</h2>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
            <h2><a href="{{URL::action('ContratoController@index')}}"><button class="btn btn-success" type="button">Volver</button></a></h2>
        </div>
    </div>
@endsection
@section ('contenido')
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            @if (count($errors)>0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            {!!Form::model($contrato,['method'=>'PATCH','route'=>['cont.update',$contrato->id_contrato]])!!}
            {{Form::token()}}
                <form class="was-validated">
                    <div class="form-group">
                        <label for="cat_contrato">Tipo Contrato</label>
                        <select name="cat_contrato" class="form-control">
                            @if($contrato->cat_contrato=='Tiempo Completo')
                                <option value="Tiempo Completo" selected>Tiempo Completo</option>
                                <option value="Medio Tiempo">Medio Tiempo</option>
                                <option value="Eventual">Eventual</option>
                                <option value="Otro">Otro</option>
                            @elseif($contrato->cat_contrato=='Medio Tiempo')
                                <option value="Tiempo Completo">Tiempo Completo</option>
                                <option value="Medio Tiempo" selected>Medio Tiempo</option>
                                <option value="Eventual">Eventual</option>
                                <option value="Otro">Otro</option>
                            @elseif($contrato->cat_contrato=='Eventual')
                                <option value="Tiempo Completo">Tiempo Completo</option>
                                <option value="Medio Tiempo">Medio Tiempo</option>
                                <option value="Eventual" selected>Eventual</option>
                                <option value="Otro">Otro</option>
                            @elseif($contrato->cat_contrato=='Otro')
                                <option value="Tiempo Completo">Tiempo Completo</option>
                                <option value="Medio Tiempo">Medio Tiempo</option>
                                <option value="Eventual">Eventual</option>
                                <option value="Otro" selected>Otro</option>
                            @endif
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="nombre">Fecha Inicio</label>
                        <input type="date" name="fechai" class="form-control" value="{{$contrato->fechai}}" placeholder="Fecha">
                    </div>
                    <div class="form-group">
                        <label for="nombre">Fecha Final (si aplica)</label>
                        <input type="date" name="fechaf" class="form-control" value="{{$contrato->fechaf}}" placeholder="Fecha">
                    </div>
                    <div class="form-group">
                        <label for="nombre">Cargo</label>
                        <input type="text" name="cargo" class="form-control" value="{{$contrato->cargo}}" placeholder="cargo">
                    </div>
                    <div class="form-group">
                        <label for="nombre">Salario</label>
                        <input type="number" name="salario" class="form-control" value="{{$contrato->salario}}" placeholder="salario">
                    </div>
                    <div class="form-group">
                        <label for="nombre">Multa de Incumplimiento</label>
                        <input type="number" name="multa" class="form-control" value="{{$contrato->multa}}" placeholder="multa">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary" type="submit">Guardar</button>
                        <button class="btn btn-danger" type="reset">Cancelar</button>
                    </div>
                </form>

            {!!Form::close()!!}

        </div>
    </div>
@stop