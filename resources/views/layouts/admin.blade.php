<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>RRHH</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('css/font-awesome.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('css/AdminLTE.min.css')}}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{asset('css/_all-skins.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/horarios.css')}}">
    <link rel="apple-touch-icon" href="{{asset('img/apple-touch-icon.png')}}">
    <link rel="shortcut icon" href="{{asset('img/favicon.ico')}}">
    <script src="{{asset('js/sweetalert.min.js')}}"></script>
    <script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
    @yield('script_propuesta')
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <header class="main-header">

        <!-- Logo -->
        <a href="index2.html" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->

            <span class="logo-mini"><b>RRHH</b></span>
            <!-- logo for regular state and mobile devices -->

            <span class="logo-lg"><b>Recursos Humanos</b></span>

        </a>

        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Navegación</span>
            </a>
            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- Messages: style can be found in dropdown.less-->

                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <small class="bg-red">Online</small>
                            <span class="hidden-xs">{{Auth::user()->nombre}}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <p>
                                    <img src="{{asset('./img/user-icon.png')}}" width="150" height="150">
                                </p>
                            </li>

                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-center">
                                    <a class="btn btn-default btn-block " href="{{URL::action('InfoController@edit',Auth::user()->id_usuario)}}">
                                        Informacion
                                    </a>
                                    <a class="btn btn-default btn-block " href="{{URL::action('PerfilController@edit',Auth::user()->id_usuario)}}">
                                        Cambiar Contraseña
                                    </a>
                                    <a class="btn btn-danger btn-block " href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        </ul>
                    </li>

                </ul>
            </div>

        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->

            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu">
                <li class="header"></li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-laptop"></i>
                        <span>Empleados</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{URL::action('EmpleadoController@index')}}"><i class="fa fa-circle-o"></i>Nomina de Empleados</a></li>
                        <li><a href="{{URL::action('EmpinactivoController@index')}}"><i class="fa fa-circle-o"></i>Empleados Inactivos</a></li>
                        <li><a href="{{URL::action('VinculacionController@index')}}"><i class="fa fa-circle-o"></i>Historial Vinculaciones</a></li>
                        <li><a href="{{URL::action('DesvinculacionController@index')}}"><i class="fa fa-circle-o"></i>Historial Desvinculaciones</a></li>
                        <li><a href="{{URL::action('ReincorporacionController@index')}}"><i class="fa fa-circle-o"></i>Historial Reincorporaciones</a></li>
                        <li><a href="{{URL::action('HojaController@index')}}"><i class="fa fa-circle-o"></i>Hoja de Vida</a></li>
                        <li><a href="{{URL::action('FamiliarController@index')}}"><i class="fa fa-circle-o"></i>Lista de Familiares</a></li>
                        <li><a href="{{URL::action('MemoController@index')}}"><i class="fa fa-circle-o"></i>Registrar Memorandum</a></li>
                        <li><a href="{{URL::action('MemoController@listam')}}"><i class="fa fa-circle-o"></i>Lista Memorandums</a></li>
                        <li><a href="{{URL::action('RegistromemController@index')}}"><i class="fa fa-circle-o"></i>Historial Registro Memos</a></li>
                        <li><a href="{{URL::action('EliminacionmemController@index')}}"><i class="fa fa-circle-o"></i>Historial Eliminacion Memos</a></li>
                        <li><a href="{{URL::action('ContratoController@index')}}"><i class="fa fa-circle-o"></i>Datos de Contrato</a></li>
                    </ul>
                </li>

                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-th"></i>
                        <span>Capacitaciones</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{URL::action('CursoController@index')}}"><i class="fa fa-circle-o"></i>Cursos Solicitados</a></li>
                        <li><a href="{{URL::action('CursoController@aprobado')}}"><i class="fa fa-circle-o"></i>Cursos Aprobados</a></li>
                        <li><a href="{{URL::action('CursoController@rechazado')}}"><i class="fa fa-circle-o"></i>Cursos Rechazados</a></li>
                        <li><a href="{{URL::action('CursoSolicitudController@index')}}"><i class="fa fa-circle-o"></i>Propuestas de Cursos</a></li>
                        <li><a href="{{URL::action('CursoAprobacionController@index')}}"><i class="fa fa-circle-o"></i>Aprobacion de Cursos</a></li>
                    </ul>
                </li>

                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-pencil"></i>
                        <span>Postulantes</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{URL::action('PostulanteController@index')}}"><i class="fa fa-circle-o"></i>Registrar Postulante</a></li>
                    </ul>
                </li>
            </ul>
        </section>
    </aside>





    <!--Contenido-->
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <!-- Main content -->
        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title"> @yield('titulo_content')</h3>
                            <div class="box-tools pull-right">
                                <!--<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>-->
                                <!--<button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>-->
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <!--Contenido-->
                                @yield('contenido')
                                <!--Fin Contenido-->
                                </div>
                            </div>

                        </div>
                    </div><!-- /.row -->
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </section>
    </div><!-- /.col -->
</div><!-- /.row -->

</section><!-- /.content -->
</div><!-- /.content-wrapper -->
<!--Fin-Contenido-->
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 2.3.0
    </div>
</footer>


<!-- jQuery 2.1.4 -->
<script src="{{asset('js/jQuery-2.1.4.min.js')}}"></script>
<!-- Bootstrap 3.3.5 -->
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('js/app.min.js')}}"></script>

</body>
</html>
