@extends ('layouts.admin')
@section('titulo_content')
    <h2>Lista de Empleados Inactivos</h2>
@endsection
@section ('contenido')
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            @include('empleado.search2')
        </div>
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
            <a href="/empleado"><button class="btn btn-success btn-sm">Empleados Activos</button></a>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 table-responsive">
            <table class="table table-striped table-bordered table-condensed table-hover">
                <thead>
                <th>Id</th>
                <th colspan="2" style="text-align:center">Nombres </th>
                <th colspan="2" style="text-align:center">Apellidos</th>
                <th colspan="3" style="text-align:center">Documento</th>
                <th>Nacionalidad</th>
                <th>Edad</th>
                <th>Genero</th>
                <th>Celular</th>
                <th>Telefono</th>
                <th>Direccion</th>
                <th>Motivo de Baja</th>

                </thead>
                @foreach ($empleados as $per)

                    <tr>
                        <td>{{$per->id_empleado}}</td>
                        <td>{{$per->primer_nombre}}</td>
                        <td>{{$per->segundo_nombre}}</td>
                        <td>{{$per->primer_apellido}}</td>
                        <td>{{$per->segundo_apellido}}</td>
                        <td>{{$per->cat_tipo_documento}}</td>
                        <td>{{$per->documento}}</td>
                        <td>{{$per->complemento1}}</td>
                        <td>{{$per->cat_nacionalidad}}</td>
                        <td>{{$per->edad}}</td>
                        <td>{{$per->cat_genero}}</td>
                        <td>{{$per->celular}}</td>
                        <td>{{$per->telefono}}</td>
                        <td>{{$per->direccion}}</td>
                        @switch($per->mbaja)
                            @case(0)
                            <td class="default">
                            {{'Sin especificar'}}
                            @break
                            @case(1)
                            <td class="success">
                            {{'Despido'}}
                            @break
                            @case(2)
                            <td class="primary">
                            {{'Renuncia'}}
                            @break
                            @case(3)
                            <td class="info">
                            {{'Traslado'}}
                            @break
                            @case(4)
                            <td class="warning">
                            {{'Jubilacion'}}
                            @break
                            @case(5)
                            <td class="danger">
                                {{'Fallecimiento'}}
                                @break
                            @case(6)
                            <td class="secondary">
                                {{'Sin especificar'}}
                                @break
                                @endswitch
                            </td>
                            @if($per->mbaja==0)
                        <td><a href="{{URL::action('EmpinactivoController@crear',$per->id_empleado)}}"><button class="btn btn-warning" title="Registrar Motivo de Baja"><i class="fa fa-plus"></i></button></a></td>
                            @else
                                <td><a href="{{URL::action('EmpinactivoController@edit',$per->id_empleado)}}"><button class="btn btn-primary" title="Editar Motivo de Baja"><i class="fa fa-edit"></i></button></a></td>
                           @endif
                            @if($per->mbaja!=4 && $per->mbaja!=5)
                            <td><a href="" data-target="#modal-delete-{{$per->id_empleado}}" data-toggle="modal"><button class="btn btn-success" title="Reincorporar al Empleado"><i class="fa fa-arrow-circle-up"></i></button></a></td>
                            @endif
                    </tr>
                    @include('empleado.modal2')
                @endforeach
            </table>
            {{$empleados->render()}}
        </div>
    </div>
@stop