@extends ('layouts.admin')
@section('titulo_content')
    <h2>Lista de Empleados Activos</h2>
@endsection
@section ('contenido')
    <div class="row">
        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
            @include('empleado.search')
        </div>
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
            <a href="empleado/create"><button class="btn btn-success btn-sm">Nuevo Empleado</button></a>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
            <a href="einactivo"><button class="btn btn-danger btn-sm">Empleados Inactivos</button></a>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 table-responsive">
                <table class="table table-striped table-bordered table-condensed table-hover">
                    <thead>
                    <th>Id</th>
                    <th colspan="2" style="text-align:center">Nombres </th>
                    <th colspan="2" style="text-align:center">Apellidos</th>
                    <th colspan="3" style="text-align:center">Documento</th>
                    <th>Nacionalidad</th>
                    <th>Edad</th>
                    <th>Genero</th>
                    <th>E. Civil</th>
                    <th>Nro de Hijos</th>
                    <th>Celular</th>
                    <th>Telefono</th>
                    <th>Direccion</th>

                    </thead>
                    @foreach ($empleados as $per)

                        <tr>
                            <td>{{$per->id_empleado}}</td>
                            <td>{{$per->primer_nombre}}</td>
                            <td>{{$per->segundo_nombre}}</td>
                            <td>{{$per->primer_apellido}}</td>
                            <td>{{$per->segundo_apellido}}</td>
                            <td>{{$per->cat_tipo_documento}}</td>
                            <td>{{$per->documento}}</td>
                            <td>{{$per->complemento1}}</td>
                            <td>{{$per->cat_nacionalidad}}</td>
                            <td>{{$per->edad}}</td>
                            <td>{{$per->cat_genero}}</td>
                            <td>{{$per->cat_estado_civil}}</td>
                            <td>{{$per->nhijos}}</td>
                            <td>{{$per->celular}}</td>
                            <td>{{$per->telefono}}</td>
                            <td>{{$per->direccion}}</td>
                            <td><a href="{{URL::action('EmpleadoController@edit',$per->id_empleado)}}"><button class="btn btn-success" title="Editar Empleado"><i class="fa fa-edit"></i></button></a></td>
                            <td><a href="" data-target="#modal-delete-{{$per->id_empleado}}" data-toggle="modal"><button class="btn btn-danger" title="Dar de Baja al Empleado"><i class="fa fa-trash"></i></button></a></td>
                            <td><a href="{{route('enviar',['id'=>$per->id_usuario,'ci'=>$per->documento])}}"><button class="btn btn-primary" title="Enviar datos login"><i class="fa fa-envelope"></i></button></a></td>
                            <td><a href="{{URL::action('FamiliarController@crear',$per->id_empleado)}}"><button class="btn btn-secondary" title="Registrar Familiar"><i class="fa fa-users"></i></button></a></td>
                            <td><a href="{{asset ('hojas/'.$per->hoja)}}"><button class="btn btn-warning" title="Descargar CV Preliminar"><i class="fa fa-download"></i></button></a></td>
                        </tr>
                        @include('empleado.modal')
                    @endforeach
                </table>
            {{$empleados->render()}}
        </div>
    </div>
@stop