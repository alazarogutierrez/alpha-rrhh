@extends ('layouts.admin')
@section('titulo_content')
    <div class="row">
        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
            <h2>Modificar Empleado: {{ $empleado->id_empleado}}</h2>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
            <h2><a href="{{URL::action('EmpleadoController@index')}}"><button class="btn btn-success" type="button">Volver</button></a></h2>
        </div>
    </div>
@endsection
@section ('contenido')
<div class="row">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        @if (count($errors)>0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        {!!Form::model($empleado,['method'=>'PATCH','route'=>['empleado.update',$empleado->id_empleado,$emp_seg->id_empleado,$emp_r->id_usuario,$emp_afp->id_empleado,$emp_ar->id_empleado],'files'=>'true'])!!}
        {{Form::token()}}
        <div class="form-group">
            <label for="nombre">Nombre*</label>
            <input type="text" name="primer_nombre" class="form-control" value="{{$empleado->primer_nombre}}" placeholder="Nombre">
        </div>
        <div class="form-group">
            <label for="nombre">Segundo Nombre</label>
            <input type="text" name="segundo_nombre" class="form-control" value="{{$empleado->segundo_nombre}}" placeholder="Segundo Nombre">
        </div>
        <div class="form-group">
            <label for="nombre">Tercer Nombre</label>
            <input type="text" name="tercer_nombre" class="form-control" value="{{$empleado->tercer_nombre}}" placeholder="Tercer Nombre">
        </div>
        <div class="form-group">
            <label for="nombre">Apellido*</label>
            <input type="text" name="primer_apellido" class="form-control" value="{{$empleado->primer_apellido}}" placeholder="Primer Apellido">
        </div>
        <div class="form-group">
            <label for="nombre">Segundo Apellido</label>
            <input type="text" name="segundo_apellido" class="form-control" value="{{$empleado->segundo_apellido}}" placeholder="Segundo Apellido">
        </div>
        <div class="form-group">
            <label for="nombre">Tercer Apellido</label>
            <input type="text" name="tercer_apellido" class="form-control" value="{{$empleado->tercer_apellido}}" placeholder="Tercer Apellido">
        </div>
        <div class="form-group">
            <label for="nombre">Documento</label>
            <input type="text" name="documento" class="form-control" value="{{$empleado->documento}}" placeholder="Dcoumento"><br>
            <input type="text" name="complemento1" class="form-control" value="{{$empleado->complemento1}}" placeholder="Complemento 1"><br>
            <input type="text" name="complemento2" class="form-control" value="{{$empleado->complemento2}}" placeholder="Complemento 2">
        </div>
        <div class="form-group">
            <label>Tipo Documento*</label>
            <select name="cat_tipo_documento" class="form-control">
                @if($empleado->cat_tipo_documento=='CI')
                <option value="CI" selected>CI</option>
                <option value="RUN">RUN</option>
                <option value="PPTE">PPTE</option>
                <option value="CE">CE</option>
                <option value="Otro">Otro</option>
                @elseif($empleado->cat_tipo_documento=='RUN')
                    <option value="CI">CI</option>
                    <option value="RUN" selected>RUN</option>
                    <option value="PPTE">PPTE</option>
                    <option value="CE">CE</option>
                    <option value="Otro">Otro</option>
                @elseif($empleado->cat_tipo_documento=='PPTE')
                    <option value="CI">CI</option>
                    <option value="RUN">RUN</option>
                    <option value="PPTE" selected>PPTE</option>
                    <option value="CE">CE</option>
                    <option value="Otro">Otro</option>
                @elseif($empleado->cat_tipo_documento=='CE')
                    <option value="CI">CI</option>
                    <option value="RUN">RUN</option>
                    <option value="PPTE">PPTE</option>
                    <option value="CE" selected>CE</option>
                    <option value="Otro">Otro</option>
                @elseif($empleado->cat_tipo_documento=='Otro')
                    <option value="CI">CI</option>
                    <option value="RUN">RUN</option>
                    <option value="PPTE">PPTE</option>
                    <option value="CE">CE</option>
                    <option value="Otro" selected>Otro</option>
                @endif
            </select>
        </div>
        <div class="form-group">
            <label>Genero*</label>
        <select name="cat_genero" class="form-control">
            @if($empleado->cat_genero=='Masculino')
                <option value="Masculino" selected>Masculino</option>
                <option value="Femenino">Femenino</option>
            @elseif($empleado->cat_genero=='Femenino')
                <option value="Masculino">Masculino</option>
                <option value="Femenino" selected>Femenino</option>
            @endif
        </select>
        </div>
        <div class="form-group">
            <label for="nombre">Fecha Nacimiento*</label>
            <input type="date" name="fecha_nacimiento" class="form-control" value="{{$empleado->fecha_nacimiento}}" placeholder="Fecha de Nacimiento">
        </div>
        <div class="form-group">
            <label for="nombre">Celular</label>
            <input type="text" name="celular" class="form-control" value="{{$empleado->celular}}" placeholder="Celular">
        </div>
        <div class="form-group">
            <label for="nombre">Telefono</label>
            <input type="text" name="telefono" class="form-control" value="{{$empleado->telefono}}" placeholder="Telefono">
        </div>
        <div class="form-group">
            <label for="nombre">Direccion</label>
            <input type="text" name="direccion" class="form-control" value="{{$empleado->direccion}}" placeholder="Direccion">
        </div>
        <div class="form-group">
            <label>Estado Civil*</label>
            <select name="cat_estado_civil" class="form-control">
                @if($empleado->cat_estado_civil=='Soltero')
                <option value="Soltero" selected>Soltero</option>
                <option value="Casado">Casado</option>
                <option value="Divorciado">Divorciado</option>
                <option value="Viudo">Viudo</option>
                @elseif($empleado->cat_estado_civil=='Casado')
                    <option value="Soltero">Soltero</option>
                    <option value="Casado" selected>Casado</option>
                    <option value="Divorciado">Divorciado</option>
                    <option value="Viudo">Viudo</option>
                @elseif($empleado->cat_estado_civil=='Divorciado')
                    <option value="Soltero">Soltero</option>
                    <option value="Casado">Casado</option>
                    <option value="Divorciado" selected>Divorciado</option>
                    <option value="Viudo">Viudo</option>
                @elseif($empleado->cat_estado_civil=='Viudo')
                    <option value="Soltero">Soltero</option>
                    <option value="Casado">Casado</option>
                    <option value="Divorciado">Divorciado</option>
                    <option value="Viudo" selected>Viudo</option>
                @endif
            </select>
        </div>
        <div class="form-group">
            <label>Pais de Nacionalidad*</label>
            <select name="cat_nacionalidad" class="form-control">
                <option value="AR">Argentina</option>
                <option value="AF">Afganistan</option>
                <option value="AL">Albania</option>
                <option value="DE">Alemania</option>
                <option value="AD">Andorra</option>
                <option value="AO">Angola</option>
                <option value="AI">Anguilla</option>
                <option value="AQ">Antártida</option>
                <option value="AG">Antigua/Barbados</option>
                <option value="AN">Antillas Holandesas</option>
                <option value="SA">Arabia Saudita</option>
                <option value="DZ">Argelia</option>
                <option value="AM">Armenia</option>
                <option value="AW">Aruba</option>
                <option value="AZ">Aserbaidjan</option>
                <option value="AU">Australia</option>
                <option value="AT">Austria</option>
                <option value="BS">Bahamas</option>
                <option value="BD">Bangladesh</option>
                <option value="BB">Barbados</option>
                <option value="BE">Belgica</option>
                <option value="BZ">Belize</option>
                <option value="BJ">Benin</option>
                <option value="BM">Bermuda</option>
                <option value="BH">Bharain</option>
                <option value="BT">Bhutan</option>
                <option value="BOL" selected>Bolivia</option>
                <option value="BA">Bosnia-Herz.</option>
                <option value="BW">Botswana</option>
                <option value="BV">Bouvet Island</option>
                <option value="BR">Brasil</option>
                <option value="IO">Brit.Ind.Oc.Ter</option>
                <option value="VG">Brit.Virgin Is.</option>
                <option value="BN">Brunei Dar-es-S</option>
                <option value="BG">Bulgaria</option>
                <option value="BF">Burkina-Faso</option>
                <option value="BU">Burma</option>
                <option value="BI">Burundi</option>
                <option value="KH">Camboya</option>
                <option value="CM">Camerun</option>
                <option value="CA">Canada</option>
                <option value="CF">Central Afr.Rep</option>
                <option value="TD">Chad</option>
                <option value="CZ">Chechenia</option>
                <option value="CL">Chile</option>
                <option value="CN">China</option>
                <option value="CY">Chipre</option>
                <option value="CC">Coconut Islands</option>
                <option value="CO">Columbia</option>
                <option value="KM">Comoro Is.</option>
                <option value="CG">Congo</option>
                <option value="CK">Cook Islands</option>
                <option value="KP">Corea del Norte</option>
                <option value="KR">Corea del Sur</option>
                <option value="CI">Costa de Marfil</option>
                <option value="CR">Costa Rica</option>
                <option value="HR">Croacia</option>
                <option value="CU">Cuba</option>
                <option value="DK">Dinamarca</option>
                <option value="DJ">Djibouti</option>
                <option value="DM">Dominica</option>
                <option value="TP">East Timor</option>
                <option value="EC">Ecuador</option>
                <option value="EG">Egipto</option>
                <option value="SV">El Salvador</option>
                <option value="AE">Emiratos Arabes Unidos</option>
                <option value="ER">Eritrea</option>
                <option value="SI">Eslovenia</option>
                <option value="ES">España</option>
                <option value="EE">Estonia</option>
                <option value="ET">Etiopía</option>
                <option value="FO">Faroe Islands</option>
                <option value="RU">Federación Rusa</option>
                <option value="FJ">Fiji</option>
                <option value="PH">Filipinas</option>
                <option value="FI">Finlandia</option>
                <option value="FR">Francia</option>
                <option value="PF">Fren.Polynesia</option>
                <option value="GF">French Guinea</option>
                <option value="GA">Gabon</option>
                <option value="GM">Gambia</option>
                <option value="GE">Georgia</option>
                <option value="GH">Ghana</option>
                <option value="GI">Gibraltar</option>
                <option value="GB">Gran Bretaña</option>
                <option value="GD">Granada</option>
                <option value="GR">Grecia</option>
                <option value="GL">Groenlandia</option>
                <option value="GP">Guadalupe</option>
                <option value="GU">Guam</option>
                <option value="GT">Guatemala</option>
                <option value="GQ">Gui.Ecuatorial</option>
                <option value="GN">Guinea</option>
                <option value="GW">Guinea-Bissau</option>
                <option value="GY">Guyana</option>
                <option value="HT">Haiti</option>
                <option value="HM">Heard/McDon.Isl</option>
                <option value="HN">Honduras</option>
                <option value="HK">Hong Kong</option>
                <option value="HU">Hungría</option>
                <option value="IN">India</option>
                <option value="ID">Indonesia</option>
                <option value="IR">Iran</option>
                <option value="IQ">Iraq</option>
                <option value="IE">Irlanda</option>
                <option value="CV">Isla Cabo Verde</option>
                <option value="CX">Isla de Pascua</option>
                <option value="IS">Islandia</option>
                <option value="KY">Islas Caimán</option>
                <option value="FK">Islas Malvinas</option>
                <option value="IL">Israel</option>
                <option value="IT">Italia</option>
                <option value="JM">Jamaica</option>
                <option value="JP">Japan</option>
                <option value="JO">Jordania</option>
                <option value="KZ">Kazakhstan</option>
                <option value="KE">Kenia</option>
                <option value="KG">Kirghizstan</option>
                <option value="KI">Kiribati</option>
                <option value="KW">Kuwait</option>
                <option value="LA">Laos</option>
                <option value="LV">Latvia</option>
                <option value="LS">Lesotho</option>
                <option value="LB">Líbano</option>
                <option value="LR">Liberia</option>
                <option value="LY">Libia</option>
                <option value="LI">Liechtenstein</option>
                <option value="LT">Lituania</option>
                <option value="LU">Luxemburgo</option>
                <option value="MO">Macau</option>
                <option value="MK">Macedonia</option>
                <option value="MG">Madagascar</option>
                <option value="MY">Malasia</option>
                <option value="MW">Malawi</option>
                <option value="MV">Maldives</option>
                <option value="ML">Mali</option>
                <option value="MT">Malta</option>
                <option value="MA">Marruecos</option>
                <option value="MH">Marshall island</option>
                <option value="MQ">Martinique</option>
                <option value="MR">Mauritania</option>
                <option value="MU">Mauritius</option>
                <option value="XM">Mayotte</option>
                <option value="YT">Mayotte</option>
                <option value="MX">Mexico</option>
                <option value="FM">Micronesia</option>
                <option value="UM">Minor Outl.Ins.</option>
                <option value="MD">Moldavia</option>
                <option value="MC">Monaco</option>
                <option value="MN">Mongolia</option>
                <option value="MS">Montserrat</option>
                <option value="MZ">Mozambique</option>
                <option value="MM">Myanmar</option>
                <option value="NA">Namibia</option>
                <option value="NR">Nauru</option>
                <option value="NP">Nepal</option>
                <option value="NL">Netherlands</option>
                <option value="NC">New Caledonia</option>
                <option value="NI">Nicaragua</option>
                <option value="NE">Nigeria</option>
                <option value="NU">Niue</option>
                <option value="NF">Norfolk Island</option>
                <option value="MP">North Mariana</option>
                <option value="YE">North Yemen</option>
                <option value="NO">Noruega</option>
                <option value="NZ">Nueva Zelandia</option>
                <option value="OM">Oman</option>
                <option value="PK">Pakistan</option>
                <option value="PW">Palau</option>
                <option value="PA">Panama</option>
                <option value="PG">Papua Nw Guinea</option>
                <option value="PY">Paraguay</option>
                <option value="PE">Peru</option>
                <option value="PN">Pitcairn Islnds</option>
                <option value="PL">Polonia</option>
                <option value="PT">Portugal</option>
                <option value="PR">Puerto Rico</option>
                <option value="QA">Qatar</option>
                <option value="DO">Rep. Dominicana</option>
                <option value="RE">Reunion</option>
                <option value="RW">Ruanda</option>
                <option value="RO">Rumania</option>
                <option value="BY">Rusia Blanca</option>
                <option value="ST">S.Tome,Principe</option>
                <option value="AS">Samoa,Americana</option>
                <option value="SM">San Marino</option>
                <option value="SN">Senegal</option>
                <option value="SC">Seychelles</option>
                <option value="SL">Sierra Leona</option>
                <option value="SG">Singapur</option>
                <option value="SK">Slowakei</option>
                <option value="SB">Solomon Islands</option>
                <option value="SO">Somalia</option>
                <option value="ZA">South Africa</option>
                <option value="YD">South Yemen</option>
                <option value="LK">Sri Lanka</option>
                <option value="SH">St. Helena</option>
                <option value="LC">St. Lucia</option>
                <option value="VC">St. Vincent</option>
                <option value="KN">St.Chr.,Nevis</option>
                <option value="PM">St.Pier,Miquel.</option>
                <option value="GS">Sth Sandwich Is</option>
                <option value="TF">Sth Terr. Franc</option>
                <option value="SD">Sudan</option>
                <option value="CH">Suiza</option>
                <option value="SR">Surinam</option>
                <option value="SJ">Svalbard</option>
                <option value="SZ">Swaziland</option>
                <option value="SE">Sweden</option>
                <option value="SY">Syria</option>
                <option value="TJ">Tadjikistan</option>
                <option value="TW">Taiwan</option>
                <option value="TZ">Tanzania</option>
                <option value="TH">Thailand</option>
                <option value="TG">Togo</option>
                <option value="TK">Tokelau Islands</option>
                <option value="TO">Tonga</option>
                <option value="TT">Trinidad,Tobago</option>
                <option value="TN">Tunisia</option>
                <option value="TR">Turkey</option>
                <option value="TM">Turkmenistan</option>
                <option value="TC">Turks&Caicos Is</option>
                <option value="TV">Tuvalu</option>
                <option value="UG">Uganda</option>
                <option value="UA">Ukraine</option>
                <option value="US">United States</option>
                <option value="UY">Uruguay</option>
                <option value="VI">US Virgin Is.</option>
                <option value="UZ">Uzbekistan</option>
                <option value="VU">Vanuatu</option>
                <option value="VA">Vatikanstadt</option>
                <option value="VE">Venezuela</option>
                <option value="VN">Vietnam</option>
                <option value="WF">Wallis,Futuna</option>
                <option value="EH">West Sahara</option>
                <option value="WS">Western Samoa</option>
                <option value="YU">Yugoslavia</option>
                <option value="ZR">Zaire</option>
                <option value="ZM">Zambia</option>
                <option value="ZW">Zimbabwe</option>
            </select>
        </div>
        <div class="form-group">
            <label>Discapacidad</label>
            <select name="cat_discapacidad" class="form-control">
                @if($empleado->cat_discapacidad=='Grado 1')
                <option value="Grado 1" selected>Grado 1</option>
                <option value="Grado 2">Grado 2</option>
                <option value="Grado 3">Grado 3</option>
                <option value="Ninguna">Ninguna</option>
                @elseif($empleado->cat_discapacidad=='Grado 2')
                    <option value="Grado 1">Grado 1</option>
                    <option value="Grado 2" selected>Grado 2</option>
                    <option value="Grado 3">Grado 3</option>
                    <option value="Ninguna">Ninguna</option>
                @elseif($empleado->cat_discapacidad=='Grado 3')
                    <option value="Grado 1">Grado 1</option>
                    <option value="Grado 2">Grado 2</option>
                    <option value="Grado 3" selected>Grado 3</option>
                    <option value="Ninguna">Ninguna</option>
                @elseif($empleado->cat_discapacidad=='Ninguna')
                    <option value="Grado 1">Grado 1</option>
                    <option value="Grado 2">Grado 2</option>
                    <option value="Grado 3">Grado 3</option>
                    <option value="Ninguna" selected>Ninguna</option>
                @endif
            </select>
        </div>
        <div class="form-group">
            <label for="hoja">Curriculum Vitae Preliminar</label>
            <input type="file" name="hoja">
        </div>
        <div class="form-group">
            <label for="nombre">Seguro</label>
            <select class="form-control" name="seguro">
                @foreach($seguro as $s)
                    @if ($s->id_seguro==$emp_seg->id_seguro)
                        <option value="{{$s->id_seguro}}" selected>{{$s->cat_seguro}}</option>
                    @else
                        <option value="{{$s->id_seguro}}" >{{$s->cat_seguro}}</option>
                    @endif
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="nombre">Afp a aportar</label>
            <select class="form-control" name="afp">
                @foreach($afp as $s)
                    @if ($s->id_afp==$emp_afp->id_afp)
                        <option value="{{$s->id_afp}}" selected>{{$s->cat_afp}}</option>
                    @else
                        <option value="{{$s->id_afp}}" >{{$s->cat_afp}}</option>
                    @endif
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="nombre">Area de Trabajo</label>
            <select class="form-control" name="area">
                @foreach($area as $s)
                    @if ($s->id_area==$emp_ar->id_area)
                        <option value="{{$s->id_area}}" selected>{{$s->nombre}}</option>
                    @else
                        <option value="{{$s->id_area}}" >{{$s->nombre}}</option>
                    @endif
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <button class="btn btn-primary" type="submit">Guardar</button>
            <button class="btn btn-danger" type="reset">Cancelar</button>
        </div>

        {!!Form::close()!!}

    </div>
</div>
@stop