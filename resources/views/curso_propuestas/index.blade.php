@extends ('layouts.admin')
@section('titulo_content')
    Curso de {{$curso->tema}}
@endsection
@section ('contenido')
    <div class="row">
        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
            <h5>Costo solicitado: {{$curso->presupuesto_solicitado}} (Bs)</h5>
            <h5>Estudiantes: {{$curso->estudiantes}} </h5>
            <h5>Total: {{$curso->estudiantes*$curso->presupuesto_solicitado}} (Bs)</h5>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
            <a href="{{URL::action('CursoPropuestaController@edit',$curso->id_capacitacion)}}"><button class="btn btn-success">Nueva Propuesta</button></a>
            <a href="" data-target="#modal-delete-{{$curso->id_capacitacion}}" data-toggle="modal"><button class="btn btn-info">Ver Temario</button></a>

        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-lg-12 offset-lg-12 ">

            <div class="table-responsive">
                <table class="table table-striped table-bordered table-condensed table-hover">
                    <thead>
                        <th>Institucion</th>
                        <th>Costo / estudiante (Bs)</th>
                        <th>Total (Bs)</th>
                        <th>CV Docnte/es</th>
                        <th>Temario Propuesto</th>
                    </thead>
                    <tbody>
                        @foreach($curso2 as $item)
                             <tr>
                                 <td>{{$item->instituto}}</td>
                                 <td>{{$item->costo}}</td>
                                 <td>{{$item->costo*$curso->estudiantes}}</td>
                                 <td>
                                     <a href="{{asset ('cv_docentes/'.$item->descripcion_cv)}}"><button class="btn btn-default" title="Descargar Hoja de Vida"><i class="fa fa-download"></i></button></a>
                                 </td>
                                 <td>
                                     <a href="{{asset ('propuestas/'.$item->descripcion)}}"><button class="btn btn-default" title="Descargar Hoja de Vida"><i class="fa fa-download"></i></button></a>
                                 </td>
                                 <td>
                                     <a href="" data-target="#modal-delete-{{$item->id_capacitacion_propuesta}}-elegir" data-toggle="modal"><button class="btn btn-success">Elegir</button></a>
                                     <a href="" data-target="#modal-delete-{{$item->id_capacitacion_propuesta}}-eliminar" data-toggle="modal"><button class="btn btn-danger">Eliminar</button></a>
                                     <a href="{{URL::action('CursoPropuestaController@show',$item->id_capacitacion_propuesta)}}"><button class="btn btn-info">Editar</button></a>
                                 </td>

                                 @include('curso_propuestas.modal')
                                 @include('curso_propuestas.modal2')
                                 @include('curso_propuestas.modal3')
                             <tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop