<div class="modal fade modal-slide-in-right" aria-hidden="true"
     role="dialog" tabindex="-1" id="modal-delete-{{$item->id_capacitacion_propuesta}}-eliminar">
    {{Form::Open(array('action'=>array('CursoPropuestaController@eliminar_propuesta',$curso->id_capacitacion,$item->id_capacitacion_propuesta),'method'=>'delete'))}}
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Eliminar Propuesta</h4>
            </div>
            <div class="modal-body">
                <p>Confirme si desea Eliminar la Propuesta</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-primary">Confirmar</button>
            </div>
        </div>
    </div>
    {{Form::Close()}}

</div>