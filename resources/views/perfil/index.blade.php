@extends ('layouts.admin')
@section('titulo_content')
    <h2>Cambio de Contraseña</h2>
@endsection
@section ('contenido')

    <div class="row">
        <div class="col-lg-12 offset-lg-12 ">
            {!!Form::model($user,['method'=>'PATCH','route'=>['perfil.update',$user->id_usuario]])!!}
            {{Form::token()}}
            <div class="col-xs-offset-4">
                <div class="row">
                    <div class="col-sm-7">
                        <div class="form-group">
                            <label for="nombre">Antigua Contraseña</label>
                            <input type="password" name="a_password" class="form-control" value="">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-7">
                        <div class="form-group">
                            <label for="nombre">Nueva Contraseña</label>
                            <input type="password" name="n_password" class="form-control" value="">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-7">
                        <div class="form-group">
                            <button class="btn btn-primary float-right" type="submit">Guardar</button>
                            <button class="btn btn-danger float-right" type="reset">Cancelar</button>
                        </div>
                    </div>
                </div>
            </div>


            {!!Form::close()!!}
        </div>
    </div>
@stop