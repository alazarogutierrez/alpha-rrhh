<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>REPORTE</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
<body>
<div class="container">
    <div align="center">
        <h1>REPORTE DE CURSO</h1>
    </div>
                @foreach($curso as $c)
                    <p><strong>Tema</strong> {{$c->tema}}</p>
                    <p><strong>Estudiantes</strong> {{$c->estudiantes}}</p>
                    <p><strong>Inversion</strong> {{$c->costo*$c->estudiantes}} (Bs)</p>
                    <p><strong>Instituto</strong> {{$c->instituto}}</p>
                    <p><strong>Numero de sesiones</strong> {{$c->sesiones}}</p>
                    <p><strong>Fecha inicio</strong> {{$c->fecha_inicio}}</p>
                    <p><strong>Fecha finalizacion</strong> {{$c->fecha_fin}}</p>
                @endforeach
    <div class="row">
        <h3>Horarios</h3>
    </div>
    <div align="center">
        <div class="col-lg-8">
            <table class="table table-bordered">
                <thead class="thead-dark">
                <tr>
                    <th>Lunes</th>
                    <th>Martes</th>
                    <th>Miercoles</th>
                    <th>Jueves</th>
                    <th>Viernes</th>
                    <th>Sabado</th>
                    <th>Domingo</th>
                </tr>
                </thead>
                <tbody>
                <tr id="semana">
                    <td id="Lunes" class="dias">
                        @foreach($horarios as $h)
                            @if($h->dia=="Lunes")
                                <div class="item clearfix dias_hora" id="{{$h->hora_ini}}-{{$h->hora_fin}}">
                                    <div class="item__description">
                                        {{$h->hora_ini}}-{{$h->hora_fin}}
                                    </div>
                                </div>
                            @endif
                        @endforeach

                    </td>
                    <td id="Martes" class="dias">
                        @foreach($horarios as $h)
                            @if($h->dia=="Martes")
                                <div class="item clearfix dias_hora" id="{{$h->hora_ini}}-{{$h->hora_fin}}">
                                    <div class="item__description">
                                        {{$h->hora_ini}}-{{$h->hora_fin}}
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </td>
                    <td id="Miercoles" class="dias">
                        @foreach($horarios as $h)
                            @if($h->dia=="Miercoles")
                                <div class="item clearfix dias_hora" id="{{$h->hora_ini}}-{{$h->hora_fin}}">
                                    <div class="item__description">
                                        {{$h->hora_ini}}-{{$h->hora_fin}}
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </td>
                    <td id="Jueves" class="dias">
                        @foreach($horarios as $h)
                            @if($h->dia=="Jueves")
                                <div class="item clearfix dias_hora" id="{{$h->hora_ini}}-{{$h->hora_fin}}">
                                    <div class="item__description">
                                        {{$h->hora_ini}}-{{$h->hora_fin}}
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </td>
                    <td id="Viernes" class="dias">
                        @foreach($horarios as $h)
                            @if($h->dia=="Viernes")
                                <div class="item clearfix dias_hora" id="{{$h->hora_ini}}-{{$h->hora_fin}}">
                                    <div class="item__description">
                                        {{$h->hora_ini}}-{{$h->hora_fin}}
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </td>
                    <td id="Sabado" class="dias">
                        @foreach($horarios as $h)
                            @if($h->dia=="Sabado")
                                <div class="item clearfix dias_hora" id="{{$h->hora_ini}}-{{$h->hora_fin}}">
                                    <div class="item__description">
                                        {{$h->hora_ini}}-{{$h->hora_fin}}
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </td>
                    <td id="Domingo" class="dias">
                        @foreach($horarios as $h)
                            @if($h->dia=="Domingo")
                                <div class="item clearfix dias_hora" id="{{$h->hora_ini}}-{{$h->hora_fin}}">
                                    <div class="item__description">
                                        {{$h->hora_ini}}-{{$h->hora_fin}}
                                    </div>

                                </div>
                            @endif
                        @endforeach
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <div class="row">
        <h3>Notas</h3>
    </div>
    <div class="row">
        <table class="table">
            <thead>
            <tr>
                <th scope="col">Domento</th>
                <th scope="col">Nombre</th>
                <th scope="col">Nota</th>
                <th scope="col">Clasificacion</th>
            </tr>
            </thead>
            <tbody>
            @foreach($nota_maxima as $nmax)
                <tr class="table-success">
                    <th>{{$nmax->documento}}</th>
                    <td>{{strtoupper($nmax->primer_apellido)}} {{strtoupper($nmax->primer_nombre)}} </td>
                    <td>{{$nmax->nota_final}}</td>
                    <td>MAXIMA</td>
                </tr>
            @endforeach
            @foreach($nota_minima as $nmin)
                <tr class="table-danger">
                    <th>{{$nmin->documento}}</th>
                    <td>{{strtoupper($nmin->primer_apellido)}} {{strtoupper($nmin->primer_nombre)}} </td>
                    <td>{{$nmin->nota_final}}</td>
                    <td>MINIMA</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <br>
    <div class="row">
        <h3>Alumnos</h3>
    </div>
    <div class="row">
        <table class="table">
            <thead>
            <tr>
                <th scope="col">Domento</th>
                <th scope="col">Nombre</th>
                <th scope="col">Nota</th>
                <th scope="col">Asistencia</th>
                <th scope="col">Resultado</th>
            </tr>
            </thead>
            <tbody>
            @foreach($empleado as $emp)
                <tr class="table-light">
                    <th>{{$emp->documento}}</th>
                    <td>{{strtoupper($emp->primer_apellido)}} {{strtoupper($emp->primer_nombre)}} </td>
                    <td>{{$emp->nota_final}} / 100</td>
                    <td>{{$emp->asistencia}} / 100</td>
                    <td>{{$emp->desempeno}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>




<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
</body>
</html>