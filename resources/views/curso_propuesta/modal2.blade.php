<div class="modal fade modal-slide-in-right" aria-hidden="true"
     role="dialog" tabindex="-1" id="modal-horario">
    <div class="modal-dialog">
        <div class="modal-content">
            <form>
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"
                                aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title">Agregar Horario</h4>
                    </div>
                    <div class="modal-body">

                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="fecha_fin">Dia</label>
                                        <select class="form-control" id="dia">
                                            <option value="Lunes">Lunes</option>
                                            <option value="Martes">Martes</option>
                                            <option value="Miercoles">Miercoles</option>
                                            <option value="Jueves">Jueves</option>
                                            <option value="Viernes">Viernes</option>
                                            <option value="Sabado">Sabado</option>
                                            <option value="Domingo">Domingo</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="fecha_fin">Hora inicio</label>
                                        <div class='input-group date' id='datetimepicker1'>
                                            <input type='time' class="form-control" value="00:00" id="hora_i">
                                            <span class="input-group-addon">
                                                <span class="fa fa-clock-o"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="fecha_fin">Hora final</label>
                                        <div class='input-group date' id='datetimepicker1'>
                                            <input type='time' class="form-control" value="00:00" id="hora_f">
                                            <span class="input-group-addon">
                                                    <span class="fa fa-clock-o"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" >Cerrar</button>
                        <button type="button" class="btn btn-primary" id="aceptar" data-dismiss="modal">Aceptar</button>
                    </div>
            </form>
        </div>
    </div>
</div>