@extends ('layouts.admin')
@section('script_propuesta')
    <script src="{{asset('js/hola.js')}}"></script>
    @endsection
@section('titulo_content')
    Agregar propuesta
@endsection
@section ('contenido')
<div class="row">
        <div class="col-lg-6">
            {!!Form::open(array('url'=>'curso_propuesta','method'=>'GET','autocomplete'=>'off','files'=>'true','name'=>'registro_propuesta'))!!}

                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label for="nombre_curso">Nombre Curso</label>
                            <input type="text" name="tema" id="tema" class="form-control" value="{{$curso->tema}}" readonly>
                            <input type="hidden" id="tipo_form" value="agregar">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="institucion">Institucion*</label>
                            <input type="text" name="institucion" id="institucion" class="form-control" value="cognos" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-8">
                        <div class="form-group">
                            <label for="fecha_fin">Direccion*</label>
                            <input type="text" name="direccion" id="direccion" class="form-control" value="callle 3">
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="presupuesto">Costo / estudiante *</label>
                            <input type="number" name="costo" id="costo" class="form-control" min="1" max="{{$curso->presupuesto}}" value="333"required>
                            <input type="hidden" name="id" id="id" value="{{$curso->id_capacitacion}}">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-5">
                        <div class="form-group">
                            <label for="fecha_inicio">Fecha Inicio*</label>
                            <input type="date"  name="fecha_inicio" id="fecha_inicio" class="form-control" min="{{date('Y-m-d')}}" required >
                        </div>
                    </div>
                    <div class="col-lg-5">
                        <div class="form-group">
                            <label for="fecha_fin">Fecha Finalización*</label>
                            <input type="date" name="fecha_fin" id="fecha_fin" class="form-control"  min="{{date('Y-m-d')}}" required >
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            <label for="fecha_fin">No. Sesiones*</label>
                            <input type="number" name="sesiones"  id="sesiones" class="form-control" min="1"  value="4" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="descripcion">Contenido del curso *</label>
                            <input type="file" name="descripcion"  id="descripcion" class="form-control" accept="application/pdf">
                        </div>

                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="descripcion">Hoja de vida del docente/es*</label>
                            <input type="file" name="cv_docentes" id="cv_docentes"  class="form-control" accept="application/pdf">
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-8">
                        <div class="form-group">
                            <button class="btn btn-primary float-right" type="submit"  id="guardar" >Guardar</button>
                        </div>
                    </div>

                </div>
            {!!Form::close()!!}
        </div>
        <div class="col-lg-6 ">
            <div class="form-group">
                <label for="fecha_fin">Horarios*
                    <a href="" data-target="#modal-horario" data-toggle="modal">
                        <button class="btn btn-default btn-sm"><i class="fa fa-plus"></i></button>


                    </a>
                    <!--<button class="btn btn-default btn-sm" id="probar"><i class="fa fa-plus-circle"></i></button>-->
                    @include('curso_propuesta.modal2')
                </label>
            </div>

            <table class="table table-bordered">
                <thead>
                <tr class="bg-navy-active">
                    <th>Lunes</th>
                    <th>Martes</th>
                    <th>Miercoles</th>
                    <th>Jueves</th>
                    <th>Viernes</th>
                    <th>Sabado</th>
                    <th>Domingo</th>
                </tr>
                </thead>
                <tbody>
                <tr id="semana">
                    <td id="Lunes" class="dias"></td>
                    <td id="Martes" class="dias"></td>
                    <td id="Miercoles" class="dias"></td>
                    <td id="Jueves" class="dias"></td>
                    <td id="Viernes" class="dias"></td>
                    <td id="Sabado" class="dias"></td>
                    <td id="Domingo" class="dias"></td>
                </tr>
                </tbody>
            </table>

        </div>
</div>
@endsection