@extends ('layouts.admin')
@section('titulo_content')
   Asignacion de propuestas
@endsection
@section ('contenido')
    <div class="row">
        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
            @include('curso_propuesta.search')
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 offset-lg-12 ">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-condensed table-hover">
                    <thead>
                    <th>Tema</th>
                    <th>Presupuesto(Bs)</th>
                    <th>Tiempo en Espera</th>
                    </thead>
                    @foreach ($cursos as $per)

                            <tr>
                                <td>{{$per->tema}}</td>
                                <td>{{$per->presupuesto}}</td>
                                <td>{{\Carbon\Carbon::createFromTimeStamp(strtotime($per->fch_ini))->diffForHumans()}}</td>
                                <td>
                                    <a href="{{URL::action('CursoPropuestaController@edit',$per->id_capacitacion)}}"><button class="btn btn-success" >Asignar Propuesta</button></a>
                                    <a href="" data-target="#modal-rechazar-{{$per->id_capacitacion}}" data-toggle="modal"><button class="btn btn-danger">Sin Propuesta</button></a>
                                </td>
                            </tr>
                        @include('curso_propuesta.modal')
                    @endforeach
                </table>
            </div>
            {{$cursos->render()}}
        </div>
    </div>
@stop