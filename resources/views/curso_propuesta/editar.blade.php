@extends ('layouts.admin')
@section('script_propuesta')
    <script src="{{asset('js/hola.js')}}"></script>
@endsection
@section('titulo_content')
    Agregar propuesta
@endsection
@section ('contenido')
    <div class="row">
        <div class="col-lg-6">
            {!!Form::open(array('url'=>'curso_propuesta','method'=>'POST','autocomplete'=>'off','files'=>'true','name'=>'curso_propuesta.actualizar_propuesta'))!!}

            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="nombre_curso">Nombre Curso</label>
                        <input type="text" name="tema" id="tema" class="form-control" value="{{$curso->tema}}" readonly>
                        <input type="hidden" id="tipo_form" name="tipo_form" value="actualizar">
                        <input type="hidden" id="id_propuesta" name="id_propuesta" value="{{$propuesta->id_capacitacion_propuesta}}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="institucion">Institucion*</label>
                        <input type="text" name="institucion" id="institucion" class="form-control" value="{{$propuesta->instituto}}" required>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8">
                    <div class="form-group">
                        <label for="fecha_fin">Direccion*</label>
                        <input type="text" name="direccion" id="direccion" class="form-control" value="{{$propuesta->direccion}}" required>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label for="presupuesto">Costo / estudiante *</label>
                        <input type="number" name="costo" id="costo" class="form-control" min="1" max="{{$curso->presupuesto}}" value="{{$propuesta->costo}}"required>
                        <input type="hidden" name="id" id="id" value="{{$curso->id_capacitacion}}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-5">
                    <div class="form-group">
                        <label for="fecha_inicio">Fecha Inicio*</label>
                        <input type="date"  name="fecha_inicio" id="fecha_inicio" class="form-control" min="{{date('Y-m-d')}}" value="{{$propuesta->fecha_inicio}}" required>
                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="form-group">
                        <label for="fecha_fin">Fecha Finalización*</label>
                        <input type="date" name="fecha_fin" id="fecha_fin" class="form-control"  min="{{date('Y-m-d')}}" value="{{$propuesta->fecha_fin}}" required>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="form-group">
                        <label for="fecha_fin">No. Sesiones*</label>
                        <input type="number" name="sesiones"  id="sesiones" class="form-control" min="1"  value="{{$propuesta->sesiones}}" required>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="descripcion">Contenido del curso *</label>
                        <input type="file" name="descripcion"  id="descripcion" class="form-control">
                    </div>

                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="descripcion">Hoja de vida del docente/es*</label>
                        <input type="file" name="cv_docentes" id="cv_docentes" class="form-control">
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-sm-8">
                    <div class="form-group">
                        <button class="btn btn-primary float-right" type="submit" id="guardar">Guardar</button>
                    </div>
                </div>

            </div>
            {!!Form::close()!!}
        </div>
        <div class="col-lg-6 ">
            <div class="form-group">
                <label for="fecha_fin">Horarios*
                    <a href="" data-target="#modal-horario" data-toggle="modal">
                        <button class="btn btn-default btn-sm"><i class="fa fa-plus"></i></button>


                    </a>
                    <!--<button class="btn btn-default btn-sm" id="probar"><i class="fa fa-plus-circle"></i></button>-->
                    @include('curso_propuesta.modal2')
                </label>
            </div>

            <table class="table table-bordered">
                <thead>
                <tr class="bg-navy-active">
                    <th>Lunes</th>
                    <th>Martes</th>
                    <th>Miercoles</th>
                    <th>Jueves</th>
                    <th>Viernes</th>
                    <th>Sabado</th>
                    <th>Domingo</th>
                </tr>
                </thead>
                <tbody>
                <tr id="semana">
                        <td id="Lunes" class="dias">
                            @foreach($horarios as $h)
                                @if($h->dia=="Lunes")
                                    <div class="item clearfix dias_hora" id="{{$h->hora_ini}}-{{$h->hora_fin}}">
                                        <div class="item__description">
                                            {{$h->hora_ini}}-{{$h->hora_fin}}
                                        </div>
                                        <div class="right clearfix">
                                            <div class="item__delete">
                                                <button class="item__delete--btn"><i class="fa fa-remove"  style="color:red"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endforeach

                        </td>
                        <td id="Martes" class="dias">
                            @foreach($horarios as $h)
                                @if($h->dia=="Martes")
                                    <div class="item clearfix dias_hora" id="{{$h->hora_ini}}-{{$h->hora_fin}}">
                                        <div class="item__description">
                                            {{$h->hora_ini}}-{{$h->hora_fin}}
                                        </div>
                                        <div class="right clearfix">
                                            <div class="item__delete">
                                                <button class="item__delete--btn"><i class="fa fa-remove"  style="color:red"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </td>
                        <td id="Miercoles" class="dias">
                            @foreach($horarios as $h)
                                @if($h->dia=="Miercoles")
                                    <div class="item clearfix dias_hora" id="{{$h->hora_ini}}-{{$h->hora_fin}}">
                                        <div class="item__description">
                                            {{$h->hora_ini}}-{{$h->hora_fin}}
                                        </div>
                                        <div class="right clearfix">
                                            <div class="item__delete">
                                                <button class="item__delete--btn"><i class="fa fa-remove"  style="color:red"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </td>
                        <td id="Jueves" class="dias">
                            @foreach($horarios as $h)
                                @if($h->dia=="Jueves")
                                    <div class="item clearfix dias_hora" id="{{$h->hora_ini}}-{{$h->hora_fin}}">
                                        <div class="item__description">
                                            {{$h->hora_ini}}-{{$h->hora_fin}}
                                        </div>
                                        <div class="right clearfix">
                                            <div class="item__delete">
                                                <button class="item__delete--btn"><i class="fa fa-remove"  style="color:red"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </td>
                        <td id="Viernes" class="dias">
                            @foreach($horarios as $h)
                                @if($h->dia=="Viernes")
                                    <div class="item clearfix dias_hora" id="{{$h->hora_ini}}-{{$h->hora_fin}}">
                                        <div class="item__description">
                                            {{$h->hora_ini}}-{{$h->hora_fin}}
                                        </div>
                                        <div class="right clearfix">
                                            <div class="item__delete">
                                                <button class="item__delete--btn"><i class="fa fa-remove"  style="color:red"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </td>
                        <td id="Sabado" class="dias">
                            @foreach($horarios as $h)
                                @if($h->dia=="Sabado")
                                    <div class="item clearfix dias_hora" id="{{$h->hora_ini}}-{{$h->hora_fin}}">
                                        <div class="item__description">
                                            {{$h->hora_ini}}-{{$h->hora_fin}}
                                        </div>
                                        <div class="right clearfix">
                                            <div class="item__delete">
                                                <button class="item__delete--btn"><i class="fa fa-remove"  style="color:red"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </td>
                        <td id="Domingo" class="dias">
                            @foreach($horarios as $h)
                                @if($h->dia=="Domingo")
                                    <div class="item clearfix dias_hora" id="{{$h->hora_ini}}-{{$h->hora_fin}}">
                                        <div class="item__description">
                                            {{$h->hora_ini}}-{{$h->hora_fin}}
                                        </div>
                                        <div class="right clearfix">
                                            <div class="item__delete">
                                                <button class="item__delete--btn"><i class="fa fa-remove"  style="color:red"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </td>
                </tr>
                </tbody>
            </table>

        </div>
    </div>
@endsection