@extends ('layouts.admin')
@section('titulo_content')
    <h2>Hoja de Vida de Empleados</h2>
@endsection
@section ('contenido')
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            @include('hoja.search')
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 table-responsive">
            <table class="table table-striped table-bordered table-condensed table-hover">
                <thead>
                <th>Id</th>
                <th colspan="2" style="text-align:center">Nombres </th>
                <th colspan="2" style="text-align:center">Apellidos</th>
                <th colspan="3" style="text-align:center">Documento</th>


                </thead>
                @foreach ($empleados as $per)

                    <tr>
                        <td>{{$per->id_empleado}}</td>
                        <td>{{$per->primer_nombre}}</td>
                        <td>{{$per->segundo_nombre}}</td>
                        <td>{{$per->primer_apellido}}</td>
                        <td>{{$per->segundo_apellido}}</td>
                        <td>{{$per->cat_tipo_documento}}</td>
                        <td>{{$per->documento}}</td>
                        <td>{{$per->complemento1}}</td>
                        @if($per->hv==0)
                        <td><a href="{{URL::action('HojaController@crear',$per->id_empleado)}}"><button class="btn btn-warning" title="Registrar Hoja de Vida"><i class="fa fa-plus"></i></button></a></td>
                        @else
                            <td><a href="{{URL::action('HojaController@show',$per->id_empleado)}}"><button class="btn btn-primary" title="Ver Hoja de Vida"><i class="fa fa-eye"></i></button></a></td>
                            <td><a href="{{URL::action('HojaController@edit',$per->id_empleado)}}"><button class="btn btn-success" title="Editar Hoja de Vida"><i class="fa fa-edit"></i></button></a></td>
                            <td><a href="{{URL::action('HojaController@pdf',$per->id_empleado)}}"><button class="btn btn-secondary" title="Descargar Hoja de Vida en PDF"><i class="fa fa-download"></i></button></a></td>
                        @endif
                    </tr>
                @endforeach
            </table>
            {{$empleados->render()}}
        </div>
    </div>
@stop