@extends ('layouts.admin')
@section('titulo_content')
    <div class="row">
        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
            <h2>Ver Hoja de Vida</h2>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
            <h2><a href="{{URL::action('HojaController@index')}}"><button class="btn btn-success" type="button">Volver</button></a></h2>
        </div>
    </div>
@endsection
@section ('contenido')
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            @if (count($errors)>0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            {!!Form::model($hoja)!!}
            {{Form::token()}}
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <div class="form-group">
                        <h3>Nivel de Instruccion</h3>
                        <select name="nivel" class="form-control" readonly>
                            @if($hoja->nivel=='Bachiller')
                                <option value="Bachiller" selected>Bachiller</option>
                                <option value="Pregrado">Pregrado</option>
                                <option value="Postgrado">Postgrado</option>
                                <option value="Otro">Otro</option>
                            @elseif($hoja->nivel=='Pregrado')
                                <option value="Bachiller">Bachiller</option>
                                <option value="Pregrado" selected>Pregrado</option>
                                <option value="Postgrado">Postgrado</option>
                                <option value="Otro">Otro</option>
                            @elseif($hoja->nivel=='Postgrado')
                                <option value="Bachiller">Bachiller</option>
                                <option value="Pregrado">Pregrado</option>
                                <option value="Postgrado" selected>Postgrado</option>
                                <option value="Otro">Otro</option>
                            @elseif($hoja->nivel=='Otro')
                                <option value="Bachiller">Bachiller</option>
                                <option value="Pregrado">Pregrado</option>
                                <option value="Postgrado">Postgrado</option>
                                <option value="Otro" selected>Otro</option>
                            @endif
                        </select>
                    </div>
                    <h3>Colegiatura</h3>
                    <h4>Nivel Primaria</h4>
                    <div class="form-group">
                        <label>Institucion Educativa</label>
                        <input type="text" name="primaria" class="form-control" value="{{$hoja->primaria}}" placeholder="Institucion Educativa" readonly>
                    </div>
                    <div class="form-group">
                        <label>Año de Culminacion</label>
                        <input type="text" name="añoprimaria" class="form-control"  value="{{$hoja->añoprimaria}}" placeholder="Año de Culminacion" readonly>
                    </div>
                    <h4>Nivel Secundaria</h4>
                    <div class="form-group">
                        <label>Institucion Educativa</label>
                        <input type="text" name="secundaria" class="form-control"  value="{{$hoja->secundaria}}" placeholder="Institucion Educativa" readonly>
                    </div>
                    <div class="form-group">
                        <label>Año de Culminacion</label>
                        <input type="text" name="añosecundaria" class="form-control" value="{{$hoja->añosecundaria}}" placeholder="Año de Culminacion" readonly>
                    </div>
                    <h3>Pregrado</h3>
                    <h4>Pregrado 1</h4>
                    <div class="form-group">
                        <label>Universidad</label>
                        <input type="text" name="pregrado1" class="form-control" value="{{$hoja->pregrado1}}" placeholder="Universidad" readonly>
                    </div>
                    <div class="form-group">
                        <label>Titulo Obtenido</label>
                        <input type="text" name="tpregrado1" class="form-control" value="{{$hoja->tpregrado1}}" placeholder="Titulo Obtenido" readonly>
                    </div>
                    <div class="form-group">
                        <label>Año de Culminacion</label>
                        <input type="text" name="apregrado1" class="form-control" value="{{$hoja->apregrado1}}" placeholder="Año de Culminacion" readonly>
                    </div>
                    <h4>Pregrado 2</h4>
                    <div class="form-group">
                        <label>Universidad</label>
                        <input type="text" name="pregrado2" class="form-control" value="{{$hoja->pregrado2}}" placeholder="Universidad" readonly>
                    </div>
                    <div class="form-group">
                        <label>Titulo Obtenido</label>
                        <input type="text" name="tpregrado2" class="form-control" value="{{$hoja->tpregrado2}}" placeholder="Titulo Obtenido"readonly>
                    </div>
                    <div class="form-group">
                        <label>Año de Culminacion</label>
                        <input type="text" name="apregrado2" class="form-control" value="{{$hoja->apregrado2}}" placeholder="Año de Culminacion" readonly>
                    </div>
                    <h3>Postgrado</h3>
                    <h4>Postgrado 1</h4>
                    <div class="form-group">
                        <label>Universidad</label>
                        <input type="text" name="postgrado1" class="form-control" value="{{$hoja->postgrado1}}" placeholder="Universidad" readonly>
                    </div>
                    <div class="form-group">
                        <label>Titulo Obtenido</label>
                        <input type="text" name="tpostgrado1" class="form-control" value="{{$hoja->tpostgrado1}}" placeholder="Titulo Obtenido" readonly>
                    </div>
                    <div class="form-group">
                        <label>Año de Culminacion</label>
                        <input type="text" name="apostgrado1" class="form-control" value="{{$hoja->apostgrado1}}" placeholder="Año de Culminacion" readonly>
                    </div>
                    <h4>Postgrado 2</h4>
                    <div class="form-group">
                        <label>Universidad</label>
                        <input type="text" name="postgrado2" class="form-control" value="{{$hoja->postgrado2}}" placeholder="Universidad" readonly>
                    </div>
                    <div class="form-group">
                        <label>Titulo Obtenido</label>
                        <input type="text" name="tpostgrado2" class="form-control" value="{{$hoja->tpostgrado2}}" placeholder="Titulo Obtenido" readonly>
                    </div>
                    <div class="form-group">
                        <label>Año de Culminacion</label>
                        <input type="text" name="apostgrado2" class="form-control" value="{{$hoja->apostgrado2}}" placeholder="Año de Culminacion" readonly>
                    </div>
                    <h4>Postgrado 3</h4>
                    <div class="form-group">
                        <label>Universidad</label>
                        <input type="text" name="postgrado3" class="form-control" value="{{$hoja->postgrado3}}" placeholder="Universidad" readonly>
                    </div>
                    <div class="form-group">
                        <label>Titulo Obtenido</label>
                        <input type="text" name="tpostgrado3" class="form-control" value="{{$hoja->tpostgrado3}}" placeholder="Titulo Obtenido" readonly>
                    </div>
                    <div class="form-group">
                        <label>Año de Culminacion</label>
                        <input type="text" name="apostgrado3" class="form-control" value="{{$hoja->apostgrado3}}" placeholder="Año de Culminacion" readonly>
                    </div>
                    <h3>Idiomas</h3>
                    <div class="form-group">
                        <label>Idioma 1</label>
                        <input type="text" name="idioma1" class="form-control" value="{{$hoja->idioma1}}" placeholder="Idioma 1" readonly>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <div class="form-group">
                        <label>Idioma 2</label>
                        <input type="text" name="idioma2" class="form-control" value="{{$hoja->idioma2}}" placeholder="Idioma 2" readonly>
                    </div>
                    <div class="form-group">
                        <label>Idioma 3</label>
                        <input type="text" name="idioma3" class="form-control" value="{{$hoja->idioma3}}" placeholder="Idioma 3" readonly>
                    </div>
                    <h3>Experiencia Laboral</h3>
                    <h4>Experiencia Laboral 1</h4>
                    <div class="form-group">
                        <label>Institucion</label>
                        <input type="text" name="elaboral1" class="form-control" value="{{$hoja->elaboral1}}" placeholder="Institucion" readonly>
                    </div>
                    <div class="form-group">
                        <label>Cargo</label>
                        <input type="text" name="celaboral1" class="form-control" value="{{$hoja->celaboral1}}" placeholder="Cargo" readonly>
                    </div>
                    <div class="form-group">
                        <label>Año de Salida</label>
                        <input type="text" name="aelaboral1" class="form-control" value="{{$hoja->aelaboral1}}" placeholder="Año de Salida" readonly>
                    </div>
                    <div class="form-group">
                        <label>Numero de Referencia</label>
                        <input type="text" name="relaboral1" class="form-control" value="{{$hoja->relaboral1}}" placeholder="Numero de Referencia" readonly>
                    </div>
                    <h4>Experiencia Laboral 2</h4>
                    <div class="form-group">
                        <label>Institucion</label>
                        <input type="text" name="elaboral2" class="form-control" value="{{$hoja->elaboral2}}" placeholder="Institucion" readonly>
                    </div>
                    <div class="form-group">
                        <label>Cargo</label>
                        <input type="text" name="celaboral2" class="form-control" value="{{$hoja->celaboral2}}" placeholder="Cargo" readonly>
                    </div>
                    <div class="form-group">
                        <label>Año de Salida</label>
                        <input type="text" name="aelaboral2" class="form-control" value="{{$hoja->aelaboral2}}" placeholder="Año de Salida" readonly>
                    </div>
                    <div class="form-group">
                        <label>Numero de Referencia</label>
                        <input type="text" name="relaboral2" class="form-control" value="{{$hoja->relaboral2}}" placeholder="Numero de Referencia" readonly>
                    </div>
                    <h4>Experiencia Laboral 3</h4>
                    <div class="form-group">
                        <label>Institucion</label>
                        <input type="text" name="elaboral3" class="form-control" value="{{$hoja->elaboral3}}" placeholder="Institucion" readonly>
                    </div>
                    <div class="form-group">
                        <label>Cargo</label>
                        <input type="text" name="celaboral3" class="form-control" value="{{$hoja->celaboral3}}" placeholder="Cargo" readonly>
                    </div>
                    <div class="form-group">
                        <label>Año de Salida</label>
                        <input type="text" name="aelaboral3" class="form-control" value="{{$hoja->aelaboral3}}" placeholder="Año de Salida" readonly>
                    </div>
                    <div class="form-group">
                        <label>Numero de Referencia</label>
                        <input type="text" name="relaboral3" class="form-control" value="{{$hoja->relaboral3}}" placeholder="Numero de Referencia" readonly>
                    </div>
                    <h3>Cursos o Conferencias</h3>
                    <h4>Curso 1</h4>
                    <div class="form-group">
                        <label>Institucion</label>
                        <input type="text" name="curso1" class="form-control" value="{{$hoja->curso1}}" placeholder="Institucion" readonly>
                    </div>
                    <div class="form-group">
                        <label>Titulo Obtenido</label>
                        <input type="text" name="tcurso1" class="form-control" value="{{$hoja->tcurso1}}" placeholder="Titulo de Obtenido" readonly>
                    </div>
                    <div class="form-group">
                        <label>Año de Salida</label>
                        <input type="text" name="acurso1" class="form-control" value="{{$hoja->acurso1}}" placeholder="Año de Culminacion" readonly>
                    </div>
                    <h4>Curso 2</h4>
                    <div class="form-group">
                        <label>Institucion</label>
                        <input type="text" name="curso2" class="form-control" value="{{$hoja->curso2}}" placeholder="Institucion" readonly>
                    </div>
                    <div class="form-group">
                        <label>Titulo Obtenido</label>
                        <input type="text" name="tcurso2" class="form-control" value="{{$hoja->tcurso2}}" placeholder="Titulo de Obtenido" readonly>
                    </div>
                    <div class="form-group">
                        <label>Año de Salida</label>
                        <input type="text" name="acurso2" class="form-control" value="{{$hoja->acurso2}}" placeholder="Año de Culminacion" readonly>
                    </div>
                    <h4>Curso 3</h4>
                    <div class="form-group">
                        <label>Institucion</label>
                        <input type="text" name="curso3" class="form-control" value="{{$hoja->curso3}}" placeholder="Institucion" readonly>
                    </div>
                    <div class="form-group">
                        <label>Titulo Obtenido</label>
                        <input type="text" name="tcurso3" class="form-control" value="{{$hoja->tcurso3}}" placeholder="Titulo de Obtenido" readonly>
                    </div>
                    <div class="form-group">
                        <label>Año de Salida</label>
                        <input type="text" name="acurso3" class="form-control" value="{{$hoja->acurso3}}" placeholder="Año de Culminacion" readonly>
                    </div>
                </div>

            {!!Form::close()!!}

        </div>
    </div>
@stop