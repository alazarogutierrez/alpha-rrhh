@extends ('layouts.admin')
@section('titulo_content')
    <div class="row">
        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
            <h2>Registrar Hoja de Vida</h2>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
            <h2><a href="{{URL::action('HojaController@index')}}"><button class="btn btn-success" type="button">Volver</button></a></h2>
        </div>
    </div>
@endsection
@section ('contenido')
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            @if (count($errors)>0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
                {!!Form::model($empleado,['method'=>'PATCH','route'=>['hoja.guardar',$empleado->id_empleado]])!!}
                {{Form::token()}}
            <form class="was-validated">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                <div class="form-group">
                    <h3>Nivel de Instruccion</h3>
                    <select name="nivel" class="form-control">
                        <option value="Bachiller">Bachiller</option>
                        <option value="Pregrado" selected>Pregrado</option>
                        <option value="Postgrado">Postgrado</option>
                        <option value="Otro">Otro</option>
                    </select>
                </div>
                    <h3>Colegiatura</h3>
                    <h4>Nivel Primaria</h4>
                <div class="form-group">
                    <label>Institucion Educativa</label>
                    <input type="text" name="primaria" class="form-control" placeholder="Institucion Educativa">
                </div>
                    <div class="form-group">
                        <label>Año de Culminacion</label>
                        <input type="text" name="añoprimaria" class="form-control" placeholder="Año de Culminacion">
                    </div>
                    <h4>Nivel Secundaria</h4>
                    <div class="form-group">
                        <label>Institucion Educativa</label>
                        <input type="text" name="secundaria" class="form-control" placeholder="Institucion Educativa">
                    </div>
                    <div class="form-group">
                        <label>Año de Culminacion</label>
                        <input type="text" name="añosecundaria" class="form-control" placeholder="Año de Culminacion">
                    </div>
                    <h3>Pregrado</h3>
                    <h4>Pregrado 1</h4>
                    <div class="form-group">
                        <label>Universidad</label>
                        <input type="text" name="pregrado1" class="form-control" placeholder="Universidad">
                    </div>
                    <div class="form-group">
                        <label>Titulo Obtenido</label>
                        <input type="text" name="tpregrado1" class="form-control" placeholder="Titulo Obtenido">
                    </div>
                    <div class="form-group">
                        <label>Año de Culminacion</label>
                        <input type="text" name="apregrado1" class="form-control" placeholder="Año de Culminacion">
                    </div>
                    <h4>Pregrado 2</h4>
                    <div class="form-group">
                        <label>Universidad</label>
                        <input type="text" name="pregrado2" class="form-control" placeholder="Universidad">
                    </div>
                    <div class="form-group">
                        <label>Titulo Obtenido</label>
                        <input type="text" name="tpregrado2" class="form-control" placeholder="Titulo Obtenido">
                    </div>
                    <div class="form-group">
                        <label>Año de Culminacion</label>
                        <input type="text" name="apregrado2" class="form-control" placeholder="Año de Culminacion">
                    </div>
                    <h3>Postgrado</h3>
                    <h4>Postgrado 1</h4>
                    <div class="form-group">
                        <label>Universidad</label>
                        <input type="text" name="postgrado1" class="form-control" placeholder="Universidad">
                    </div>
                    <div class="form-group">
                        <label>Titulo Obtenido</label>
                        <input type="text" name="tpostgrado1" class="form-control" placeholder="Titulo Obtenido">
                    </div>
                    <div class="form-group">
                        <label>Año de Culminacion</label>
                        <input type="text" name="apostgrado1" class="form-control" placeholder="Año de Culminacion">
                    </div>
                    <h4>Postgrado 2</h4>
                    <div class="form-group">
                        <label>Universidad</label>
                        <input type="text" name="postgrado2" class="form-control" placeholder="Universidad">
                    </div>
                    <div class="form-group">
                        <label>Titulo Obtenido</label>
                        <input type="text" name="tpostgrado2" class="form-control" placeholder="Titulo Obtenido">
                    </div>
                    <div class="form-group">
                        <label>Año de Culminacion</label>
                        <input type="text" name="apostgrado2" class="form-control" placeholder="Año de Culminacion">
                    </div>
                    <h4>Postgrado 3</h4>
                    <div class="form-group">
                        <label>Universidad</label>
                        <input type="text" name="postgrado3" class="form-control" placeholder="Universidad">
                    </div>
                    <div class="form-group">
                        <label>Titulo Obtenido</label>
                        <input type="text" name="tpostgrado3" class="form-control" placeholder="Titulo Obtenido">
                    </div>
                    <div class="form-group">
                        <label>Año de Culminacion</label>
                        <input type="text" name="apostgrado3" class="form-control" placeholder="Año de Culminacion">
                    </div>
                    <h3>Idiomas</h3>
                    <div class="form-group">
                        <label>Idioma 1</label>
                        <input type="text" name="idioma1" class="form-control" placeholder="Idioma 1">
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <div class="form-group">
                        <label>Idioma 2</label>
                        <input type="text" name="idioma2" class="form-control" placeholder="Idioma 2">
                    </div>
                    <div class="form-group">
                        <label>Idioma 3</label>
                        <input type="text" name="idioma3" class="form-control" placeholder="Idioma 3">
                    </div>
                    <h3>Experiencia Laboral</h3>
                    <h4>Experiencia Laboral 1</h4>
                    <div class="form-group">
                        <label>Institucion</label>
                        <input type="text" name="elaboral1" class="form-control" placeholder="Institucion">
                    </div>
                    <div class="form-group">
                        <label>Cargo</label>
                        <input type="text" name="celaboral1" class="form-control" placeholder="Cargo">
                    </div>
                    <div class="form-group">
                        <label>Año de Salida</label>
                        <input type="text" name="aelaboral1" class="form-control" placeholder="Año de Salida">
                    </div>
                    <div class="form-group">
                        <label>Numero de Referencia</label>
                        <input type="text" name="relaboral1" class="form-control" placeholder="Numero de Referencia">
                    </div>
                    <h4>Experiencia Laboral 2</h4>
                    <div class="form-group">
                        <label>Institucion</label>
                        <input type="text" name="elaboral2" class="form-control" placeholder="Institucion">
                    </div>
                    <div class="form-group">
                        <label>Cargo</label>
                        <input type="text" name="celaboral2" class="form-control" placeholder="Cargo">
                    </div>
                    <div class="form-group">
                        <label>Año de Salida</label>
                        <input type="text" name="aelaboral2" class="form-control" placeholder="Año de Salida">
                    </div>
                    <div class="form-group">
                        <label>Numero de Referencia</label>
                        <input type="text" name="relaboral2" class="form-control" placeholder="Numero de Referencia">
                    </div>
                    <h4>Experiencia Laboral 3</h4>
                    <div class="form-group">
                        <label>Institucion</label>
                        <input type="text" name="elaboral3" class="form-control" placeholder="Institucion">
                    </div>
                    <div class="form-group">
                        <label>Cargo</label>
                        <input type="text" name="celaboral3" class="form-control" placeholder="Cargo">
                    </div>
                    <div class="form-group">
                        <label>Año de Salida</label>
                        <input type="text" name="aelaboral3" class="form-control" placeholder="Año de Salida">
                    </div>
                    <div class="form-group">
                        <label>Numero de Referencia</label>
                        <input type="text" name="relaboral3" class="form-control" placeholder="Numero de Referencia">
                    </div>
                    <h3>Cursos o Conferencias</h3>
                    <h4>Curso 1</h4>
                    <div class="form-group">
                        <label>Institucion</label>
                        <input type="text" name="curso1" class="form-control" placeholder="Institucion">
                    </div>
                    <div class="form-group">
                        <label>Titulo Obtenido</label>
                        <input type="text" name="tcurso1" class="form-control" placeholder="Titulo de Obtenido">
                    </div>
                    <div class="form-group">
                        <label>Año de Salida</label>
                        <input type="text" name="acurso1" class="form-control" placeholder="Año de Culminacion">
                    </div>
                    <h4>Curso 2</h4>
                    <div class="form-group">
                        <label>Institucion</label>
                        <input type="text" name="curso2" class="form-control" placeholder="Institucion">
                    </div>
                    <div class="form-group">
                        <label>Titulo Obtenido</label>
                        <input type="text" name="tcurso2" class="form-control" placeholder="Titulo de Obtenido">
                    </div>
                    <div class="form-group">
                        <label>Año de Salida</label>
                        <input type="text" name="acurso2" class="form-control" placeholder="Año de Culminacion">
                    </div>
                    <h4>Curso 3</h4>
                    <div class="form-group">
                        <label>Institucion</label>
                        <input type="text" name="curso3" class="form-control" placeholder="Institucion">
                    </div>
                    <div class="form-group">
                        <label>Titulo Obtenido</label>
                        <input type="text" name="tcurso3" class="form-control" placeholder="Titulo de Obtenido">
                    </div>
                    <div class="form-group">
                        <label>Año de Salida</label>
                        <input type="text" name="acurso3" class="form-control" placeholder="Año de Culminacion">
                    </div>
                <div class="form-group">
                    <button class="btn btn-primary" type="submit">Guardar</button>
                    <button class="btn btn-danger" type="reset">Cancelar</button>
                </div>
                </div>
            </form>
            {!!Form::close()!!}

        </div>
    </div>
@stop