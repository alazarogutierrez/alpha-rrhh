@extends ('layouts.admin')
@section('titulo_content')
    <h2>Lista de Memorandums</h2>
@endsection
@section ('contenido')
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            @include('memo.search2')
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 table-responsive">
            <table class="table table-striped table-bordered table-condensed table-hover">
                <thead>
                <th>Id Mem</th>
                <th>Id Empleado</th>
                <th>Motivo</th>
                <th>Fecha</th>
                <th>Descripcion</th>
                <th>Sancion</th>
                </thead>
                @foreach ($memos as $per)
                    <tr>
                        <td>{{$per->id_mem}}</td>
                        <td>{{$per->id_empleado}}</td>
                        <td>{{$per->motivo}}</td>
                        <td>{{$per->fecha}}</td>
                        <td>{{$per->descripcion}}</td>
                        <td>{{$per->sancion}}</td>
                        <td><a href="{{URL::action('MemoController@show',$per->id_mem)}}"><button class="btn btn-primary" title="Ver Memorandum"><i class="fa fa-eye"></i></button></a></td>
                        <td><a href="{{URL::action('MemoController@edit',$per->id_mem)}}"><button class="btn btn-success" title="Editar Memorandum"><i class="fa fa-edit"></i></button></a></td>
                        <td><a href="" data-target="#modal-delete-{{$per->id_mem}}" data-toggle="modal"><button class="btn btn-danger" title="Eliminar Memorandum"><i class="fa fa-trash"></i></button></a></td>
                    </tr>
                    @include('memo.modal')
                @endforeach
            </table>
            {{$memos->render()}}
        </div>
    </div>
@stop