@extends ('layouts.admin')
@section('titulo_content')
    <div class="row">
        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
            <h2>Ver Memorandum</h2>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
            <h2><a href="{{URL::action('MemoController@listam')}}"><button class="btn btn-success" type="button">Volver</button></a></h2>
        </div>
    </div>
@endsection
@section ('contenido')
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            @if (count($errors)>0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            {!!Form::model($memo)!!}
            {{Form::token()}}
            <div class="form-group">
                <label for="nombre">Motivo</label>
                <select name="motivo" class="form-control" readonly>
                    @if($memo->motivo=='Atrasos')
                        <option value="Atrasos" selected>Atrasos</option>
                        <option value="Inasistencia">Inasistencia</option>
                        <option value="Indisciplina">Indisciplina</option>
                        <option value="Otro">Otro</option>
                    @elseif($memo->motivo=='Inasistencia')
                        <option value="Atrasos">Atrasos</option>
                        <option value="Inasistencia" selected>Inasistencia</option>
                        <option value="Indisciplina">Indisciplina</option>
                        <option value="Otro">Otro</option>
                    @elseif($memo->motivo=='Indisciplina')
                        <option value="Atrasos">Atrasos</option>
                        <option value="Inasistencia">Inasistencia</option>
                        <option value="Indisciplina" selected>Indisciplina</option>
                        <option value="Otro">Otro</option>
                    @elseif($memo->motivo=='Otro')
                        <option value="Atrasos">Atrasos</option>
                        <option value="Inasistencia">Inasistencia</option>
                        <option value="Indisciplina">Indisciplina</option>
                        <option value="Otro" selected>Otro</option>
                    @endif
                </select>
            </div>
            <div class="form-group">
                <label for="nombre">Fecha</label>
                <input type="date" name="fecha" class="form-control" value="{{$memo->fecha}}" placeholder="Fecha" readonly>
            </div>
            <div class="form-group">
                <label for="nombre">Descripcion</label>
                <textarea class="form-control" name="descripcion" value="{{$memo->descripcion}}" rows="4" readonly></textarea>
            </div>
            <div class="form-group">
                <label for="nombre">Sancion</label>
                <input type="text" name="sancion" class="form-control" value="{{$memo->sancion}}" placeholder="sancion" readonly>
            </div>
            <div class="form-group">
                <label for="nombre">Descuento</label>
                <input type="number" name="descuento" class="form-control" value="{{$memo->descuento}}" placeholder="descuento" readonly>
            </div>
            {!!Form::close()!!}

        </div>
    </div>
@stop