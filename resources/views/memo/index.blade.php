@extends ('layouts.admin')
@section('titulo_content')
    <h2>Registrar Memorandum</h2>
@endsection
@section ('contenido')
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            @include('memo.search')
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 table-responsive">
            <table class="table table-striped table-bordered table-condensed table-hover">
                <thead>
                <th>Id</th>
                <th colspan="2" style="text-align:center">Nombres </th>
                <th colspan="2" style="text-align:center">Apellidos</th>
                <th colspan="3" style="text-align:center">Documento</th>
                <th>Estado de Memorandum</th>
                </thead>
                @foreach ($empleados as $per)
                    <tr>
                        <td>{{$per->id_empleado}}</td>
                        <td>{{$per->primer_nombre}}</td>
                        <td>{{$per->segundo_nombre}}</td>
                        <td>{{$per->primer_apellido}}</td>
                        <td>{{$per->segundo_apellido}}</td>
                        <td>{{$per->cat_tipo_documento}}</td>
                        <td>{{$per->documento}}</td>
                        <td>{{$per->complemento1}}</td>
                        @switch($per->memo)
                            @case(0)
                            <td class="default">
                            {{'Sin Memorandum'}}
                            @break
                            @case(1)
                            <td class="success">
                            {{'1er Memorandum'}}
                            @break
                            @case(2)
                            <td class="warning">
                            {{'2do Memorandum'}}
                            @break
                            @case(3)
                            <td class="danger">
                                {{'3er Memorandum'}}
                                @break
                                @endswitch
                            </td>
                        @if($per->memo==3)
                            <td><a href="" data-target="#modal-delete-{{$per->id_empleado}}" data-toggle="modal"><button class="btn btn-danger" title="Dar de Baja al Empleado"><i class="fa fa-trash"></i></button></a></td>

                        @else
                            <td><a href="{{URL::action('MemoController@crear',$per->id_empleado)}}"><button class="btn btn-warning" title="Registrar Memorandum"><i class="fa fa-plus"></i></button></a></td>

                            @endif
                            <td><a href="{{route('enviar2',['id'=>$per->id_empleado,'memo'=>$per->memo])}}"><button class="btn btn-primary" title="Enviar Notificacion de Memorandums"><i class="fa fa-envelope"></i></button></a></td>
                    </tr>

                    @include('empleado.modal')
                @endforeach
            </table>
            {{$empleados->render()}}
        </div>
    </div>
@stop