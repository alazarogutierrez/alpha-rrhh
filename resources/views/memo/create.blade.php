@extends ('layouts.admin')
@section('titulo_content')
    <div class="row">
        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
            <h2>Añadir Memorandum</h2>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
            <h2><a href="{{URL::action('MemoController@index')}}"><button class="btn btn-success" type="button">Volver</button></a></h2>
        </div>
    </div>
@endsection
@section ('contenido')
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            @if (count($errors)>0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            {!!Form::model($empleado,['method'=>'PATCH','route'=>['memo.guardar',$empleado->id_empleado]])!!}
            {{Form::token()}}
            <form class="was-validated">
                <div class="form-group">
                    <label for="nombre">Motivo</label>
                    <select name="motivo" class="form-control">
                        <option value="Atrasos"selected>Atrasos</option>
                        <option value="Inasistencia">Inasistencia</option>
                        <option value="Indisciplina">Indisciplina</option>
                        <option value="Otro">Otro</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="nombre">Fecha</label>
                    <input type="date" name="fecha" class="form-control" placeholder="Fecha">
                </div>
                <div class="form-group">
                    <label for="nombre">Descripcion</label>
                    <textarea class="form-control" name="descripcion" rows="4"></textarea>
                </div>
                <div class="form-group">
                    <label for="nombre">Sancion</label>
                    <input type="text" name="sancion" class="form-control" placeholder="sancion">
                </div>
                <div class="form-group">
                    <label for="nombre">Descuento</label>
                    <input type="number" name="descuento" class="form-control" placeholder="descuento">
                </div>

                <div class="form-group">
                    <button class="btn btn-primary" type="submit">Guardar</button>
                    <button class="btn btn-danger" type="reset">Cancelar</button>
                </div>
            </form>
            {!!Form::close()!!}

        </div>
    </div>
@stop