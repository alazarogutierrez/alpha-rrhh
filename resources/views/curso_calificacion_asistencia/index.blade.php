@extends ('layouts.admin')
@section('titulo_content')
    <div class="row">
        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
            <h3>{{$curso->tema}}</h3>
        </div>
    </div>
@endsection
@section ('contenido')


    <div class="row">
        <div class="col-lg-10 offset-lg-6">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-condensed table-hover">
                    <thead>
                    <th>CI</th>
                    <th>Nombre</th>
                    <th>Nota Final /100</th>
                    <th>Asistencia </th>
                    </thead>
                    @foreach ($alumnos as $per)
                       <tr>
                            {{Form::Open(array('action'=>array('CalificacionEmpleadoController@update',$per->id_empleado_capacitacion),'method'=>'put'))}}
                                <td>{{$per->documento}}</td>
                                <td>{{strtoupper($per->primer_apellido)}} {{strtoupper($per->primer_nombre)}}</td>
                                <td>
                                    <div class='input-group date' id='datetimepicker1'>
                                        <input type="number" class="form-control" name="nota_final" value="{{$per->nota_final}}" min="0" max="100">
                                        <span class="input-group-addon">
                                                {{'/100'}}
                                        </span>
                                    </div>
                                </td>
                                <td>
                                    <div class='input-group date' id='datetimepicker1'>
                                        <input type="number" class="form-control" name="asistencia" value="{{$per->asistencia}}" min="0" max="{{$per->sesiones}}">
                                        <span class="input-group-addon">
                                                {{'/'.$per->sesiones}}
                                        </span>
                                    </div>
                                </td>
                                <td><button type="submit" class="btn btn-primary">Guardar</button></td>
                            {{Form::Close()}}
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
@stop