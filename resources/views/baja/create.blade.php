@extends ('layouts.admin')
@section('titulo_content')
    <div class="row">
        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
            <h2>Registrar Motivo de Baja</h2>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
            <h2><a href="{{URL::action('EmpinactivoController@index')}}"><button class="btn btn-success" type="button">Volver</button></a></h2>
        </div>
    </div>
@endsection
@section ('contenido')
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            @if (count($errors)>0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            {!!Form::model($empleado,['method'=>'PATCH','route'=>['baja.guardar',$empleado->id_empleado]])!!}
            {{Form::token()}}
            <form class="was-validated">
                <div class="form-group">
                    <label for="nombre">Fecha*</label>
                    <input type="date" name="fecha" class="form-control" placeholder="Fecha">
                </div>
                <div class="form-group">
                    <label>Motivo*</label>
                    <select name="motivo" class="form-control">
                        <option value="1"selected>Despido</option>
                        <option value="2">Renuncia</option>
                        <option value="3">Traslado</option>
                        <option value="4">Jubilacion</option>
                        <option value="5">Fallecimiento</option>
                        <option value="6">Sin Especificar</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="nombre">Monto de Compesación (Si aplica)</label>
                    <input type="number" name="monto" class="form-control" min="0" value="0">
                </div>

                <div class="form-group">
                    <button class="btn btn-primary" type="submit">Guardar</button>
                    <button class="btn btn-danger" type="reset">Cancelar</button>
                </div>
            </form>
            {!!Form::close()!!}

        </div>
    </div>
@stop