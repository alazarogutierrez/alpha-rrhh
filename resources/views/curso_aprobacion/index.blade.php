@extends ('layouts.admin')
@section('titulo_content')
    Solicitudes pendientes
@endsection
@section ('contenido')
    <div class="row">
        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
            @include('curso_aprobacion.search')
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 offset-lg-12 ">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-condensed table-hover">
                    <thead>
                    <th>Tema</th>
                    <th>Estudiantes</th>
                    <th>Presupuesto / estudiante (Bs)</th>
                    <th>Total(Bs)</th>
                    <th>Tiempo en Espera</th>
                    </thead>
                    @foreach ($cursos as $per)

                            <tr>
                                <td>{{$per->tema}}</td>
                                <td>{{$per->estudiantes}}</td>
                                <td>{{$per->presupuesto_solicitado}}</td>
                                <td>{{$per->presupuesto_solicitado*$per->estudiantes}}</td>
                                <td>{{\Carbon\Carbon::createFromTimeStamp(strtotime($per->fch_ini))->diffForHumans()}}</td>
                                <td>
                                        <a href="{{URL::action('CursoPropuestasController@ver_curso',$per->id_capacitacion)}}"><button class="btn btn-info" >Propuestas</button></a>
                                        <a href="" data-target="#modal-delete-{{$per->id_capacitacion}}" data-toggle="modal"><button class="btn btn-danger">Eliminar</button></a>
                                </td>
                            </tr>
                            @include('curso_aprobacion.modal')
                    @endforeach
                </table>
            </div>
            {{$cursos->render()}}
        </div>
    </div>
@stop