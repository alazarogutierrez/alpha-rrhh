@extends ('layouts.admin')
@section('titulo_content')
    Informacion Curso
@endsection
@section ('contenido')
    <div class="row">
        <div class="col-lg-12 offset-lg-12">
            @if (count($errors)>0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            {!!Form::model($curso,['method'=>'PATCH','route'=>['curso_aprobacion.update',$curso->id_capacitacion]])!!}
            {{Form::token()}}
            <div class="col-xs-offset-4">
                <div class="row">
                    <div class="col-sm-7">
                        <div class="form-group">
                            <label for="nombre">Titulo</label>
                            <input type="text" name="tema" class="form-control" value="{{$curso->tema}}" readonly>
                        </div>
                        <div class="form-group">
                            <label for="nombre">Estudiantes</label>
                            <input type="number" name="capacidad" class="form-control" min="0" value="{{$curso->estudiantes}}" readonly>
                        </div>
                        <div class="form-group">
                            <label for="nombre">Presupuesto</label>
                            <input type="number" name="presupuesto" class="form-control" min="0" value="0">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-7">
                        <div class="form-group">
                            <label for="nombre">Observaciones</label>
                            <textarea class="form-control" name="descripcion" rows="4">
                             </textarea>
                        </div>
                    </div>


                </div>
                <div class="row">
                    <div class="col-sm-7">
                        <div class="form-group">
                            <button class="btn btn-success float-right" name="aprobacion" type="submit" value="1">Aceptar</button>
                            <button class="btn btn-danger float-right" name="aprobacion" type="submit" value="2">Rechazar</button>
                        </div>
                    </div>
                </div>
            </div>
            {!!Form::close()!!}

        </div>
    </div>
@endsection