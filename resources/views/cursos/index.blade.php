@extends ('layouts.admin')
@section ('contenido')
    <div class="row">
        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
            <h3>Capacitacion<a href="{{route('curso.create')}}"><button class="btn btn-success">Nuevo</button></a></h3>
            @include('cursos.search')
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 offset-lg-12 ">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-condensed table-hover">
                    <thead>
                    <th>Tema</th>
                    <th>Estudiantes</th>
                    <th>Tiempo en Espera</th>
                    <th>Estado RRHH</th>
                    <th>Estado Finanzas</th>
                    </thead>
                    @foreach ($cursos as $per)
                        @if($per->aprobacion==1 && $per->segunda_aprobacion==1)
                        <tr class="success">
                        @elseif($per->aprobacion==2 && $per->segunda_aprobacion==2)
                        <tr class="danger">
                        @else
                        <tr>
                        @endif
                            <td>{{$per->tema}}</td>
                            <td>{{$per->estudiantes}}</td>
                            <td>{{\Carbon\Carbon::createFromTimeStamp(strtotime($per->fch_ini))->diffForHumans()}}</td>

                                @switch($per->aprobacion)
                                    @case(0)
                                        <td class="warning">
                                        {{'En espera'}}
                                        @break
                                    @case(1)
                                        <td class="success">
                                        {{'Aprobado'}}
                                        @break
                                    @case(2)
                                        <td class="danger">
                                        {{'Rechazado'}}
                                        @break
                                @endswitch
                            </td>
                                @switch($per->segunda_aprobacion)
                                    @case(0)
                                    <td class="warning">
                                    {{'En espera'}}
                                    @break
                                    @case(1)
                                    <td class="success">
                                    {{'Aprobado'}}
                                    @break
                                    @case(2)
                                    <td class="danger">
                                        {{'Rechazado'}}
                                        @break
                                        @endswitch
                                    </td>
                            <td>
                                @if($per->aprobacion ==0)
                                    <a href="{{URL::action('CursoController@edit',$per->id_capacitacion)}}"><button class="btn btn-info" >Editar</button></a>
                                    <a href="" data-target="#modal-delete-{{$per->id_capacitacion}}" data-toggle="modal"><button class="btn btn-danger">Eliminar</button></a>
                                @else
                                    <a href="" data-target="#modal-observaciones-{{$per->id_capacitacion}}" data-toggle="modal"><button class="btn btn-info">Observaciones</button></a>
                                    @if($per->segunda_aprobacion == 1)
                                        <a href="{{asset ('propuestas/'.$per->descripcion)}}"><button class="btn btn-default" title="Descargar Hoja de Vida"><i class="fa fa-download"></i></button></a>
                                    @endif
                                @endif
                            </td>
                        </tr>
                        @include('cursos.modal')
                            @include('cursos.modal2')
                    @endforeach
                </table>
            </div>
            {{$cursos->render()}}
        </div>
    </div>
@stop