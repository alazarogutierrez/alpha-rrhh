@extends ('layouts.admin')
@section('titulo_content')
    Editar curso
@endsection
@section ('contenido')
<div class="row">
    <div class="col-lg-12 offset-lg-12">
        @if (count($errors)>0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        {!!Form::model($curso,['method'=>'PATCH','route'=>['curso.update',$curso->id_capacitacion]])!!}
        {{Form::token()}}
        <div class="col-xs-offset-4">
            <div class="row">
                <div class="col-sm-7">
                    <div class="form-group">
                        <label for="nombre">Titulo</label>
                        <input type="text" name="tema" class="form-control" value="{{$curso->tema}}">
                    </div>
                    <div class="form-group">
                        <label for="nombre">Estudiantes</label>
                        <input type="number" name="capacidad" class="form-control" min="1" value="{{$curso->estudiantes}}">
                    </div>
                    <div class="form-group">
                        <label for="nombre">Presupuesto Solicitado</label>
                        <input type="number" name="pre_solicitado" class="form-control" min="1" value="{{$curso->presupuesto_solicitado}}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-7">
                    <div class="form-group">
                        <label for="nombre">Temario</label>
                        <textarea class="form-control" name="descripcion" rows="4">
                            {{$curso->descripcion}}
                        </textarea>
                    </div>
                </div>


            </div>
            <div class="row">
                <div class="col-sm-7">
                    <div class="form-group">
                        <button class="btn btn-primary float-right" type="submit">Guardar</button>
                    </div>
                </div>
            </div>
        </div>
        {!!Form::close()!!}

    </div>
</div>
@endsection