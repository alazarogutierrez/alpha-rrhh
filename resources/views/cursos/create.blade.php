@extends ('layouts.admin')
@section('titulo_content')
    Solicitar capacitacion
@endsection
@section ('contenido')

                    {!!Form::open(array('url'=>'curso','method'=>'POST','autocomplete'=>'off'))!!}
                    {{Form::token()}}
                    <div class="col-xs-offset-4">
                        <div class="row">
                            <div class="col-sm-7">
                                <div class="form-group">
                                    <label for="nombre">Tema</label>
                                    <input type="text" name="tema" class="form-control" placeholder="Titulo">
                                </div>
                                <div class="form-group">
                                    <label for="nombre">Personal Capacitado</label>
                                    <input type="number" name="capacidad" class="form-control" min="1" value="1">
                                </div>
                                <div class="form-group">
                                    <label for="nombre">Presupuesto Solicitado(por persona)</label>
                                    <input type="number" name="pre_solicitado" class="form-control" min="1" value="1">
                                </div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-7">
                                <div class="form-group">
                                    <label for="nombre">Temario</label>
                                    <textarea class="form-control" name="descripcion" rows="4"></textarea>
                                </div>
                            </div>


                        </div>
                        <div class="row">
                            <div class="col-sm-7">
                                <div class="form-group">
                                    <button class="btn btn-primary float-right" type="submit">Guardar</button>
                                    <button class="btn btn-danger float-right" type="reset">Cancelar</button>
                                </div>
                            </div>
                        </div>
                    </div>


                    {!!Form::close()!!}


@stop