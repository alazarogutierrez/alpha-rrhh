@extends ('layouts.admin')
@section ('contenido')
    <div class="row">
        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
            <h3>Cursos Aprobados</h3>
            @include('cursos.search_aprobado')
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 offset-lg-12 ">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-condensed table-hover">
                    <thead>
                    <th>Tema</th>
                    <th>Estudiantes</th>
                    <th>Temario</th>
                    <th>Hoja de vida</th>
                    <th>Inversion</th>
                    </thead>
                    @foreach ($cursos as $per)
                        @if($per->aprobacion==1 && $per->segunda_aprobacion==1)
                            <tr class="success">
                        @elseif($per->aprobacion==2 && $per->segunda_aprobacion==2)
                            <tr class="danger">
                        @else
                            <tr>
                                @endif
                                <td>{{$per->tema}}</td>
                                <td>{{$per->estudiantes}}</td>
                                <td>
                                    <a href="{{asset ('propuestas/'.$per->descripcion)}}"><button class="btn btn-default" title="Descargar Hoja de Vida"><i class="fa fa-download"></i></button></a>
                                </td>
                                <td>
                                    <a href="{{asset ('cv_docentes/'.$per->descripcion_cv)}}"><button class="btn btn-default" title="Descargar Hoja de Vida"><i class="fa fa-download"></i></button></a>
                                </td>
                                <td>
                                   {{$per->costo*$per->estudiantes}}
                                </td>
                                <td>
                                    @if($per->estado_curso == 0)
                                        <a href="{{URL::action('InscripcionEmpleadoController@show',$per->id_capacitacion)}}"><button class="btn btn-default">Inscribir</button></a>
                                    @else
                                        <a href="{{URL::action('CalificacionEmpleadoController@show',$per->id_capacitacion)}}"><button class="btn btn-default">Calificaciones y Asistencia</button></a>
                                        <a href="{{URL::action('ReporteCursoController@show',$per->id_capacitacion)}}"><button class="btn btn-default">Reporte</button></a>
                                    @endif
                                </td>
                            </tr>
                            @include('cursos.modal')
                            @include('cursos.modal2')
                            @endforeach
                </table>
            </div>
            {{$cursos->render()}}
        </div>
    </div>
@stop