@extends('layouts.admin')
@section('titulo_content')
    <h2>Hoja de Vida</h2>
@endsection
@section('content')
    <table class="table table-hover table-striped">
        <thead>
        <tr>
            <th>Pregrado</th>
            <th>Titulo</th>
            <th>Año</th>
        </tr>
        </thead>
        <tbody>
        @foreach($hojas as $hoja)
            <tr>
                <td>{{$hoja->pregrado1}}</td>
                <td>{{$hoja->tpregrado1}}</td>
                <td>{{$hoja->apregrado1}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{$hojas->render()}}
@endsection