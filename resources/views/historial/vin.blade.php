@extends ('layouts.admin')
@section('titulo_content')
    <h2>Historial Vinculaciones de Empleados</h2>
@endsection
@section ('contenido')
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            @include('historial.search3')
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 table-responsive">
            <table class="table table-striped table-bordered table-condensed table-hover">
                <thead>
                <th>Id</th>
                <th colspan="2" style="text-align:center">Empleado Vinculado</th>
                <th>Usuario Transaccion</th>
                <th>Fecha y Hora Transaccion</th>
                <th>Host Transaccion</th>
                </thead>
                @foreach ($vinculaciones as $per)
                    <tr>
                        <td>{{$per->id_vin}}</td>
                        <td style="text-align:center">{{$per->id_empleado}}</td>
                        <td style="text-align:center">{{$per->documento}}</td>
                        <td>{{$per->usuario_ini}}</td>
                        <td>{{$per->fch_ini}}</td>
                        <td>{{$per->host_ini}}</td>
                    </tr>
                @endforeach
            </table>
            {{$vinculaciones->render()}}
        </div>
    </div>
@stop