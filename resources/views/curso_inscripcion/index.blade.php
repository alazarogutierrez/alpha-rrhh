@extends ('layouts.admin')
@section('titulo_content')
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h3>Inscripcion de empleados</h3>
        </div>
    </div>
@endsection
@section ('contenido')
    <div class="row">
        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
            @include('curso_inscripcion.search')
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

           @if(isset($plazas))
               @if($plazas>0)
                   <h4>Plazas restantes
                       <strong>
                           {{$plazas}}
                       </strong>
                   </h4>
               @else
                       <div class="row">
                           <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                               <a href="" data-target="#modal-delete-cerrar_inscripcion" data-toggle="modal"><button class="btn btn-warning">CERRAR INSCRIPCION</button></a>
                           </div>
                       </div>
                       @include('curso_inscripcion.modal')
               @endif
           @endif
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 offset-lg-12 ">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-condensed table-hover">
                    <thead>
                    <th>Nombre</th>
                    <th>Apellido</th>
                    <th>CI</th>
                    </thead>
                    @foreach ($empleado as $emp)
                        <tr>
                            <td>
                                {{strtoupper($emp->primer_nombre)}}
                            </td>
                            <td>
                                {{strtoupper($emp->primer_apellido)}}
                            </td>
                            <td>
                                {{$emp->documento}}
                            </td>
                            <td>
                                @if($emp->estado_inscripcion==0)
                                    <a href="{{URL::action('InscripcionEmpleadoController@edit',$emp->id_empleado_capacitacion)}}"><button class="btn btn-success" >Inscribir</button></a>
                                @else
                                    <a href="{{URL::action('InscripcionEmpleadoController@edit',$emp->id_empleado_capacitacion)}}"><button class="btn btn-danger" >Borrar</button></a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
@stop