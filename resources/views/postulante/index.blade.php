@extends ('layouts.admin')
@section('titulo_content')
    <h2>Base de Datos Talentos</h2>
@endsection
@section ('contenido')
    <div class="row">
        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
            @include('postulante.search')
        </div>
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
            <a href="postulante/create"><button class="btn btn-success btn-sm">Nuevo Postulante</button></a>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 table-responsive">
            <table class="table table-striped table-bordered table-condensed table-hover">
                <thead>
                <th>Id</th>
                <th colspan="2" style="text-align:center">Nombres </th>
                <th colspan="2" style="text-align:center">Apellidos</th>
                <th colspan="3" style="text-align:center">Documento</th>
                <th>Nacionalidad</th>
                <th>Edad</th>
                <th>Genero</th>
                <th>E. Civil</th>
                <th>Celular</th>
                <th>Telefono</th>
                <th>Direccion</th>

                </thead>
                @foreach ($postulantes as $per)

                    <tr>
                        <td>{{$per->id_postulante}}</td>
                        <td>{{$per->primer_nombre}}</td>
                        <td>{{$per->segundo_nombre}}</td>
                        <td>{{$per->primer_apellido}}</td>
                        <td>{{$per->segundo_apellido}}</td>
                        <td>{{$per->cat_tipo_documento}}</td>
                        <td>{{$per->documento}}</td>
                        <td>{{$per->complemento1}}</td>
                        <td>{{$per->cat_nacionalidad}}</td>
                        <td>{{$per->edad}}</td>
                        <td>{{$per->cat_genero}}</td>
                        <td>{{$per->cat_estado_civil}}</td>
                        <td>{{$per->celular}}</td>
                        <td>{{$per->telefono}}</td>
                        <td>{{$per->direccion}}</td>
                        <td><a href="{{URL::action('PostulanteController@edit',$per->id_postulante)}}"><button class="btn btn-success" title="Editar Postulante"><i class="fa fa-edit"></i></button></a></td>
                        <td><a href="" data-target="#modal-delete-{{$per->id_postulante}}" data-toggle="modal"><button class="btn btn-danger" title="Eliminar Postulante"><i class="fa fa-trash"></i></button></a></td>
                    </tr>
                    @include('postulante.modal')
                @endforeach
            </table>
            {{$postulantes->render()}}
        </div>
    </div>
@stop