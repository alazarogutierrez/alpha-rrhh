
$( document ).ready(function() {

    $('#aceptar').click(function (event) {
            var dia=document.getElementById('dia').value;
            var hora_i=document.getElementById('hora_i').value;
            var hora_f=document.getElementById('hora_f').value;
            var lista=document.getElementById(dia).children;
            validar=(function (hora_i,hora_f,lista) {
                var hi_h=hora_i.split(':')[0];
                var hi_m=hora_i.split(':')[1];
                var hf_h=hora_f.split(':')[0];
                var hf_m=hora_f.split(':')[1];
                var mensaje="";
                function hora(hi_h,hi_m,hf_h,hf_m) {
                    var dif = (hf_h*3600+hf_m*60)-(hi_h*3600+hi_m*60);
                    return dif<3600?true:false;
                }
                function solapamiento(lista,hi_h,hi_m,hf_h,hf_m) {
                    var hora=hi_h*3600+hi_m*60;
                    var hora2=hf_h*3600+hf_m*60;
                    var flag=false;
                    //console.log(hora);
                    for(i=0;i<lista.length;i++){
                        var h_insert = (lista[i].id.split('-'));
                        var inicio=h_insert[0].split(':')[0]*3600+h_insert[0].split(':')[1]*60;
                        var fin=h_insert[1].split(':')[0]*3600+h_insert[1].split(':')[1]*60;

                        //console.log("Hora  "+ inicio+ " :  "+fin);
                        //console.log(inicio+" "+hora+" "+fin);
                        if(hora>=inicio && hora<fin){
                            flag=true;
                            break;
                        }
                        if(hora2>=inicio && hora2<fin){
                            flag=true;
                            break;
                        }
                    }
                    return flag;
                }
                return{
                    verificacion:function () {
                        if(hora(hi_h,hi_m,hf_h,hf_m)){
                            swal("Intente de nuevo","La hora de inicio es mayor que la hora de finalizacion y la duracion de los cursos debe ser mas de una hora","warning");
                            return false;
                        }else if(solapamiento(lista,hi_h,hi_m,hf_h,hf_m)){
                            swal("Intente de nuevo","Hay cruce de horarios","warning");
                            return false;
                        }else{
                            return true;
                        }
                    }
                }
            })(hora_i,hora_f,lista);
            //console.log(lista);
            if(validar.verificacion()){
                $('#'+dia).append(
                    '<div class="item clearfix dias_hora" id="'+hora_i+'-'+hora_f+'"><div class="item__description">'
                    +hora_i+' - '+hora_f+
                    '</div><div class="right clearfix"><div class="item__delete"><button class="item__delete--btn"><i class="fa fa-remove"  style="color:red"></i></button>'
                );
            }
    });
    $('.dias').click(function (e) {
        var padre=e.target.parentElement.parentElement.parentElement.parentElement.parentElement;
        if($(padre).attr('class')=='dias')
        {
           // swal('eliminado');
            padre.removeChild(e.target.parentElement.parentElement.parentElement.parentElement);
        }
    });
    $('form').on('submit',function (e) {
        e.preventDefault();
        //var form_url = $("form[id='formulario']").attr("action");
        var CSRF_TOKEN = $('input[name="_token"]').val();
        var array =  $('#semana').children().toArray();
        var len=$('#semana').children().toArray().length;
        var semana = [];
        //console.log(array);
        for(i = 0 ; i < len; i++ ){
            var s_array = $("#"+array[i].id).children().toArray();
            var len2 = $("#"+array[i].id).children().toArray().length;
            for( j = 0; j < len2; j++){
                semana.push(array[i].id+" "+s_array[j].id);
            }
        }
        //console.log(semana);
        //si es alamacenamiento
        console.log($('#tipo_form').val());
        if($('#tipo_form').val()!="actualizar"){
            var form_data = new FormData();
            form_data.append('institucion', $('input[name="institucion"]').val());
            form_data.append('costo', $('input[name="costo"]').val());
            form_data.append('id', $('input[name="id"]').val());
            form_data.append('fecha_inicio', $('input[name="fecha_inicio"]').val());
            form_data.append('fecha_fin', $('input[name="fecha_fin"]').val());
            form_data.append('sesiones', $('input[name="sesiones"]').val());
            form_data.append('direccion', $('input[name="direccion"]').val());
            form_data.append('descripcion', $('input[name="descripcion"]').prop('files')[0]);
            form_data.append('cv_docentes', $('input[name="cv_docentes"]').prop('files')[0]);
            form_data.append('semana',semana);
            //console.log($('input[name="descripcion"]').prop('files'));
            if(semana.length>0){
                $.ajax({
                    url:"/curso_propuesta",
                    headers:{'X-CSRF-TOKEN':CSRF_TOKEN},
                    type:"POST",
                    processData: false,
                    contentType: false,
                    data:form_data,
                    success:function (data) {
                        console.log(data);
                        window.location.href = "http://localhost:8000/curso_propuestas/"+$('input[name="id"]').val();
                    }

                });
            }else{
                swal("Faltan horarios","Debe llenar la seccion horarios presionando '+' ","warning");
            }
        }else{
            console.log("actualizando ...");
            console.log($('#id').val());
            console.log($('#id_propuesta').val());
            console.log(semana);
            var form_data = new FormData();
            form_data.append('institucion', $('input[name="institucion"]').val());
            form_data.append('costo', $('input[name="costo"]').val());
            form_data.append('id', $('input[name="id"]').val());
            form_data.append('fecha_inicio', $('input[name="fecha_inicio"]').val());
            form_data.append('fecha_fin', $('input[name="fecha_fin"]').val());
            form_data.append('sesiones', $('input[name="sesiones"]').val());
            form_data.append('direccion', $('input[name="direccion"]').val());
            form_data.append('semana',semana);
            form_data.append('id_capacitacion_propuesta',$('input[name="id_propuesta"]').val());
            form_data.append('descripcion', $('input[name="descripcion"]').prop('files')[0]);
            form_data.append('cv_docentes', $('input[name="cv_docentes"]').prop('files')[0]);
            //console.log($('input[name="descripcion"]').prop('files'));
            if(semana.length>0){
                $.ajax({
                    url:"/curso_propuesta/"+$('#id_propuesta').val()+"/"+$('#id').val(),
                    headers:{'X-CSRF-TOKEN':CSRF_TOKEN},
                    type:"POST",
                    processData: false,
                    contentType: false,
                    data:form_data,
                    success:function (data) {
                        console.log(data);
                        window.location.href = "http://localhost:8000/curso_propuestas/"+$('input[name="id"]').val();
                    }

                });
            }else{
                swal("Faltan horarios","Debe llenar la seccion horarios presionando '+' ","warning");
            }
        }



    });

    //si es actualizacion

    /*$('#probar').click(function (e) {
        var array =  $('#semana').children().toArray();
        var len=$('#semana').children().toArray().length;
        var semana = [];
        console.log(array);
        for(i = 0 ; i < len; i++ ){
            semana[i]= [];
            var s_array = $("#"+array[i].id).children().toArray();
            var len2 = $("#"+array[i].id).children().toArray().length;
            for( j = 0; j < len2; j++){
                semana[i].push(array[i].id+" "+s_array[j].id);
            }
        }
        console.log(semana);
    });*/

});